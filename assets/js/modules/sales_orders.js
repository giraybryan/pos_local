$(document).ready(function(){
	
	$('.back-list').click(function(){
		$(this).addClass('hide');
		$('.right-panel').addClass('hide');
		$('#sales-list-wrapper').removeClass('hide');
	});

	$('.back-to-list').click(function(){
		$('.list-container').removeClass('hide');
		$('.view-container').addClass('hide');
	});


	$(this).on('click', '.view-item', function(){
		data_encode = $(this).attr('data-encode');
		data = jQuery.parseJSON($(this).attr('data-encode')) ;
		$.each(data, function(column, value){

			if(column == 'required_item_ids'){

				//for selected items
				items_selected = [];
				$.each(value, function(index, item_value){
					items_selected.push(item_value.text);
					$('#'+column+'_view').html(items_selected.join());
					console.log(items_selected.join());
				});

				
			}	else if(column == 'id'){

				//for edit button under view form
				$('#'+column+'_view').attr('data-id', value).attr('data-encode', data_encode);

				$('#transaction_wrapper').html(ajax_loader);	
				$.ajax({
					type: 'GET',
					url:  base_url+'sales_orders/get_transactions/'+value,
					success: function(data){
						$('#transaction_wrapper').html(data);

					}

				});

			} else if(column == 'used'){
					$('#'+column+'_view').html(parseInt(data.max_use) - parseInt(data.used));
			}
			 else {

			 	//put normal column value per label
				$('#'+column+'_view').html(value);
			}
			
			$('.list-container').addClass('hide');
			$('.view-container').removeClass('hide');
		
		});
	
		
	});
	
	
	// VIEW TRANSACTIONS
	$(this).on('click','.view-transaction', function(){
		transaction_id = $(this).attr('data-id');
	    console.log(transaction_id);
		$('#modal').modal('show');
		$('.modal-content').html($('#log_modal').html());
		$('.modal-body').html(ajax_loader);
				
		$.ajax({
			type: 'GET',
			url : base_url+'sales_orders/get_transaction_details/'+transaction_id,
			success: function(data) {
				$('.modal-body').html(data);
			}

		});
		
	});
	
	
});
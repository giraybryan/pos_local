$(document).ready(function(){
	
	
	/* FILTER FUNCTIONS*/
	$('#filter_category').change(function(){
		get_list();
	});

	$('#filter_keyword').keyup(function(){
		get_list();
	});

	$('#filter_branch').change(function(){
		get_list();
	});
	/* FILTER FUNCTIONS*/
});


function get_list(){
	
	branch_id = 0;
	category_id = 0;
	keyword = 0;

	if($('#filter_keyword').val())
		keyword = $('#filter_keyword').val();

	if($('#filter_branch').val())
		branch_id = $('#filter_branch').val();

	if($('#filter_category').val())
		category_id = $('#filter_category').val();

	
	
	$('#list_container').html(ajax_loader);
	$('#list_container').load(base_url+'purchase_orders/paginate/'+keyword+'/'+branch_id+'/'+category_id);
}
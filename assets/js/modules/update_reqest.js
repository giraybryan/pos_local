$(document).ready(function() { 

	redeliver_modal = $('#redeliver_modal').html();
	$('#redeliver_modal').html('');
	$('#redeliver').click(function(){

		$('#modal').modal('show');
		$('.modal-content').html(redeliver_modal);
		$('.modal-dialog').attr('style','width: 30% !important');
		validation_form();	
	});

	$('.deliver_qty').keyup(function(){

		msg ='';
		current_field = $(this);
		if( parseInt($(this).siblings('.current_stock').val()) < parseInt($(this).siblings('.request_qty').val()) ){
			console.log($(this).siblings('.current_stock').val()+' '+$(this).siblings('.request_qty').val());
			$('#submit').attr('disabled', true);
			msg = 'Not enough remaining stocks';
			console.log('Not enough remaining stocks');

			$(current_field).addClass('invalid');


		} else {


			$(current_field).removeClass('invalid');



			$('#submit').attr('disabled', false);
			if($(this).val() == $(this).siblings('.request_qty').val()){
				
				invalid_ctr = 0;
				$('.deliver_qty').each(function(){
					if($(this).hasClass('invalid')){
						invalid_ctr = invalid_ctr + 1;
						$('#submit').attr('disabled', true);
					} 

					if(invalid_ctr == 0){
						$('#submit').attr('disabled', false);
					}
						
				});
				
			} else {
				$('#submit').attr('disabled', true);
				msg = 'Not equal to reqested quantity';
				console.log('Not equal to reqested quantity');
			}

		}

		$(current_field).closest('.request_info').find('.error_msg').html(msg);

		



	});

	$('#redeliver_form button[type="submit"]').click(function(e){

		e.preventDefault();
		alert('x');
	});

	
	
});


function validation_form(){

	$('#redeliver_form').validate({
		rules: {

			password: {
				required: true,
			},
			
			

		
		},
		submitHandler: function(form) {
			
			user_id = $('#user_id').val();
			if (user_id == '') {
				user_id = 0;
			};
			
			var data = new FormData( form );
			
			swal({   
				title: "Are you sure?",   
				text: "You will now be Reset the device that is already sync with this Request Order.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Save it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				confirmButtonColor: '#21B0F0', 
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'request_orders/redeliver',
							data: data,
							async: false,
							cache: false,
							contentType: false,
							processData: false,
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								if(response.result == 1){
								
									swal("Saved!", response.msg, "success");  
									$('.back-list').trigger('click');
									
									$('[data-dismiss="modal"]').trigger('click');
									
								} else {
									swal("Cancelled", response.msg , "error"); 
								}
							}
						});
						

						
					} else {     
							swal("Cancelled", "Canceled", "error");   
					} 
			});
		},
	});

}
$(document).ready(function(){
	
	$('.datepick').datepicker();

	$('.back-list').click(function(){
		$(this).addClass('hide');
		$('.right-panel').addClass('hide');
		$('#employee-list-wrapper').removeClass('hide');
	});

	$('.back-to-list').click(function(){
		$('.list-container').removeClass('hide');
		$('.view-container').addClass('hide');
	});

	$('#add-item').click(function(){
		$('.right-panel').addClass('hide');
		$('#employee-form-wrapper').removeClass('hide');
		$('#employee-form input[type="text"]').val('');
		$('#employee-form input[type="hidden"]').val('');
		$('#employee-form input[type="file"]').val('');
		$('#employee-form select').val('').trigger('change');
		$('#employee-form textarea').val('').trigger('change');
		$('#employee-form input[type="email"]').val('').trigger('change');
		$('.back-list').removeClass('hide');
		$('.current_file').addClass('hide');

	
	});

	$(this).on('click', '.edit-item', function(){
		data = jQuery.parseJSON($(this).attr('data-encode')) ;
		$('.add-item').trigger('click');
		
		$.each(data, function(column, value){

			$('#'+column).val(value).trigger('change');
		
		
		});
	});



	$(this).on('click', '.view-item', function(){
		data_encode = $(this).attr('data-encode');
		data = jQuery.parseJSON($(this).attr('data-encode')) ;
		$.each(data, function(column, value){

			if(column == 'required_item_ids'){

				//for selected items
				items_selected = [];
				$.each(value, function(index, item_value){
					items_selected.push(item_value.text);
					$('#'+column+'_view').html(items_selected.join());
					console.log(items_selected.join());
				});

				
			}	else if(column == 'id'){

				//for edit button under view form
				$('#'+column+'_view').attr('data-id', value).attr('data-encode', data_encode);

			} else if(column == 'used'){
					$('#'+column+'_view').html(parseInt(data.max_use) - parseInt(data.used));
			}
			 else {

			 	//put normal column value per label
				$('#'+column+'_view').html(value);
			}
			
			$('.list-container').addClass('hide');
			$('.view-container').removeClass('hide');
		
		});
	
		
	});


	$('#employee-form').validate({
		rules: {

			first_name: {
				required: true,
			},
			last_name: {
				required: true,
			},
			phone: {
				required: true,
			},
			email: {
				required: true,
			},
			branch_id: {
				required: true,
			},
			password: {
				required: true,
			},
			shift: {
				required: true,
			},


		
		},
		submitHandler: function(form) {
			
			user_id = $('#user_id').val();
			if (user_id == '') {
				user_id = 0;
			};
			
			var data = new FormData( form );
			
			swal({   
				title: "Are you sure?",   
				text: "You will now be saving the employee.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Save it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				confirmButtonColor: '#21B0F0', 
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'employees/add',
							data: data,
							async: false,
							cache: false,
							contentType: false,
							processData: false,
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								if(response.result == 1){
								
									swal("Saved!", response.msg, "success");  
									$('.back-list').trigger('click');
									
									get_list();
									
								} else {
									swal("Cancelled", response.msg , "error"); 
								}
							}
						});
						

						
					} else {     
							swal("Cancelled", "Canceled", "error");   
					} 
			});
		},
	});


	$('#filter_keyword').keyup(function(){
		get_list();
	});

	$('#filter_branch').change(function(){
		get_list();
	});

	// VIEW LOGS
	$(this).on('click','.employee-log', function(){
		employee_id = $(this).attr('data-id');

		$('#modal').modal('show');
		$('.modal-content').html($('#log_modal').html());
		$('.modal-body').html(ajax_loader);
	
		$.ajax({
			type: 'GET',
			url : base_url+'employees/paginate_logs/'+employee_id,
			success: function(data) {
				$('.modal-body').html(data);
			}

		});

	});

	

	
});


function get_list(){
	
	branch_id = 0;
	keyword = 0;

	if($('#filter_keyword').val())
		keyword = $('#filter_keyword').val();

	if($('#filter_branch').val())
		branch_id = $('#filter_branch').val();

	
	
	$('#list_container').html(ajax_loader);
	$('#list_container').load(base_url+'employees/paginate/'+keyword+'/'+branch_id);
}
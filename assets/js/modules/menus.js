$(document).ready(function(){
	
	$('.back-list').click(function(){
		$(this).addClass('hide');
		$('.right-panel').addClass('hide');
		$('#category-list-wrapper').removeClass('hide');
	});

	$('.back-to-list').click(function(){
		$('.list-container').removeClass('hide');
		$('.view-container').addClass('hide');
	});

	$('#add-item').click(function(){
		$('.right-panel').addClass('hide');
		$('#menu-form-wrapper').removeClass('hide');
		$('#menu-form input[type="text"]').val('');
		$('#menu-form input[type="hidden"]').val('');
		$('#menu-form input[type="file"]').val('');
		$('#menu-form select').val('').trigger('change');
		$('#menu-form textarea').val('').trigger('change');
		$('.back-list').removeClass('hide');
		$('.current_file').addClass('hide');
		$('#modify_btn').attr('href', '').addClass('hide');

		
	});

	$(this).on('click', '.edit-item', function(){
		data = jQuery.parseJSON($(this).attr('data-encode')) ;
		$('.add-item').trigger('click');
		
		$.each(data, function(column, value){
			
			if(column == 'image' && value != null){
				console.log(column+' '+value);
				$('.current_file').removeClass('hide');
				$('#current_image').attr('src', base_url+'uploads/menu/'+value);
			} else if(column == 'image' && value == null) {
				$('.current_file').addClass('hide');

			}

			if(column == 'id'){
				$('#modify_btn').attr('href', base_url+'menus/menu_ingredients/'+value).removeClass('hide');
			}

			$('#'+column).val(value).trigger('change');
			//$('#menu_categories').rules("remove","required");
		
		});
	});

	$('#menu').change(function(){
		file_obj = $(this)
		var f=this.files[0]
		
		if(f.size > 2000000){
			
			$(file_obj).siblings('label.error').remove();
			$(file_obj).siblings('label.errors').remove();
				
			$(file_obj).parent('div').append('<label for="menu" generated="true" class="errors">Please select file with filesize less than 2MB.</label>');
			$(file_obj).closest('form').find('input[type="submit"]').attr('disabled',true);
			$(file_obj).closest('form').find('button[type="submit"]').attr('disabled',true);
		} else {
			$(file_obj).siblings('label.errors').remove();
			$(file_obj).closest('form').find('[type="submit"]').attr('disabled',false);
			$(file_obj).closest('form').find('button[type="submit"]').attr('disabled',false);
		}
	});

	$('#menu-form').validate({
		rules: {

			title: {
				required: true,
			},
			description: {
				required: true,
			},
			menu_category_id: {
				required: true,
			},
			status_id: {
				required: true,
			},
			price: {
				required: true,
			},
			menu: {
				accept:   "png|jpe?g|gif",
			}
		},
		submitHandler: function(form) {
			
			user_id = $('#user_id').val();
			if (user_id == '') {
				user_id = 0;
			};
			
			var data = new FormData( form );
			
			swal({   
				title: "Are you sure?",   
				text: "You will now be saving the menu.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Save it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				confirmButtonColor: '#21B0F0', 
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'menus/add',
							data: data,
							async: false,
							cache: false,
							contentType: false,
							processData: false,
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								if(response.result == 1){
								
									swal("Saved!", response.msg, "success");  
					
									get_list();

									$('#modify_btn').attr('href', response.modify_link).removeClass('hide');
									
								} else {
									swal("Cancelled", response.msg , "error"); 
								}
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		},
	});


	/* FILTER FUNCTIONS*/
	$('#filter_category').change(function(){
		get_list();
	});

	$('#filter_keyword').keyup(function(){
		get_list();
	});

	$('#filter_status').change(function(){
		get_list();
	});

	
	/* FILTER FUNCTIONS*/
});




function get_list(){
	
	category_id = 0;
	keyword = 0;
	status_id = 0;

	if($('#filter_keyword').val())
		keyword = $('#filter_keyword').val();

	if($('#filter_category').val())
		category_id = $('#filter_category').val();

	if($('#filter_status').val())
		status_id = $('#filter_status').val();

	
	
	$('#list_container').html(ajax_loader);
	$('#list_container').load(base_url+'menus/paginate/'+category_id+'/'+keyword+'/'+status_id);
}
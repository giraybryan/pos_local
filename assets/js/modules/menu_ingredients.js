$(document).ready(function(){
	 $('[data-toggle="tooltip"]').tooltip();
	[{
		name: 'advanced',
		pull: 'clone',
		put: false
		
	},
	{
		name: 'advanced',
		pull: 'clone',
		put: true
	}, {
		name: 'advanced',
		pull: false,
		put: true
	}].forEach(function (groupOpts, i) {
		Sortable.create(document.getElementById('advanced-' + (i + 1)), {
			sort: (i != 1),
			group: groupOpts,
			animation: 150,
			change: function( event, ui ) {
				alert(event+' '+ui);
			 },
			 filter: '.js-remove',
			onFilter: function (evt) {
				evt.item.parentNode.removeChild(evt.item);
				item_list();

			},
			 onEnd: function (/**Event*/evt) {
			 	item = evt.item;
			 	check_items(item);
		      	item_list();
		       $('[data-toggle="tooltip"]').tooltip();
		    },
		});
	});
	

	$(this).on('keyup','.item_qty', function(){
		item_qty = $(this).val();

		$(this).closest('li').attr('data-qty', item_qty);

	});
	$(this).on('click','.exclude-btn', function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$(this).attr('data-original-title','Add to Exclude list')
			$(this).closest('li').attr('data-exclude', 0)
		} else {
			$(this).addClass('active').attr('data-original-title','Remove From Exclude list');

			$(this).closest('li').attr('data-exclude', 1)
			
		}
		item_list();
		 
	});
	
	$('#filter_category').change(function(){
		category_id = 0;

		if($(this).val())
			category_id = $(this).val();

		$('#advanced-1').html('<li>'+ajax_loader+'</li>');
		$('#advanced-1').load(base_url+'menus/get_inventory_items/'+category_id);
	});

	$('#modify-form').validate({
		rules: {

			title: {
				required: true,
			},
			description: {
				required: true,
			},
			menu_category_id: {
				required: true,
			},
			status_id: {
				required: true,
			},
			price: {
				required: true,
			},
		},
		submitHandler: function(form) {
			
			user_id = $('#user_id').val();
			if (user_id == '') {
				user_id = 0;
			};
			
			var data = new FormData( form );
			
			swal({   
				title: "Are you sure?",   
				text: "You will now be saving the menu ingredients and addons.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Save it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				confirmButtonColor: '#21B0F0', 
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'menus/modify_menu',
							data: data,
							async: false,
							cache: false,
							contentType: false,
							processData: false,
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								if(response.result == 1){
								
									swal("Saved!", response.msg, "success");  
									
								} else {
									swal("Cancelled", response.msg , "error"); 
								}
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		},
	});
});

function check_items(item_obj){
	item_id = $(item_obj).attr('data-id');

	if($('#advanced-2 li[data-id="'+item_id+'"]').length > 1){
		
		$(item_obj).remove();
		item_qty = parseInt($('#advanced-2 li[data-id="'+item_id+'"]').attr('data-qty')) + 1;
		$('#advanced-2 li[data-id="'+item_id+'"]').attr('data-qty', item_qty);
		item_list();


	}
	$('#advanced-3 item_details').html('');
}
function item_list(){
	ingredients = [];
	addons = [];
	excludes = [];

	$('#excludes').val(excludes.join());
	$('#ingredients').val(ingredients.join());
	$('#addons').val(addons.join());
	$('#advanced-3 .item_details').html('');

	$('#advanced-2 li').each(function(){

		ingredient_id = $(this).attr('data-id');
		ingredient_qty = $(this).attr('data-qty');

		item_details = '<input type="hidden" name="ingredient['+ingredient_id+'][ingredient_id]" value="'+ingredient_id+'"/>';
		item_details = item_details + '<input type="text" class="form-control item_qty" placeholder="Quantity" name="ingredient['+ingredient_id+'][qty]" value="'+ingredient_qty+'"/>';

		$(this).find('.item_details').html(item_details);

		ingredients.push($(this).attr('data-id'));
		
		if($(this).attr('data-exclude') == 1){
			excludes.push($(this).attr('data-id'));
			$('#excludes').val(excludes.join());
			
		}
			

		$('#ingredients').val(ingredients.join());


	});

	$('#advanced-3 li').each(function(){

		addons.push($(this).attr('data-id'));
		$('#addons').val(addons.join());
	});

}
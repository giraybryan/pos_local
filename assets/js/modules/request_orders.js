$(document).ready(function(){
	
	$('.back-to-list').click(function(){
		$('.list-container').removeClass('hide');
		$('.view-container').addClass('hide');
	});

	$(this).on('click', '.view-item', function(){
		data_encode = $(this).attr('data-encode');
		data = jQuery.parseJSON($(this).attr('data-encode')) ;
		$.each(data, function(column, value){
			
			if(column == 'Delivered'){

				//for edit button under view form
				if(value == 1){
					value = 'Delivered';
				} else {

					value = 'Pending';
				}
				$('#'+column+'_view').html(value);;

			// } else if(column == 'used'){
					//$('#'+column+'_view').html(parseInt(data.max_use) - parseInt(data.used));
			} else {

			 	//put normal column value per label
			 	console.log(column+' '+value);
				$('#'+column+'_view').html(value);
			}
			
			$('.list-container').addClass('hide');
			$('.view-container').removeClass('hide');
		
		});
	
		
	});

	/* FILTER FUNCTIONS*/
	$('#filter_category').change(function(){
		get_list();
	});

	$('#filter_keyword').keyup(function(){
		get_list();
	});

	$('#filter_branch').change(function(){
		get_list();
	});


	
	/* FILTER FUNCTIONS*/
});


function get_list(){
	
	branch_id = 0;
	category_id = 0;
	keyword = 0;

	if($('#filter_keyword').val())
		keyword = $('#filter_keyword').val();

	if($('#filter_branch').val())
		branch_id = $('#filter_branch').val();

	if($('#filter_category').val())
		category_id = $('#filter_category').val();

	
	
	$('#list_container').html(ajax_loader);
	$('#list_container').load(base_url+'request_orders/paginate/'+keyword+'/'+branch_id+'/'+category_id);
}
$(document).ready(function(){

	

	$('#filter_category, #filter_danger').change(function(){

		get_list();
	});
	 $('[data-toggle="tooltip"]').tooltip();
	[{
		name: 'advanced',
		pull: 'clone',
		put: false
		
	},
	{
		name: 'advanced',
		pull: 'clone',
		put: true
	}, {
		name: 'advanced',
		pull: false,
		put: true
	}].forEach(function (groupOpts, i) {
		Sortable.create(document.getElementById('advanced-' + (i + 1)), {
			sort: (i != 1),
			group: groupOpts,
			animation: 150,
			change: function( event, ui ) {
				alert(event+' '+ui);
			 },
			 filter: '.js-remove',
			onFilter: function (evt) {
				evt.item.parentNode.removeChild(evt.item);
				item_list();

			},
			 onEnd: function (/**Event*/evt) {
			 	item = evt.item;
			 	check_items(item);
		      	item_list();
		       $('[data-toggle="tooltip"]').tooltip();
		    },
		});
	});
	

	$(this).on('keyup','.item_qty', function(){
		item_qty = $(this).val();

		$(this).closest('li').attr('data-qty', item_qty);

	});
	
	

	$('#ro_form').submit(function(e){
		e.preventDefault();
	});
	$('#ro_form').validate({
		rules: {

			title: {
				required: true,
			},
			description: {
				required: true,
			},
			menu_category_id: {
				required: true,
			},
			status_id: {
				required: true,
			},
			price: {
				required: true,
			},
		},
		submitHandler: function(form) {
			
			user_id = $('#user_id').val();
			if (user_id == '') {
				user_id = 0;
			};
			
			var data = new FormData( form );
			
			swal({   
				title: "Are you sure?",   
				text: "You will now be saving the menu ingredients and addons.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Save it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				confirmButtonColor: '#21B0F0', 
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'request_orders/add',
							data: data,
							async: false,
							cache: false,
							contentType: false,
							processData: false,
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								if(response.result == 1){
								
									swal("Saved!", response.msg, "success");  

									
									window.location.replace(base_url+"request_orders");
									
								} else {
									swal("Cancelled", response.msg , "error"); 
								}
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		},
	});
});

function get_list(){

	threshold = 0;
	category_id = 0;

	if($('#filter_category').val())
		category_id = $('#filter_category').val();

	if($('#filter_danger:checked').length)
		threshold = 1;


	$('#advanced-1').load(base_url+'request_orders/get_inventory_support/'+category_id+'/'+threshold);
}

function check_items(item_obj){
	item_id = $(item_obj).attr('data-id');

	if($('#advanced-2 li[data-id="'+item_id+'"]').length > 1){
		
		$(item_obj).remove();
		item_qty = parseInt($('#advanced-2 li[data-id="'+item_id+'"]').attr('data-qty')) + 1;
		$('#advanced-2 li[data-id="'+item_id+'"]').attr('data-qty', item_qty);
		item_list();


	}

	$('#advanced-1 li').each(function(){
		li_obj = $(this);
		item_id = $(this).attr('data-id');

		if($('#advanced-2 li[data-id="'+item_id+'"]').length > 1 ){
			$(li_obj).find('.form_list').html('');
		}
		
	}); 


}
function item_list(){
	ingredients = [];
	addons = [];
	excludes = [];

	
	$('#advanced-3 li').each(function(){

		addons.push($(this).attr('data-id'));
		$('#addons').val(addons.join());
	});

}
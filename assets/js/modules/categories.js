$(document).ready(function(){
	
	$('.back-list').click(function(){
		$(this).addClass('hide');
		$('.right-panel').addClass('hide');
		$('#category-list-wrapper').removeClass('hide');
	});

	$('.back-to-list').click(function(){
		$('.list-container').removeClass('hide');
		$('.view-container').addClass('hide');
	});

	$('#add-item').click(function(){
		$('.right-panel').addClass('hide');
		$('#category-form-wrapper').removeClass('hide');
		$('#menu-category-form input[type="text"]').val('');
		$('#menu-category-form input[type="hidden"]').val('');
		$('#menu-category-form input[type="file"]').val('');
		$('#menu-category-form select').val('').trigger('change');
		$('.back-list').removeClass('hide');
		$('.current_file').addClass('hide');

		$('#menu_categories').rules("add","required");
	});


	$(this).on('click', '.view-item', function(){
		data_encode = $(this).attr('data-encode');
		data = jQuery.parseJSON($(this).attr('data-encode')) ;
		$.each(data, function(column, value){
			if(column == 'image'){
				$('#'+column+'_view').attr('src', base_url+'uploads/menu_categories/'+value);
			} else if(column == 'id'){
				$('#'+column+'_view').attr('data-id', value).attr('data-encode', data_encode);
			} else {
				$('#'+column+'_view').html(value);
			}
			
			$('.list-container').addClass('hide');
			$('.view-container').removeClass('hide');
		
		});
		

		/* FOR CATEGORY OPTION*/
		$('#add-option').attr('data-id', data.id);

		get_options(data.id);
		
	});

	$(this).on('click', '.edit-item', function(){
		data = jQuery.parseJSON($(this).attr('data-encode')) ;
		$('.add-item').trigger('click');
		
		$.each(data, function(column, value){
			
			if(column == 'image' && value != null){
				$('.current_file').removeClass('hide');
				$('#current_image').attr('src', base_url+'uploads/menu_categories/'+value);
			} else {
				$('.current_file').addClass('hide');
			}

			$('#'+column).val(value).trigger('change');
			$('#menu_categories').rules("remove","required");
		
		});
	});

	$('#filter_keyword').keyup(function(){
		keyword = 0;
		
		if($(this).val())
			keyword = $(this).val();
		
		get_list(keyword);
	});


	$('#menu_categories').change(function(){
		file_obj = $(this)
		var f=this.files[0]
		
		if(f.size > 2000000){
			
			$(file_obj).siblings('label.error').remove();
			$(file_obj).siblings('label.errors').remove();
				
			$(file_obj).parent('div').append('<label for="menu_categories" generated="true" class="errors">Please select file with filesize less than 2MB.</label>');
			$(file_obj).closest('form').find('input[type="submit"]').attr('disabled',true);
			$(file_obj).closest('form').find('button[type="submit"]').attr('disabled',true);
		} else {
			$(file_obj).siblings('label.errors').remove();
			$(file_obj).closest('form').find('[type="submit"]').attr('disabled',false);
			$(file_obj).closest('form').find('button[type="submit"]').attr('disabled',false);
		}
	})

	$('#menu-category-form').validate({
		rules: {
			
			
			menu_category_name: {
				required: true,
			},
			menu_categories: {
				accept:   "png|jpe?g|gif",
			}
			
		},
		submitHandler: function(form) {
			
			user_id = $('#user_id').val();
			if (user_id == '') {
				user_id = 0;
			};
			
			var data = new FormData( form );
			
			swal({   
				title: "Are you sure?",   
				text: "You will now be saving the category.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Save it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				confirmButtonColor: '#21B0F0', 
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'categories/add',
							data: data,
							async: false,
							cache: false,
							contentType: false,
							processData: false,
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								if(response.result == 1){
								
									swal("Saved!", response.msg, "success");  
									$('.back-list').trigger('click');
									
									get_list();
									
								} else {
									swal("Cancelled", response.msg , "error"); 
								}
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		},
	});



	/*FOR MENU CATEGORY OPTIONS*/


	$('.back-to-details').click(function(){
		$('#category-list-wrapper').removeClass('hide');
		$('#option-category-form-wrapper').addClass('hide');
	});

	$('#add-option').click(function(){
		$('.right-panel').addClass('hide');
		$('#option-category-form-wrapper').removeClass('hide');

		$('#option-category-form-wrapper input[type="text"],#option-category-form-wrapper input[type="hidden"]').val('');
		$('#menu_category_id').val($(this).attr('data-id'));


	});



	$('#option-category-form').validate({
		rules: {
			
			
			option_name: {
				required: true,
			},
			price: {
				required: true,
			},
			
		},
		submitHandler: function(form) {
			
			category_id = $('#menu_category_id').val();
			if (category_id == '') {
				category_id = 0;
			};
			
			var data = new FormData( form );
			
			swal({   
				title: "Are you sure?",   
				text: "You will now be saving the option.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Save it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				confirmButtonColor: '#21B0F0', 
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'categories/add_option',
							data: data,
							async: false,
							cache: false,
							contentType: false,
							processData: false,
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								if(response.result == 1){
								
									swal("Saved!", response.msg, "success");  
									$('.back-list').trigger('click');
									
									get_options(category_id);
									
								} else {
									swal("Cancelled", response.msg , "error"); 
								}
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		},
	});

	/*FOR MENU CATEGORY OPTIONS*/

});


function get_list(keyword, status_id){
	keywords = 0;

	if(keyword)
		keywords = keyword;
	
	$('#list_container').html(ajax_loader);
	$('#list_container').load(base_url+'categories/paginate/'+keywords);
}

function get_options(category_id){

	$('#option_list_wrapper').html(ajax_loader);
	$('#option_list_wrapper').load(base_url+'categories/category_options/'+category_id);

}
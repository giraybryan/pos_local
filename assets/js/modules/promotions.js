$(document).ready(function(){
	
	$('.datepick').datepicker();

	$('.back-list').click(function(){
		$(this).addClass('hide');
		$('.right-panel').addClass('hide');
		$('#promotion-list-wrapper').removeClass('hide');
	});

	$('.back-to-list').click(function(){
		$('.list-container').removeClass('hide');
		$('.view-container').addClass('hide');
	});

	$('#add-item').click(function(){
		$('.right-panel').addClass('hide');
		$('#promotion-form-wrapper').removeClass('hide');
		$('#promotion-form input[type="text"]').val('');
		$('#promotion-form input[type="hidden"]').val('');
		$('#promotion-form input[type="file"]').val('');
		$('#promotion-form select').val('').trigger('change');
		$('#promotion-form textarea').val('').trigger('change');
		$('.back-list').removeClass('hide');
		$('.current_file').addClass('hide');

		set_ajax_select2();
		set_ajax_promo_items();
		
	});

	$(this).on('click', '.edit-item', function(){
		data = jQuery.parseJSON($(this).attr('data-encode')) ;
		$('.add-item').trigger('click');
		
		$.each(data, function(column, value){
			
			if(column == 'required_item_ids' && value != null){
				set_ajax_select2(value);
				selected_value = []
				// $.each(value, function (index, val){

				// 	console.log(val.id);
				// 	selected_value.push(val.id);
				// 	$('#required_item_ids').val(selected_value).trigger('change');
				// 	console.log(selected_value);
				// 	alert($('#required_item_ids').val());

				// });
				

			} else if( column == 'promo_item_ids' && value != null) {

				set_ajax_promo_items(value);
				
			}	else {

				$('#'+column).val(value).trigger('change');
			}

			
			//$('#menu_categories').rules("remove","required");
		
		});
	});



	$(this).on('click', '.view-item', function(){
		data_encode = $(this).attr('data-encode');
		data = jQuery.parseJSON($(this).attr('data-encode')) ;
		$.each(data, function(column, value){

			if(column == 'required_item_ids'){

				//for selected items
				items_selected = [];
				$.each(value, function(index, item_value){
					items_selected.push(item_value.text);
					$('#'+column+'_view').html(items_selected.join());
					console.log(items_selected.join());
				});

				
			}	else if(column == 'id'){

				//for edit button under view form
				$('#'+column+'_view').attr('data-id', value).attr('data-encode', data_encode);

			} else if(column == 'used'){
					$('#'+column+'_view').html(parseInt(data.max_use) - parseInt(data.used));
			}
			 else {

			 	//put normal column value per label
				$('#'+column+'_view').html(value);
			}
			
			$('.list-container').addClass('hide');
			$('.view-container').removeClass('hide');
		
		});
	
		
	});


	$('#promotion-form').validate({
		rules: {

			title: {
				required: true,
			},
			reduction_type: {
				required: true,
			},
			reduction_amount: {
				required: true,
			},
			start_date: {
				required: true,
			},
			end_date: {
				required: true,
			},
			max_use: {
				required: true,
			},
			branch_id: {
				required: true,
			},
			description: {
				required: true,
			},
			required_amount: {
				required: true,
			}

		
		},
		submitHandler: function(form) {
			
			user_id = $('#user_id').val();
			if (user_id == '') {
				user_id = 0;
			};
			
			var data = new FormData( form );
			
			swal({   
				title: "Are you sure?",   
				text: "You will now be saving the promotion.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Save it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				confirmButtonColor: '#21B0F0', 
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'promotions/add',
							data: data,
							async: false,
							cache: false,
							contentType: false,
							processData: false,
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								if(response.result == 1){
								
									swal("Saved!", response.msg, "success");  
									$('.back-list').trigger('click');
									
									get_list();
									
								} else {
									swal("Cancelled", response.msg , "error"); 
								}
							}
						});
						

						
					} else {     
							swal("Cancelled", "Canceled", "error");   
					} 
			});
		},
	});


	$('#filter_keyword').keyup(function(){
		get_list();
	});

	$('#filter_branch').change(function(){
		get_list();
	});



	 
	$('#promo_item_ids').select2({
	    ajax: {
	      url: base_url+"menus/get_menus",
	      dataType: 'json',
	       data: function (params) {
	       
			return {
			q: params, // search term
			};
			},
		    results: function (data, page) {

		    		
		            var newData = [];
		           
		            $.each(data.items, function (item) {
		            	console.log(data.items[item]);
		                newData.push({
		                    id: data.items[item].id  //id part present in data 
		                  , text: data.items[item].title  //string to be displayed
		                });
		            });
		           
		            return { results: newData };
		       
		    },
	    },
	 	multiple: true,
	 	placeholder: 'Select items',
	    templateResult: formatRepo,
	    templateSelection: formatRepoSelection, 
	    minimumInputLength: 1,
	  });
	

	$('#required_item_ids').select2({
	    ajax: {
	      url: base_url+"menus/get_menus",
	      dataType: 'json',
	       data: function (params) {
	       
			return {
			q: params, // search term
			};
			},
		    results: function (data, page) {

		    		
		            var newData = [];
		           
		            $.each(data.items, function (item) {
		            	console.log(data.items[item]);
		                newData.push({
		                    id: data.items[item].id  //id part present in data 
		                  , text: data.items[item].title  //string to be displayed
		                });
		            });
		           
		            return { results: newData };
		       
		    },
	    },
	 	multiple: true,
	 	placeholder: 'Select items',
	    templateResult: formatRepo,
	    templateSelection: formatRepoSelection, 
	    minimumInputLength: 1,
	  });
});

function formatRepo (repo) {
	    if (repo.loading) return repo.text;

	    var markup = '<div class="clearfix">' +
	    '<div class="col-sm-1">' +
	    '<img src="' + repo.image + '" style="max-width: 100%" />' +
	    '</div>' +
	    '<div clas="col-sm-10">' +
	    '<div class="clearfix">' +
	    '<div class="col-sm-6">' + repo.title + '</div>' +
	    '</div>';

	    if (repo.title) {
	      markup += '<div>' + repo.title + '</div>';
	    }

	    markup += '</div></div>';

	    return markup;
  }
function formatRepoSelection (item) {
return item.title || item.text;
}
function set_ajax_promo_items(values){
	$('#promo_item_ids').select2({
	    ajax: {
	      url: base_url+"menus/get_menus",
	      dataType: 'json',
	       data: function (params) {
	       
			return {
			q: params, // search term
			};
			},
		    results: function (data, page) {

		    		
		            var newData = [];
		           
		            $.each(data.items, function (item) {
		            	console.log(data.items[item]);
		                newData.push({
		                    id: data.items[item].id  //menu id
		                  , text: data.items[item].title  //menu title
		                });
		            });
		           
		            return { results: newData };
		       
		    },
	    },
	   placeholder: 'Select items',
	    data: values,
	 	multiple: true,
	    templateResult: formatRepo,
	    templateSelection: formatRepoSelection, 
	    minimumInputLength: 1,
	  }).select2("data",
	     values
	);

}
function set_ajax_select2(values){

	$('#required_item_ids').select2({
	    ajax: {
	      url: base_url+"menus/get_menus",
	      dataType: 'json',
	       data: function (params) {
	       
			return {
			q: params, // search term
			};
			},
		    results: function (data, page) {

		    		
		            var newData = [];
		           
		            $.each(data.items, function (item) {
		            	console.log(data.items[item]);
		                newData.push({
		                    id: data.items[item].id  //menu id
		                  , text: data.items[item].title  //menu title
		                });
		            });
		           
		            return { results: newData };
		       
		    },
	    },
	   placeholder: 'Select items',
	    data: values,
	 	multiple: true,
	    templateResult: formatRepo,
	    templateSelection: formatRepoSelection, 
	    minimumInputLength: 1,
	  }).select2("data",
	     values
	);

}

function get_list(){
	
	branch_id = 0;
	keyword = 0;

	if($('#filter_keyword').val())
		keyword = $('#filter_keyword').val();

	if($('#filter_branch').val())
		branch_id = $('#filter_branch').val();

	
	
	$('#list_container').html(ajax_loader);
	$('#list_container').load(base_url+'promotions/paginate/'+keyword+'/'+branch_id);
}
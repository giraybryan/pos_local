$(document).ready(function(){
	
	$('.back-list').click(function(){
		$(this).addClass('hide');
		$('.right-panel').addClass('hide');
		$('#category-list-wrapper').removeClass('hide');
	});

	$('.back-to-list').click(function(){
		$('.list-container').removeClass('hide');
		$('.view-container').addClass('hide');
	});

	$('#add-item').click(function(){
		$('.right-panel').addClass('hide');
		$('#discount-form-wrapper').removeClass('hide');
		$('#discount-form input[type="text"]').val('');
		$('#discount-form input[type="hidden"]').val('');
		$('#discount-form input[type="file"]').val('');
		$('#discount-form select').val('').trigger('change');
		$('#discount-form textarea').val('').trigger('change');
		$('.back-list').removeClass('hide');
		$('.current_file').addClass('hide');

		
	});

	$(this).on('click', '.edit-item', function(){
		data = jQuery.parseJSON($(this).attr('data-encode')) ;
		$('.add-item').trigger('click');
		
		$.each(data, function(column, value){
			
			if(column == 'image' && value != null){
				console.log(column+' '+value);
				$('.current_file').removeClass('hide');
				$('#current_image').attr('src', base_url+'uploads/menu/'+value);
			} else if(column == 'image' && value == null) {
				$('.current_file').addClass('hide');

			}

			$('#'+column).val(value).trigger('change');
			//$('#menu_categories').rules("remove","required");
		
		});
	});


	$('#discount-form').validate({
		rules: {

			title: {
				required: true,
			},
			reduction_type: {
				required: true,
			},
			reduction_amount: {
				required: true,
			},
		
		},
		submitHandler: function(form) {
			
			user_id = $('#user_id').val();
			if (user_id == '') {
				user_id = 0;
			};
			
			var data = new FormData( form );
			
			swal({   
				title: "Are you sure?",   
				text: "You will now be saving the discount.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Save it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				confirmButtonColor: '#21B0F0', 
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'discounts/add',
							data: data,
							async: false,
							cache: false,
							contentType: false,
							processData: false,
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								if(response.result == 1){
								
									swal("Saved!", response.msg, "success");  
									$('.back-list').trigger('click');
									
									get_list();
									
								} else {
									swal("Cancelled", response.msg , "error"); 
								}
							}
						});
						

						
					} else {     
							swal("Cancelled", "Canceled", "error");   
					} 
			});
		},
	});


	$('#filter_keyword').keyup(function(){
		get_list();
	});

});


function get_list(){
	
	reduction_type = 0;
	keyword = 0;

	if($('#filter_keyword').val())
		keyword = $('#filter_keyword').val();

	if($('#filter_reduction_type').val())
		reduction_type = $('#filter_reduction_type').val();

	
	
	$('#list_container').html(ajax_loader);
	$('#list_container').load(base_url+'discounts/paginate/'+keyword+'/'+reduction_type);
}
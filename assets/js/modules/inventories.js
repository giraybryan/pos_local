$(document).ready(function(){
	
	$('.back-to-list').click(function(){
		$('.list-container').removeClass('hide');
		$('.view-container').addClass('hide');
	});
	$('.back-list').click(function(){
		$(this).addClass('hide');
		$('.right-panel').addClass('hide');
		$('#inventory-list-wrapper').removeClass('hide');
		$('#list-container').removeClass('hide');
	});
	

	
	$('#add-item').click(function(){
		$('.right-panel').addClass('hide');
		$('#inventory-form-wrapper').removeClass('hide');
		$('#inventory-form-wrapper input[type="text"]').val('');
		$('#inventory-form-wrapper input[type="hidden"]').val('');
		$('#inventory-form-wrapper input[type="file"]').val('');
		$('#inventory-form-wrapperselect').val('').trigger('change');
		
		$('.back-list').removeClass('hide');
	});
	
	$(this).on('click', '.edit-item', function(){
		data = jQuery.parseJSON($(this).attr('data-encode')) ;
		$('#add-item').trigger('click');
		
		$.each(data, function(column, value){
			
			$('#'+column).val(value).trigger('change');
			$('#item_image').rules("remove","required");
			$('#item_image').rules("remove","filesize");
		
		});
	});

	$(this).on('click', '.view-item', function(){
		data_encode = $(this).attr('data-encode');
		data = jQuery.parseJSON($(this).attr('data-encode')) ;
		$.each(data, function(column, value){
			if(column == 'image'){
				$('#'+column+'_view').attr('src', base_url+'uploads/item_image/'+value);
			} else if(column == 'id'){
				$('#'+column+'_view').attr('data-id', value).attr('data-encode', data_encode);
			} else {
				$('#'+column+'_view').html(value);
			}
			
			$('.list-container').addClass('hide');
			$('.view-container').removeClass('hide');
		
		});
	});

	$('#inventory-form').submit(function(e){

		e.preventDefault();
	});

	$('#item_image').change(function(){
		file_obj = $(this)
		var f=this.files[0]
		
		if(f.size > 2000000){
			
			$(file_obj).siblings('label.error').remove();
			$(file_obj).siblings('label.errors').remove();
				
			$(file_obj).parent('div').append('<label for="item_image" generated="true" class="errors">Please select file with filesize less than 2MB.</label>');
			$(file_obj).closest('form').find('input[type="submit"]').attr('disabled',true);
			$(file_obj).closest('form').find('button[type="submit"]').attr('disabled',true);
		} else {
			$(file_obj).siblings('label.errors').remove();
			$(file_obj).closest('form').find('[type="submit"]').attr('disabled',false);
			$(file_obj).closest('form').find('button[type="submit"]').attr('disabled',false);
		}
	})

	$('#inventory-form').validate({
		rules: {
			
			
			item: {
				required: true,
			},
			stock: {
				required: true,
			},
			inventory_category_id: {
				required: true,
			},
			status: {
				required: true,
			},
			 item_image: {
				required: true,
				accept:   "png|jpe?g|gif",
				//filesize:  1048576,

			}, 
			threshold: {
				required: true,
			},
			
		},
		submitHandler: function(form) {
			
			user_id = $('#user_id').val();
			if (user_id == '') {
				user_id = 0;
			};
			
			var data = new FormData( form );
			
			swal({   
				title: "Are you sure?",   
				text: "You will now be saving the item.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Save it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				confirmButtonColor: '#21B0F0', 
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'inventories/add_item',
							data: data,
							async: false,
							cache: false,
							contentType: false,
							processData: false,
							// dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								if(response == 'Item Saved!'){
								
									swal("Saved!", "Item has been saved.", "success");  
									$('.back-list').trigger('click');
									
									get_list();
									
								} else {
									swal("Cancelled", response , "error"); 
								}
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		},
	});
	
	$('#filter_category').change(function(){
		category_id = $(this).val();
		
		status_id = $('#filter_status').val();
		keyword = $('#filter_keyword').val();
		get_list(category_id = category_id);
		
	});
	
	
	$('#filter_keyword').keyup(function(){
		keyword = $(this).val();
		status_id = $('#filter_status').val();
		category_id = $('#filter_category').val();
		
		get_list(category_id, keyword, status_id );
	});
	
	$('#filter_status').change(function(){
		status_id = $(this).val();
		
		category_id = $('#filter_category').val();
		keyword = $('#filter_keyword').val();
		
		get_list(category_id, keyword, status_id );
	});

	$('#filter_keyword_category').keyup(function(){
		keyword = $(this).val();
		
		
		get_category_list(0, keyword, 0 );
	});
	



	/*FOR INVENTORY CATEGORY*/ 
	$('#view-inventory-category').click(function(){
		$('#inventory-list-wrapper').addClass('hide');
		$('.inventory_quick_search').addClass('hide');
		$('#inventory-category-wrapper').removeClass('hide');
		$('.category_quick_search').removeClass('hide');
	});
	$('.back-to-item').click(function(){
		$('#inventory-list-wrapper').removeClass('hide');
		$('#inventory-category-wrapper').addClass('hide');

		$('.inventory_quick_search').removeClass('hide');
		$('.category_quick_search').addClass('hide');
	});


	$('.back-category-list').click(function(){
		$('.right-panel').addClass('hide');
		$('#inventory-category-wrapper').removeClass('hide');
	});
	$('#add-category').click(function(){
		$('.right-panel').addClass('hide');
		$('#inventory-category-form-wrapper').removeClass('hide');

		$('#inventory-category-form input[type="text"],#inventory-category-form input[type="hidden"]').val('');
	});


	$(this).on('click','.edit-category',function(){
		$(this).attr('data-encode');

		data = jQuery.parseJSON($(this).attr('data-encode')) ;
		$('#add-category').trigger('click');
		
		$.each(data, function(column, value){
			if(column == 'image' && value != null){
				$('.current_file').removeClass('hide');
				$('#current_image').attr('src', base_url+'uploads/category_image/'+value);
			} else {
				$('.current_file').addClass('hide');
			}
			$('#category_'+column).val(value).trigger('change');
		});

	});

	$('#inventory-category-form').validate({
		rules: {
			
			
			category_name: {
				required: true,
			},
			
		},
		submitHandler: function(form) {
			
			user_id = $('#user_id').val();
			if (user_id == '') {
				user_id = 0;
			};
			
			var data = new FormData( form );
			
			swal({   
				title: "Are you sure?",   
				text: "You will now be saving this category.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Save it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				confirmButtonColor: '#21B0F0', 
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'inventories/add_category',
							data: data,
							async: false,
							cache: false,
							contentType: false,
							processData: false,
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								if(response.result == 1){
								
									swal("Saved!", "Category has been saved.", "success");  
									$('.back-category-list').trigger('click');
									
									get_category_list();
									
								} else {
									swal("Cancelled", response.msg , "error"); 
								}
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		},
	});

	
	/*FOR INVENTORY CATEGORY*/ 
});



function get_list(category_id_selected, keyword_selected, status_id_selected){
	category_id = 0;
	status_id = 0;
	keyword = 0;

	if(category_id_selected){
		category_id = category_id_selected;
	}
	if(status_id_selected){
		status_id = status_id_selected;
	}

	if(keyword_selected)
		keyword = keyword_selected

	$('#item_list').html(ajax_loader);
	$('#item_list').load(base_url+'inventories/paginate/'+category_id+'/'+keyword+'/'+status_id);
}

function get_category_list(category_id_selected, keyword_selected, status_id_selected){
	category_id = 0;
	status_id = 0;
	keyword = 0;
	category_id_selected = 0;
	keyword = 0;
	status_id_selected = 0;

	if(category_id_selected){
		category_id = category_id_selected;
	}
	if(status_id_selected){
		status_id = status_id_selected;
	}

	if(keyword_selected)
		keyword = keyword_selected


	
	$('#category_list').html(ajax_loader);
	$('#category_list').load(base_url+'inventories/paginate_category/'+keyword+'/');

	filter_category_placeholder = $('#filter_category').attr('placeholder');
	$('#filter_category').load(base_url+'inventories/get_category_option').select2({placeholder: filter_category_placeholder, allowClear: true });

	inventory_category_field_placeholder = $('#inventory_category_id').attr('placeholder');
	$('#inventory_category_id').load(base_url+'inventories/get_category_option').select2({placeholder: inventory_category_field_placeholder, allowClear: true });

	
}
$(document).ready(function(){
	
	$('#company-form').validate({
		rules: {

			name: {
				required: true,
			},
			slogan: {
				required: true,
			},
			address: {
				required: true,
			},
			company: {
				required: true,
			},
			
		},
		submitHandler: function(form) {
			
			user_id = $('#user_id').val();
			if (user_id == '') {
				user_id = 0;
			};
			
			var data = new FormData( form );
			
			swal({   
				title: "Are you sure?",   
				text: "You will now be saving the company information.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Save it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				confirmButtonColor: '#21B0F0', 
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'company/add',
							data: data,
							async: false,
							cache: false,
							contentType: false,
							processData: false,
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								if(response.result == 1){
								
									swal("Saved!", response.msg, "success");  
									$('.back-list').trigger('click');
									location.reload();
									
									
								} else {
									swal("Cancelled", response.msg , "error"); 
								}
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		},
	});


});
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2015 at 09:47 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dev_pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `search`
--

CREATE TABLE IF NOT EXISTS `search` (
  `code` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `term` text COLLATE utf8_unicode_ci NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `userlevels`
--

CREATE TABLE IF NOT EXISTS `userlevels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userlevel_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `stage_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userlevel_name` (`userlevel_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `userlevels`
--

INSERT INTO `userlevels` (`id`, `userlevel_name`, `stage_name`) VALUES
(-1, 'Super Admin', NULL),
(2, 'Manager', NULL),
(3, 'Developer', 'Development'),
(4, 'QA', 'QA'),
(5, 'Designer', 'Design'),
(6, 'Marketing', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userlevel_permissions`
--

CREATE TABLE IF NOT EXISTS `userlevel_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userlevel_id` int(11) NOT NULL,
  `module_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `permission` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `userlevel_permissions`
--

INSERT INTO `userlevel_permissions` (`id`, `userlevel_id`, `module_type`, `permission`) VALUES
(1, 2, 'Users', 13),
(2, 2, 'Projects', 13),
(3, 2, 'Positions', 13),
(4, 2, 'Tasks', 13),
(5, 2, 'Userlevels', 13),
(6, 2, 'Userlevel Permissions', 13),
(7, 3, 'Users', 4),
(8, 3, 'Projects', 0),
(9, 3, 'Positions', 0),
(10, 3, 'Tasks', 13),
(11, 3, 'Userlevels', 0),
(12, 3, 'Userlevel Permissions', 0),
(13, 4, 'Users', 4),
(14, 4, 'Projects', 13),
(15, 4, 'Positions', 0),
(16, 4, 'Tasks', 13),
(17, 4, 'Userlevels', 0),
(18, 4, 'Userlevel Permissions', 0),
(19, 5, 'Users', 4),
(20, 5, 'Projects', 0),
(21, 5, 'Positions', 0),
(22, 5, 'Tasks', 13),
(23, 5, 'Userlevels', 0),
(24, 5, 'Userlevel Permissions', 0),
(25, 6, 'Users', 4),
(26, 6, 'Projects', 13),
(27, 6, 'Positions', 0),
(28, 6, 'Tasks', 13),
(29, 6, 'Userlevels', 0),
(30, 6, 'Userlevel Permissions', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `userlevel_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `image_file` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `Unique_Username` (`username`),
  KEY `last_name` (`last_name`),
  KEY `first_name` (`first_name`),
  KEY `userlevel_id` (`userlevel_id`),
  KEY `position_id` (`position_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `last_name`, `first_name`, `phone`, `email`, `last_login`, `date_created`, `userlevel_id`, `position_id`, `image_file`) VALUES
(7, 'admin', 'admin', 'Sonido', 'Jayson', '1234567', 'jayson.sonido@ctsweb.fr', '2014-11-13 06:11:03', '2014-08-29 09:37:20', -1, 1, '20140901075626-1.jpg'),
(15, 'gabriel.concepcion', 'forward321', 'Concepcion', 'Gabriel', '', 'gabriel.concepcion@forward.ph', '2014-11-06 08:04:11', '2014-09-01 07:47:43', 3, 4, NULL),
(17, 'ryan.neri', 'forward321', 'Ryan', 'Neri', '', 'ryan.neri@forward.ph', '2014-11-12 10:39:58', '2014-09-02 23:32:42', 3, 5, '20140902114115-1.jpg'),
(18, 'marse.reyes', 'forward321', 'Reyes', 'Marilyn', '09176551037', 'marse.reyes@forward.ph', '2014-10-07 02:24:09', '2014-09-03 16:01:47', 2, 1, NULL),
(19, 'forward_admin', 'admin', 'Caillot', 'Marc-olivier', '00000', 'marco.caillot@gmail.com', NULL, '2014-09-03 16:04:44', -1, 1, '20140903040445-1.png'),
(20, 'allan.pedernal', 'forward321', 'Pedernal', 'Allan', '', 'allan.pedernal@forward.ph', '2014-09-09 01:58:44', '2014-09-09 14:57:56', 3, 4, '20140909025756-1.png'),
(21, 'mike.barbadillo', 'forward321', 'Barbadillo', 'Mike', '', 'mike.barbadillo@forward.ph', '2014-11-10 03:26:25', '2014-11-07 07:47:43', 3, 4, '20141110051747-1.png'),
(22, 'charles.hernandez', 'forward321', 'Hernandez', 'Charles', '', 'charles.hernandez@forward.ph', '2014-11-12 10:25:26', '2014-11-07 07:47:43', 3, 4, '20141107111927-1.jpg'),
(23, 'bryan.giray', 'forward321', 'Giray', 'Bryan', '', 'bryan.giray@forward.ph', '2014-11-12 11:02:39', '2014-11-07 07:47:43', 3, 4, NULL),
(24, 'drex.astrande', 'forward321', 'Astrande', 'Drex', '', 'drex.astrande@forward.ph', NULL, '2014-11-07 07:47:43', 3, 4, '20141110090435-1.jpg'),
(25, 'ralph.orden', 'forward321', 'Orden', 'Ralph', '', 'ralph.orden@forward.ph', '2014-11-12 07:38:51', '2014-11-07 07:47:43', 3, 4, NULL),
(26, 'roane.villar', 'forward321', 'Villar', 'Roane', '', 'roane.villar@forward.ph', '2014-11-12 07:29:22', '2014-11-07 07:47:43', 4, 2, NULL),
(27, 'joyce.valdez', 'forward321', 'Valdez', 'Joyce', '', 'joyce.valdez@forward.ph', '2014-11-11 03:41:31', '2014-11-07 07:47:43', 4, 2, NULL),
(28, 'clemhyn.escosora', 'forward321', 'Escosora', 'Clemhyn', '', 'clemhyn.escosora@forward.ph', NULL, '2014-11-07 07:47:43', 5, 7, '20141111031848-1.jpg'),
(29, 'jayann.yu', 'forward321', 'Yu', 'Jay-ann', '', 'jayann.yu@forward.ph', '2014-11-12 03:06:38', '2014-11-07 07:47:43', 5, 7, '20141107082322-1.png'),
(30, 'erick.lucas', 'forward321', 'Lucas', 'Erick', '', 'erick.lucas@forward.ph', '2014-11-13 06:11:23', '2014-11-07 07:47:43', 5, 7, '20141111081512-1.png'),
(31, 'alfred.lucot', 'forward321', 'Lucot', 'Alfred', '', 'alfred.lucot@forward.ph', '2014-11-11 06:58:20', '2014-11-10 12:27:28', 6, 6, NULL),
(32, 'joma.giron', 'forward321', 'Giron', 'Joma', '', 'joma.giron@forward.ph', NULL, '2014-11-10 12:27:28', 6, 6, NULL),
(33, 'pauline.padlan', 'forward321', 'Padlan', 'Pauline', '', 'pauline.padlan@forward.ph', NULL, '2014-11-10 12:27:28', 6, 9, NULL),
(34, 'sheriegale.pardeno', 'forward321', 'Pardeno', 'Sheriegale', '', 'sheriegale.pardeno@forward.ph', '2014-11-09 10:53:09', '2014-11-10 12:27:28', 6, 9, NULL),
(35, 'vincent.villamil', 'forward321', 'Villamil', 'Vincent', '', ' vincent.villamil@forward.ph', '2014-11-12 01:36:25', '2014-11-10 12:27:28', 2, 10, NULL),
(36, 'ryan.magallanes', 'forward321', 'Magallanes', 'Ryan', '', 'ryan.magallanes@forward.ph', NULL, '2014-11-10 12:27:28', 6, 8, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

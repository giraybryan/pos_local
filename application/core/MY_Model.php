<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Base_Model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}
}
class MY_Model extends Base_Model {

	public function save_images( $id = 0, $params ) {
		
		$data['id'] = $id;
		
		
		$this->load->library('upload');
		
		
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '8000';
		$config['max_width']  = '4000';
		$config['max_height']  = '4000';
		$config['encrypt_name'] = TRUE;
		

		if( $id && ! empty( $_FILES ) ) {
			// print_r($_FILES); die;
			foreach( $_FILES as $name => $image_infomation ) {
			
				if(!empty($_FILES[$name]['name'])){
				
					$config['upload_path'] = 'uploads/' .$name;
					$this->upload->initialize( $config );
					
					if ( ! $this->upload->do_upload( $name ) ) {
					
						$error = $this->upload->display_errors();
						return $error;
					} else {
					
						$image = $this->upload->data();
						
						
						$data['image'] = $image['file_name'];
						$image_params = array(
							'table' => $params['module'],
							'data' => $data,
							'module' => 'core_files',
						);
						
						$this->dynamic_save( $image_params );
						
						if(!empty($params)){
							$file_upload_data = array(
									'orig_name'		=> $image['orig_name'],
									'file_name'		=> $image['file_name'],
									'module'		=> $params['module'],
									'module_id'		=> $params['id'],
									'date_created' 	=> 	date('Y-m-d H:i:s'),
								);
							
							$save_data = array(
								'table' 		=> 'core_files',
								'data'			=> $file_upload_data,
								'description'	=> 'Saved new Image',
								'module'		=> $params['module'],
							);
							
							$this->dynamic_save($save_data);
						}
						
					}
				
				}
				
				
			
			}
		
		}
	
	}
	
	public function dynamic_save($params = array()){
		error_reporting(E_ALL);
		if(!empty($params['data']['id'])){
			$id = $params['data']['id'];
			unset($params['data']['id']);
			$this->db->where('id', $id); 
			$this->db->update($params['table'], $params['data']);
		
			$action = 'updated';
			
		} else {
			$this->db->insert($params['table'], $params['data']);
			// echo $this->db->last_query();
			$id = $this->db->insert_id();
			$action = 'saved';
		}
		
		if(!empty($this->user_info))
			$this->record_log($id, $params, $action);
		
		return $id;
	}

	public function dynamic_fetch($params = array()){
		error_reporting(E_ALL);

		$this->db->from($params['table']);


		if(!empty($params['where'])){

			foreach ($params['where'] as $key => $value) {
				$this->db->where($key, $value);
			}
		}

		$row_data = $this->db->get()->row();
		
		return $row_data;
	}
	
	/* ALWAYS RECORD HISTORY / LOG */
	public function record_log( $id = 0, $params, $action = '' ) {
		// print_r($params);
		if( $id && $params && $action ) {
		
			$data['module_id'] 	= $id;
			$data['module'] 	= $params['module'];
			$data['action'] 	= $action;

			if(!empty($this->user_info['id'])){
				$data['created_by'] = $this->user_info['id'];
			} else {
				$data['created_by'] = $this->user_info['user_id'];
			}
			
			if(!empty($params['description'])){
				$data['description'] = $params['description'];
			}
			
			$this->db->insert( 'logs', $data );
		
		} else {
			
			$this->session->set_flashdata( 'error', 'Error recording history! Check your source code!' );
			redirect( base_url() );
		
		}
		
	}
	
	public function dynamic_delete($params = array()){
		
		
		if(!empty($params['table']) && !empty($params['where'])){
		
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
			$this->db->delete($params['table']);
			$result = $this->db->affected_rows();
			if(!empty($result)){
				$params['description'] = 'Deleted an item';
				$this->record_log($value, $params, 'deleted');
				$result =  'Item Deleted!';
				
			} else {
				$result = 'failed';
			}
			return  $result;
		}
		
		return $this->db->affected_rows();
	}
}
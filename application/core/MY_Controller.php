<?php
class Admin_Controller extends CI_Controller 
{
	public $user_info = array();
	function __construct()
	{
		parent::__construct();
		
		$this->load->model( array('User_model', 'Search_model','Company_model') );
		$this->load->helper( array('formatting', 'formatting_helper', 'form_helper', 'date_helper', 'email_helper', 'form', 'date', 'url', 'text', 'image_helper' ));
		$this->load->library( array('auth','form_validation', 'email', 'session', 'access', 'abstracts', 'pagination', 'useraccess' ));
		
		$logged_in = $this->auth->is_logged_in(false, false);
		
		

		
		 
		if (!$logged_in){
			redirect('login');
		} else {
			$this->user_info = $this->user_session->userdata('user');
			// print_r($this->user_info); die;
		}
	}
	 
	function view($view, $vars = array(), $string=false)
	{
		$user			= $this->user_session->userdata('user');

	
		if(!empty($user['user_id'])){
			$vars["user"] 	= $this->User_model->fetch($user['user_id']);
		} else {
			$vars["user"] 	= $this->User_model->fetch($user['id']);
		
		}
		
		if($string)
		{
			
			$result	 = $this->load->view('Partials/header', $vars, true);
			$result	.= $this->load->view($view, $vars, true);
			$result	.= $this->load->view('Partials/footer', $vars, true);
			
			return $result;
		}
		else
		{
			
			$this->load->view('Partials/header', $vars);
			$this->load->view($view, $vars);
			$this->load->view('Partials/footer', $vars);
		}
	}
	
	/*
	This function simple calls $this->load->view()
	*/
	function partial($view, $vars = array(), $string=false)
	{
		if($string)
		{
			return $this->load->view($view, $vars, true);
		}
		else
		{
			$this->load->view($view, $vars);
		}
	}
}

class Front_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->add_package_path(APPPATH.'themes/'.$this->config->item('theme').'/');
		
		#$this->load->model();
		$this->load->helper(array('form_helper', 'formatting_helper', 'date_helper', 'ckeditor'));
		$this->load->library(array('session', 'form_validation'));
	}//end __construct()
	
	/*
	This works exactly like the regular $this->load->view()
	The difference is it automatically pulls in a header and footer.
	*/
		
	function view($view, $vars = array(), $string=false)
	{					
		if($string)
		{
			$result	 = $this->load->view('header', $vars, true);
			$result	.= $this->load->view($view, $vars, true);
			$result	.= $this->load->view('footer', $vars, true);
			
			return $result;
		}
		else
		{
			$this->load->view('header', $vars);
			$this->load->view($view, $vars);
			$this->load->view('footer', $vars);
		}
	}
	
	/*
	This function simple calls $this->load->view()
	*/
	function partial($view, $vars = array(), $string=false)
	{
		if($string)
		{
			return $this->load->view($view, $vars, true);
		}
		else
		{
			$this->load->view($view, $vars);
		}
	}
}
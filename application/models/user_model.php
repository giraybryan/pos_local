<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model {
	
	public $tbl 	 			= 'users';
	public $pk_field 			= 'id';
	public $tbl_user_projects 	= 'user_projects';
	
	function __construct()
	{
		parent::__construct();
		$this->load->dbforge();
	}

	function fetch_list($params, $count = false)
	{	
		$this->db->join('positions', 'positions.position_id = users.position_id', 'left');
		$this->db->join('userlevels', 'userlevels.id = users.userlevel_id', 'left');
	
		if ($params['search'])
		{
			if(!empty($params['search']->term)){
				
				$search_fields = array($this->tbl.'.first_name', $this->tbl.'.last_name', $this->tbl.'.email');			
				$term = explode(' ', $params['search']->term);
				
				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$index	 = 0;
					$like	 = '';
					$like	.= "(";
					foreach ($search_fields as $field){
						$index++;
						
						$like .= " ".$field." ".$not."LIKE '%".$t."%' ";
						$like .= (count($search_fields) <= $index) ? "" : $operator;
					}
					$like	.= ") ";
					
					$this->db->where($like);
				}
			}
		}
		
		if($params['limit']>0)
		{
			$this->db->limit($params['limit'], $params['offset']);
		}
		if(!empty($params['sort_by']))
		{
			$this->db->order_by($params['sort_by'], $params['sort_order']);
		}

		if($count){
			return $this->db->count_all_results($this->tbl);
		}else{
			return $this->db->get($this->tbl)->result();
		}
	}
	
	function fetch($id)
	{				
        $this->db->where($this->pk_field, $id);
        return $this->db->get($this->tbl)->row();
    }
	
	function fetch_projects($id)
	{				
        $this->db->where("user_projects.".$this->pk_field, $id);
		$this->db->join("projects", "projects.project_id = user_projects.project_id", "inner");
        $result = $this->db->get($this->tbl_user_projects)->result();
		
		$arr 	= array();
		foreach($result as $obj)
		{
			$arr[] = $obj->project_id;
		}
		return $arr;
    }

    function fetch_login($username, $password)
    {
    	
    	$this->db->where('username', $username);
		$this->db->where('password', $password);
		return $this->db->get($this->tbl)->row_array();		
    }
       
    function fetch_all()
    {
    	$this->db->order_by($this->pk_field, 'DESC');
    	return $this->db->get($this->tbl)->result();
    }    
    
    function insert($data)
    {
		$this->db->where('username', $data['username']);
		$query = $this->db->get('users');
		
		if($query->num_rows() > 0)
		{			
			return false;
		}
		else
		{
			$this->db->insert($this->tbl, $data);
			return $this->db->insert_id();    	
		}
    }    
    
    function update($id, $data)
    {
        $this->db->where($this->pk_field, $id);
        $this->db->update($this->tbl, $data);
    }
    
    function delete($id)
    {
        $this->db->where($this->pk_field, $id); 
        return $this->db->delete($this->tbl);
    }  
	 
	function delete_user_projects($id)
    {
        $this->db->where($this->pk_field, $id);
        return $this->db->delete('user_projects');
    }
		
	function update_logs($id)
	{
		$data = array('last_login' => date("Y/m/d h:i:s"));
		
        $this->db->where('id', $id);
        $this->db->update($this->tbl, $data);
    }
	
	function update_user_projects($user_id, $projects)
	{
		$this->db->where('user_id', $user_id);
		$this->db->delete('user_projects');
		
		foreach($projects as $project_id)
		{
			$data["user_id"] 	= $user_id;
			$data["project_id"] = $project_id;
			
			$this->db->insert($this->tbl_user_projects, $data);
		}
	}
	
}
?>
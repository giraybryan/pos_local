<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends MY_Model
{
	public $tbl 			= 'reports';
	public $pk_field = 'id';

	function __construct()
	{
		parent::__construct();
		$this->load->dbforge();
	}
	
}
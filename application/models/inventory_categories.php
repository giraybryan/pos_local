<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory_categories extends CI_Model
{
	public $tbl 			= 'inventory_categories';
	public $pk_field = 'id';

	function __construct()
	{
		parent::__construct();
		$this->load->dbforge();
	}
	
	function fetch($params = array(), $count = false){
	
		$this->db->select('*');
		$this->db->from($this->tbl);
		
		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}
		
		if($count){
			return $this->db->get()->num_rows();
		}
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;
		
		
		
		
	
	}

	
	
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userlevels_model extends MY_Model
{
	public $tbl 			= 'useraccess_userlevels';
	public $user_permission = 'userlevel_permissions';
	public $pk_field = 'id';

	function __construct()
	{
		parent::__construct();
		$this->load->dbforge();
	}

	function fetch_all($params = array(), $count = false){

		$this->db->select($this->tbl.'.*');
		$this->db->from($this->tbl);
		
		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}
	
		
		if(!empty($params['limit'])){
			$this->db->limit($params['limit'], $params['offset']);
		}
		
		
		if(!empty($params['like'])) {
			foreach($params['like'] as $key => $value){
			
				$this->db->where($key.' LIKE ', "%$value%");

			}
		}
		if(!empty($params['or_like'])) {
			foreach($params['or_like'] as $key => $value){
				
				$this->db->or_where($key.' LIKE ', "%$value%");
			}
		}

		if($count){
			return $this->db->get()->num_rows();
		}
		
		
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;
	}
	
	function fetch_list($params, $count = false)
	{		
		if ($params['search'])
		{
			if(!empty($params['search']->term)){
				
				$search_fields = array($this->tbl.'.userlevel');			
				$term = explode(' ', $params['search']->term);
				
				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$index	 = 0;
					$like	 = '';
					$like	.= "(";
					foreach ($search_fields as $field){
						$index++;
						
						$like .= " ".$field." ".$not."LIKE '%".$t."%' ";
						$like .= (count($search_fields) <= $index) ? "" : $operator;
					}
					$like	.= ") ";
					
					$this->db->where($like);
				}
			}
		}
		
		if($params['limit']>0)
		{
			$this->db->limit($params['limit'], $params['offset']);
		}
		if(!empty($params['sort_by']))
		{
			$this->db->order_by($params['sort_by'], $params['sort_order']);
		}

		if($count){
			return $this->db->count_all_results($this->tbl);
		}else{
			return $this->db->get($this->tbl)->result();
		}
	}

	function get_row_by_id($userlevel_id = false ){
		
		if(empty($userlevel_id))
			return false;

		$this->db->select($this->tbl.'.*');
		$this->db->from($this->tbl);
		
		if(!empty($userlevel_id)){
				$this->db->where('id', $userlevel_id);
		}
	
	
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->row_array();
			} else {
				$result = $this->db->get()->row();
			}
		} else {
			$result = $this->db->get()->row();
		}
		
		return $result;
	}
	
	function get_all(){
		return $this->db->get($this->tbl)->result();
	}
	
	function add_level($data){
		$this->db->insert($this->tbl, $data);
		return $this->db->insert_id();
	}
	
	function update($id, $data) {
		$this->db->where($this->pk_field, $id);
		$this->db->update($this->tbl, $data);
	}
	
	function delete($id) {
		return $this->db->delete($this->tbl , array('id' => $id));
	}
	
	function check_title($data, $id = false){
		
		if(!$id) 
		{
			return $this->db->get_where($this->tbl, $data)->row();
		}
		else {
			$data = array_merge($data, array('id !=' => $id));
			return $this->db->get_where($this->tbl, $data);
		}
	}
	
	function fetch($userlevel_id){
		
		$this->db->select('id as userlevel_id, userlevel');
		$this->db->where($this->pk_field, $userlevel_id);
		return $this->db->get($this->tbl)->result();
	}
	function fetch_row($userlevel_id){
		$this->db->select('id as userlevel_id, userlevel_name');
		$this->db->where($this->pk_field, $userlevel_id);
		return $this->db->get($this->tbl)->row();
	}
	
}
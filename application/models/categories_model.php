<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends MY_Model
{
	public $tbl 			= 'menu_categories';
	public $pk_field = 'id';

	function __construct()
	{
		parent::__construct();
		$this->load->dbforge();
	}
	
	
	
	function fetch_all($params = array(), $count = false){
		
		if(!empty($params['fields'])){
			$this->db->select($params['fields'], FALSE);
		} else {
			$this->db->select('*');
		}
		$this->db->from($this->tbl);
		
		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}
	
		
		if(!empty($params['limit'])){
			$this->db->limit($params['limit'], $params['offset']);
		}
		
		if(!empty($params['order_by'])){
			foreach($params['order_by'] as $key => $value){
				$this->db->order_by($key, $value);
			}
		}
		
		if(!empty($params['like'])) {
			foreach($params['like'] as $key => $value){
			
				$this->db->where($key.' LIKE ', "%$value%");

			}
		}
		if(!empty($params['or_like'])) {
			foreach($params['or_like'] as $key => $value){
				
				$this->db->or_where($key.' LIKE ', "%$value%");
			}
		}

		if($count){
			return $this->db->get()->num_rows();
		}
		
		
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;
	
	}

	function fetch_optons($params = array(), $count = false){

		if(!empty($params['select'])){

		} else {
			$this->db->select('*');	
		}
		$this->db->from('menu_category_options');


		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}

		if($count){
			return $this->db->get()->num_rows();
		}
		
		
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;

	}

	function fetch_option($id = 0){

		if(!empty($params['select'])){

		} else {
			$this->db->select('*');	
		}
		$this->db->from('menu_category_options');


		if(!empty($id)){
			
			$this->db->where('id', $id);
			
		}
		$result = $this->db->get()->row();
		
		
		return $result;

	}
	
	
	
}
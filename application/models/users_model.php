<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends MY_Model
{
	public $tbl 			= 'users';
	public $pk_field = 'id';

	function __construct()
	{
		parent::__construct();
		$this->load->dbforge();
	}

	function fetch_all($params = array(), $count = false){
	
		$this->db->select($this->tbl.'.*, settings_branch.branch_name, userlevels.userlevel_name, settings_positions.position_name'); 
		$this->db->from($this->tbl);
		
		$this->db->join('settings_branch','settings_branch.id = '.$this->tbl.'.branch_id', 'left');
		$this->db->join('userlevels','userlevels.userlevel_id = '.$this->tbl.'.userlevel_id', 'left');
		$this->db->join('settings_positions','settings_positions.id = '.$this->tbl.'.position_id', 'left');
		
		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}
	
		
		if(!empty($params['limit'])){
			$this->db->limit($params['limit'], $params['offset']);
		}
		
		
		if(!empty($params['like'])) {
			foreach($params['like'] as $key => $value){
			
				$this->db->where($key.' LIKE ', "%$value%");

			}
		}
		if(!empty($params['or_like'])) {
			foreach($params['or_like'] as $key => $value){
				
				$this->db->or_where($key.' LIKE ', "%$value%");
			}
		}

		if($count){
			return $this->db->get()->num_rows();
		}
		
		
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;
	
	}
}
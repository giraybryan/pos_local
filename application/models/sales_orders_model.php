<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales_orders_model extends MY_Model
{
	public $tbl 			= 'sales';
	public $pk_field 		= 'id';
	
	private $_tbl_transaction_items = 'sales_transaction_items';
	private $_tbl_items 			= 'menus';

	function __construct()
	{
		parent::__construct();
		$this->load->dbforge();
	}

	function fetch_all($params = array(), $count = false){

		if(!empty($params['fields'])){
			$this->db->select($params['fields'], FALSE);

		} else {
			$this->db->select($this->tbl.'.*, employees.first_name, employees.last_name');

		}
	
		$this->db->from($this->tbl);
		
		$this->db->join('employees', 'employees.id = '.$this->tbl.'.employee_id', 'left');

		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}

		if(!empty($params['order_by'])){
			foreach($params['order_by'] as $key => $value){
				$this->db->order_by($key, $value);
			}
		}
	
		
		if(!empty($params['limit'])){
			$this->db->limit($params['limit'], $params['offset']);
		}
		
		
		if(!empty($params['like'])) {
			foreach($params['like'] as $key => $value){
			
				$this->db->where($key.' LIKE ', "%$value%");

			}
		}
		if(!empty($params['or_like'])) {
			foreach($params['or_like'] as $key => $value){
				
				$this->db->or_where($key.' LIKE ', "%$value%");
			}
		}

		if($count){
			return $this->db->get()->num_rows();
		}
		
		
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;
	
	}


	public function fetch_transactions($params = array(), $count = FALSE){
		
		if(!empty($params['fields'])){
			$this->db->select($params['fields'], FALSE);

		} else {
			$this->db->select('sales_transactions.*, employees.first_name, employees.last_name');

		}
	
		$this->db->from('sales_transactions');
		$this->db->join('employees', 'employees.id = sales_transactions.employee_id', 'left');
		
	
		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}
	
		
		if(!empty($params['limit'])){
			$this->db->limit($params['limit'], $params['offset']);
		}
		
		
		if(!empty($params['like'])) {
			foreach($params['like'] as $key => $value){
			
				$this->db->where($key.' LIKE ', "%$value%");

			}
		}
		if(!empty($params['or_like'])) {
			foreach($params['or_like'] as $key => $value){
				
				$this->db->or_where($key.' LIKE ', "%$value%");
			}
		}

		if($count){
			return $this->db->get()->num_rows();
		}
		
		
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;

	}

	function fetch_transaction($id){
		
		$this->db->select('sales_transactions.*,discounts.title as discount_title, discounts.reduction_type as discount_type, discounts.reduction_amount as discount_amount  , discount_coupons.title as coupon_title, discount_coupons.reduction_type as coupon_type, discount_coupons.reduction_amount as coupon_amount, promotions.title as promo_title, promotions.description as promo_description, promotions.promo_code, promotions.promo_item_ids, promotions.reduction_type as promo_type, promotions.reduction_amount as promo_amount ');
		$this->db->from('sales_transactions');


		$this->db->join('discounts','discounts.id = sales_transactions.DiscountID','left');
		$this->db->join('discount_coupons','discount_coupons.coupon_code = sales_transactions.CouponCode','left');
		$this->db->join('promotions','promotions.id = sales_transactions.promo_id','left');
	
		$this->db->where("sales_transactions.id", $id);
		return $this->db->get()->row();
	}
	
	public function fetch_transaction_details($id)
	{
		
		$this->db->select('sales_transaction_items.id as id, itemQty,itemPrice,title, ItemSubTotal, AdditionalInformation, RemoveInformation, SizeOptionID');
		
		$this->db->join($this->_tbl_items, "$this->_tbl_transaction_items.ItemID = $this->_tbl_items.id", "left");		
		$this->db->where("$this->_tbl_transaction_items.transaction_id", $id);
		$this->db->from($this->_tbl_transaction_items);
		return $this->db->get()->result();
	}

	public function fetch_total_sales($params = array()){

		$this->db->select('SUM(`TotalAmountDue`) as total_sales');
		$this->db->from('sales');

		if(!empty($params['where'])){
			
			foreach ($params['where'] as $key => $value) {
				$this->db->where($key, $value);
			}
			
		}

		return $this->db->get()->row();

	}
	
	
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company_model extends MY_Model
{
	public $tbl 			= 'settings_company_info';
	public $pk_field = 'id';

	function __construct()
	{
		parent::__construct();
		$this->load->dbforge();
	}


	function fetch_info(){
		$this->db->select('*, CONCAT("'.base_url().'","uploads/company_logo/", image) as logo', FALSE);
		$this->db->from('settings_company_info');
		return $this->db->get()->row();

	}
}
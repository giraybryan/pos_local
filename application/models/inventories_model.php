<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventories_model extends MY_Model
{
	public $tbl 			= 'inventories';
	public $pk_field = 'id';

	function __construct()
	{
		parent::__construct();
		$this->load->dbforge();

	}
	
	
	
	function fetch_all($params = array(), $count = false){
		

		if(!empty($params['fields'])){
			$this->db->select($params['fields'], FALSE);
		} else {
			$this->db->select('inventories.*, inventory_status.status as status_label, inventory_categories.name as category_name');

		}
		
		$this->db->from($this->tbl);
		
		if(!empty($params['where'])){

			if(is_array($params['where'])){

				foreach($params['where'] as $key => $value){
					$this->db->where($key, $value);
				}

			} else {

				$this->db->where($params['where']);
			}
			
		}

		if(!empty($params['where_statement']))
			$this->db->where($params['where_statement']);

		
		
		$this->db->join('inventory_status','inventory_status.id = inventories.status','left');
		$this->db->join('inventory_categories','inventory_categories.id = inventories.inventory_category_id','left');
		// print_R($params);
		
		if(!empty($params['limit'])){
			$this->db->limit($params['limit'], $params['offset']);
		}
		
		if(!empty($params['like'])) {
			foreach($params['like'] as $key => $value){
			
				$this->db->where($key.' LIKE ', "%$value%");

			}
		}
		if(!empty($params['or_like'])) {
			foreach($params['or_like'] as $key => $value){
				
				$this->db->or_where($key.' LIKE ', "%$value%");
			}
		}


		if(!empty($params['where_in'])){

			foreach($params['where_in'] as $key => $value){
				$this->db->where_in($key, $value);

			}
		}
		
		if($count){
			return $this->db->get()->num_rows();
		}
		
		if(!empty($params['order_by'])){
			foreach($params['order_by'] as $key => $value){
				$this->db->order_by($key, $value);
			}
		}
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;
	
	}
	
	
	function fetch_status($params = array(), $count = false){
		$this->db->select('*');
		$this->db->from('inventory_status');
		
		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}
		
		if($count){
			return $this->db->get()->num_rows();
		}
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;
	}

	function get_categories($params = array(), $count = false){
		$this->db->select('*');
		$this->db->from('inventory_categories');


		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}

		if(!empty($params['limit'])){
			$this->db->limit($params['limit'], $params['offset']);
		}
		

		if(!empty($params['like'])) {
			foreach($params['like'] as $key => $value){
			
				$this->db->where($key.' LIKE ', "%$value%");

			}
		}
		if(!empty($params['or_like'])) {
			foreach($params['or_like'] as $key => $value){
				
				$this->db->or_where($key.' LIKE ', "%$value%");
			}
		}

		if($count){
			return $this->db->get()->num_rows();
		}

		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}

		return $result;
	}

	function get_item_by_id($item_id = false){


		if(empty($item_id))
			return false;

		$this->db->select('inventories.*, inventory_status.status as status_label, inventory_categories.name as category_name');
		$this->db->from($this->tbl);
		
		$this->db->where('inventories.id', $item_id);
		
		$this->db->join('inventory_status','inventory_status.id = inventories.status','left');
		$this->db->join('inventory_categories','inventory_categories.id = inventories.inventory_category_id','left');
		// print_R($params);
		
		if(!empty($params['limit'])){
			$this->db->limit($params['limit'], $params['offset']);
		}
		
		
		if(!empty($params['like'])){
			foreach($params['like'] as $_key => $_val){
				$this->db->like($_key, $_val);
			}
		}
	
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->row_array();
			} else {
				$result = $this->db->get()->row();
			}
		} else {
			$result = $this->db->get()->row();
		}
		
		return $result;

	}

	function get_item_branch_params($params = array()){

		$this->db->select('*');
		$this->db->from('branch_inventory_log');
		
		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}
		
		if(!empty($params['order_by'])){
			foreach($params['order_by'] as $key => $value){
				$this->db->order_by($key, $value);
			}
		}
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->row_array();
			} else {
				$result = $this->db->get()->row();
			}
		} else {
			$result = $this->db->get()->row();
		}
		
		return $result;
	}

	function fetch_branch_item($params = array(), $count = false){
		

		if(!empty($params['fields'])){
			$this->db->select($params['fields'], FALSE);
		} else {
			$this->db->select('inventories.*, inventory_status.status as status_label, inventory_categories.name as category_name');

		}
		
		$this->db->from($this->tbl);
		
		
		$this->db->join('inventory_status','inventory_status.id = inventories.status','left');
		$this->db->join('inventory_categories','inventory_categories.id = inventories.inventory_category_id','left');
		$this->db->join('branch_inventory_items','branch_inventory_items.inventory_item_id = inventories.id','left');
		$this->db->join('branch_inventory_log','branch_inventory_log.id = branch_inventory_items.branch_log_id','left');
		// print_R($params);



		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}
		
		if(!empty($params['limit'])){
			$this->db->limit($params['limit'], $params['offset']);
		}
		
		
		if(!empty($params['like'])){
			foreach($params['like'] as $_key => $_val){
				$this->db->like($_key, $_val);
			}
		}
		if($count){
			return $this->db->get()->num_rows();
		}
		
		if(!empty($params['order_by'])){
			foreach($params['order_by'] as $key => $value){
				$this->db->order_by($key, $value);
			}
		}
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;
	
	}
}
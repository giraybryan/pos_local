<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchase_orders_model extends MY_Model
{
	public $tbl 			= 'purchase_orders';
	public $pk_field = 'id';

	function __construct()
	{
		parent::__construct();
		$this->load->dbforge();
	}

	function fetch_all($params = array(), $count = false){

		if(!empty($params['fields'])){
			$this->db->select($params['fields'], FALSE);

		} else {
			$this->db->select($this->tbl.'.*, inventories.item, inventories.threshold, inventories.image, inventories.stock, inventories.price, inventories.inventory_category_id, inventory_categories.name as category_name, employees.first_name, employees.last_name, settings_branch.branch_name');

		}
	
		$this->db->from($this->tbl);
		
		$this->db->join('inventories', 'inventories.id = '.$this->tbl.'.IngredientID', 'left');
		$this->db->join('inventory_categories', 'inventory_categories.id = inventories.inventory_category_id', 'left');
		$this->db->join('employees', 'employees.id = '.$this->tbl.'.RequestBy', 'left');
		$this->db->join('settings_branch', 'settings_branch.id = '.$this->tbl.'.BranchID', 'left');

		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}
	
		
		if(!empty($params['limit'])){
			$this->db->limit($params['limit'], $params['offset']);
		}
		
		
		if(!empty($params['like'])) {
			foreach($params['like'] as $key => $value){
			
				$this->db->where($key.' LIKE ', "%$value%");

			}
		}
		if(!empty($params['or_like'])) {
			foreach($params['or_like'] as $key => $value){
				
				$this->db->or_where($key.' LIKE ', "%$value%");
			}
		}

		if($count){
			return $this->db->get()->num_rows();
		}
		
		
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;
	
	}


	public function fetch_transactions($params = array(), $count = FALSE){
		
		if(!empty($params['fields'])){
			$this->db->select($params['fields'], FALSE);

		} else {
			$this->db->select('sales_transactions.*, employees.first_name, employees.last_name');

		}
	
		$this->db->from('sales_transactions');
		$this->db->join('employees', 'employees.id = sales_transactions.employee_id', 'left');
		
	
		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}
	
		
		if(!empty($params['limit'])){
			$this->db->limit($params['limit'], $params['offset']);
		}
		
		
		if(!empty($params['like'])) {
			foreach($params['like'] as $key => $value){
			
				$this->db->where($key.' LIKE ', "%$value%");

			}
		}
		if(!empty($params['or_like'])) {
			foreach($params['or_like'] as $key => $value){
				
				$this->db->or_where($key.' LIKE ', "%$value%");
			}
		}

		if($count){
			return $this->db->get()->num_rows();
		}
		
		
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;

	}
	
}
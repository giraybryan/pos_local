<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers_model extends MY_Model
{
	public $tbl 			= 'customers';
	public $pk_field = 'id';

	function __construct()
	{
		parent::__construct();
		$this->load->dbforge();
	}

	function fetch_all($params = array(), $count = false){
		

		if(!empty($params['fields'])){
			$this->db->select($params['fields']);
		} else {
			$this->db->select($this->tbl.'.*');

		}
		
		$this->db->from($this->tbl);
		
	//	$this->db->join('discount_type','discount_type.id = '.$this->tbl.'.reduction_type','left');

		
		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}
	
		
		if(!empty($params['limit'])){
			$this->db->limit($params['limit'], $params['offset']);
		}
		
		
		if(!empty($params['like'])) {
			foreach($params['like'] as $key => $value){
			
				$this->db->where($key.' LIKE ', "%$value%");

			}
		}
		if(!empty($params['or_like'])) {
			foreach($params['or_like'] as $key => $value){
				
				$this->db->or_where($key.' LIKE ', "%$value%");
			}
		}

		if($count){
			return $this->db->get()->num_rows();
		}
		
		
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;
	
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menus_model extends MY_Model
{
	public $tbl 			= 'menus';
	public $pk_field = 'id';

	function __construct()
	{
		parent::__construct();
		$this->load->dbforge();
	}


	function fetch_all($params = array(), $count = false){
		if(!empty($params['fields'])){
			$this->db->select($params['fields'], FALSE);

		} else {
			$this->db->select($this->tbl.'.*, menu_status.status_name');

		}
	
		$this->db->from($this->tbl);
		
		$this->db->join('menu_status', 'menu_status.id = menus.status_id', 'left');

		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}
	
		
		if(!empty($params['limit'])){
			$this->db->limit($params['limit'], $params['offset']);
		}
		
		
		if(!empty($params['like'])) {
			foreach($params['like'] as $key => $value){
			
				$this->db->where($key.' LIKE ', "%$value%");

			}
		}
		if(!empty($params['or_like'])) {
			foreach($params['or_like'] as $key => $value){
				
				$this->db->or_where($key.' LIKE ', "%$value%");
			}
		}

		if(!empty($params['where_in'])){

			foreach($params['where_in'] as $key => $value){
				$this->db->where_in($key, $value);

			}
		}


		if(!empty($params['order_by'])){
			foreach($params['order_by'] as $key => $value){
				$this->db->order_by($key, $value);
			}
		}

		
		if($count){
			return $this->db->get()->num_rows();
		}
		
		
		
		
		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;
	
	}

	function get_menu_by_id($id){

		$this->db->select($this->tbl.'.*, menu_status.status_name, menu_categories.menu_category_name');
		$this->db->from($this->tbl);

		$this->db->join('menu_status', 'menu_status.id = menus.status_id', 'left');
		$this->db->join('menu_categories', 'menu_categories.id = menus.menu_category_id', 'left');

		$this->db->where($this->tbl.'.id', $id);
		

		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->row_array();
			} else {
				$result = $this->db->get()->row();
			}
		} else {
			$result = $this->db->get()->row();
		}
		
		return $result;
	}

	function get_ingredients($menu_id){

		$this->db->select('menu_ingredients.menu_id, menu_ingredients.qty, inventories.*');
		$this->db->from('inventories');

		$this->db->join('menu_ingredients', 'menu_ingredients.ingredient_id = inventories.id', 'left');

		$this->db->where('menu_ingredients'.'.menu_id', $menu_id);
		

		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;
	}

	function get_addons($menu_id){

		$this->db->select('menu_addons.menu_id, inventories.*');
		$this->db->from('inventories');

		$this->db->join('menu_addons', 'menu_addons.addon_id = inventories.id', 'left');

		$this->db->where('menu_addons'.'.menu_id', $menu_id);
		

		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;
	}

	function get_excludes($menu_id){
		$this->db->select('menu_excludes.menu_id, inventories.*');
		$this->db->from('inventories');

		$this->db->join('menu_excludes', 'menu_excludes.item_id = inventories.id', 'left');

		$this->db->where('menu_excludes'.'.menu_id', $menu_id);
		

		if(!empty($params['type'])){
			if($params['type'] == 'array'){
				$result = $this->db->get()->result_array();
			} else {
				$result = $this->db->get()->result();
			}
		} else {
			$result = $this->db->get()->result();
		}
		
		return $result;
	}



}
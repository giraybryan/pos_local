<?php
/******************************************
US English
Common Language
******************************************/

$lang['account_settings']																	= 'Account settings';
$lang['logout']																				= 'Logout';
$lang['login']																				= 'Login';
$lang['dashboard']																			= 'Dashboard';
$lang['property']																			= 'Properties';
$lang['client']																				= 'Clients';
$lang['my_survey']																			= 'My Surveys';
$lang['my_folders']																			= 'My Folders';
$lang['my_reports']																			= 'My Reports';
$lang['create_new_survey']																	= 'Create New Survey';
$lang['survey_title']																		= 'Survey Title';
$lang['date_created']																		= 'Date Created';
$lang['last_modified']																		= 'Last Modified';
$lang['quick_links']																		= 'Quick Links';
$lang['respondents']																		= 'Respondents';
$lang['progress']																			= 'Progress';
$lang['apply']																				= 'Apply';
$lang['cancel']																				= 'Cancel';
$lang['next']																				= 'Next';
$lang['preview']																			= 'Preview';
$lang['save_draft']																			= 'Save draft';
$lang['no_file_chosen']																		= 'No file chosen';
$lang['choose_file']																		= 'Choose file';
$lang['upload']																				= 'Upload';
$lang['add']																				= 'Add';
$lang['edit']																				= 'Edit';
$lang['delete']																				= 'Delete';
$lang['save']																				= 'Save';
$lang['yes']																				= 'Yes';
$lang['no']																					= 'No';


$lang['default_select_option'] = '---Select One---';


<?php
/******************************************
US English
Login Language
******************************************/

$lang['error_authentication_failed']	= 'Authentication Failed!';
$lang['message_logged_out']				= 'You have been logged out.';
$lang['login']							= 'Login';
$lang['enter']							= 'Enter';
$lang['register']						= 'Register';
$lang['password']						= 'Password';
$lang['username']						= 'Username';
$lang['remember']						= 'Remember Me';
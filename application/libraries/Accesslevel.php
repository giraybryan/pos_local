<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accesslevel {
	
	
	public function __construct(){
		
		$CI =& get_instance();
		$CI->load->database();
		$CI->load->library('encrypt');
	}
	
	function getModules()
	{
		return array(
			1=>'List',
			2=>'Contact',
			3=>'Lead',
			4=>'Appointment',
			//5=>'Marketing',
			5=>'Tripping',
			6=>'Closing',
		);
	}
	function getModuleMap()
	{
		return array(
			8=>'Users',
			9=>'Userlevels',
			10=>'Permissions',
			11=>'Lists',
			12=>'Quotas',
			
		);
	}
	
	
	function is_list_stage($stage){
	
		$list_stage = array('list','contact');
		
		if(in_array($stage, $list_stage)){
			return 1;
		} else {
			return 0;
		}
	}
	
	function getStagesPermissionTypes()
	{
		return array(
			1=>'View',
			2=>'Add',
			3=>'Edit',
			4=>'Delete',
		);
	}
	
	function ValidateAccess($permission, $permission_type,$tag = false){
		
		$CI =& get_instance();
		$user = $CI->session->userdata('user');
		$userlevel_id = $user['userlevel_id'];
		
		$permissions = $this->getStagesPermissionTypes();
		$permission_type_value = array_search($permission_type, $permissions); // $key = 2;
		//$userlevel_id = 1;
		
		if($userlevel_id == -1 || $userlevel_id == 0){
		
			return 1;
			
		} else {
		
			

			
			$result = $CI->db->select('*')
					->from('userlevel_permissions')
					->where('module_type', $permission)
					->where('permission', $permission_type_value)
					->where('userlevel_id', $userlevel_id)
					->get()->row();
					//echo $CI->db->last_query();
			
			if($tag) {
				/* print_r($result);
				die($permission.' '.$permission_type); */
			}
			
			if(!empty($result)){
				return 1;
			} else {
				return false;
			}
		}
	}
	
	function getcurrent_permission($userlevel_id, $module_name, $permission){
		$CI =& get_instance();
		$CI->db->select('*')
				->from('userlevel_permissions')
				->where('userlevel_id', $userlevel_id)
				->where('module_type', $module_name)
				->where('permission', $permission);
		$result = $CI->db->get()->row();
		if(!empty($result)){
			return 1;
		}
		
	}
}
// END Access class

/* End of file Access.php */
/* Location: ./Application/libraries/Access.php */
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth {
	
	private $table_name = 'users';

	var $CI;
	
	//this is the expiration for a non-remember session
	var $session_expire	= 36000; //7200;
	
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->database();
		$this->CI->load->library('encrypt');
		$this->CI->load->model('User_model');
		
		$user_session_config = array(
		    'sess_cookie_name' => 'user_session_config',
		    'sess_expiration' => 0
		);
		$this->CI->load->library('session', $user_session_config, 'user_session');
	}
	
	public function is_logged_in($redirect = false, $default_redirect = true){
		$user = $this->CI->user_session->userdata('user');
		
		if (!$user){
			if ($redirect){
				$this->CI->user_session->set_flashdata('redirect', $redirect);
			}
				
			if ($default_redirect){	
				redirect('login');
			}
			
			return false;
		}else{
			//check if the session is expired if not reset the timer
			if($user['expire'] && $user['expire'] < time()){
				$this->logout();
				if($redirect){
					$this->CI->user_session->set_flashdata('redirect', $redirect);
				}

				if($default_redirect){
					redirect('login');
				}

				return false;
			}else{
				//update the session expiration to last more time if they are not remembered
				if($user['expire']){
					$user['expire'] = time()+$this->session_expire;
					$this->CI->user_session->set_userdata(array('user'=>$user));
				}
			}
			return true;
		}
	}
	
	
	/*public function is_allowed($module, $redirect=false)
	{
		$user        = $this->CI->user_session->userdata('user');		
		$permissions = array_map("strtolower", explode(",",$user['modules']));

		if($redirect)
		{
			if( !in_array(strtolower($module), $permissions) )
			{
				redirect(admin_url());
			}
		}
		return in_array(strtolower($module), $permissions);
	}*/
	
	/*
	this function does the logging in.
	*/
	function login_user($username, $password, $remember=false)
	{
			
		$user = $this->CI->User_model->fetch_login($username, $password);
				
		if ( !empty($user) )
		{ 
									
			if($remember <> "")
			{
				$user['expire'] = time()+$this->session_expire;
			}
			else
			{
				$user['expire'] = false; 
			}
						
			
			$this->CI->user_session->set_userdata('user', $user);
			
			
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/*
	this function does the logging out
	*/
	function logout()
	{
		$this->CI->user_session->unset_userdata('user');
		$this->CI->user_session->sess_destroy();
	}

	/*
	This function resets the users password and emails them a copy
	*/
	function reset_password($username)
	{
		$user = $this->get_user_by_username($username);
		if ($user)
		{
			$this->CI->load->helper('string');
			$this->CI->load->library('email');
			
			$new_password		= random_string('alnum', 8);
			$user['password']	= $new_password;
			$this->save($user);
			
			$this->CI->email->from($this->CI->config->item('email'), $this->CI->config->item('site_name'));
			$this->CI->email->to($user['email']);
			$this->CI->email->subject($this->CI->config->item('site_name').': User Password Reset');
			$this->CI->email->message('Your password has been reset to '. $new_password .'.');
			$this->CI->email->send();
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/*
	This function gets the user by their email address and returns the values in an array
	it is not intended to be called outside this class
	*/
	private function get_user_by_username($username)
	{
		$this->CI->db->select('*');
		$this->CI->db->where('username', $username);
		$this->CI->db->limit(1);
		$result = $this->CI->db->get($this->table_name);
		$result = $result->row_array();

		if (sizeof($result) > 0)
		{
			return $result;	
		}
		else
		{
			return false;
		}
	}
	
	/*
	This function takes user array and inserts/updates it to the database
	*/
	function save($user)
	{
		if ($user['user_id'])
		{
			$this->CI->db->where('user_id', $user['user_id']);
			$this->CI->db->update($this->table_name, $user);
		}
		else
		{
			$this->CI->db->insert($this->table_name, $user);
		}
	}
	
	
	/*
	This function gets a complete list of all user
	*/
	function get_user_list()
	{
		$this->CI->db->select('*');
		$this->CI->db->order_by('last_name', 'ASC');
		$this->CI->db->order_by('first_name', 'ASC');
		$this->CI->db->order_by('email', 'ASC');
		$result = $this->CI->db->get($this->table_name);
		$result	= $result->result();
		
		return $result;
	}

	/*
	This function gets an individual user
	*/
	function get_user($id)
	{
		$this->CI->db->select('*');
		$this->CI->db->where('user_id', $id);
		$result	= $this->CI->db->get($this->table_name);
		$result	= $result->row();

		return $result;
	}		
	
	function check_id($str)
	{
		$this->CI->db->select('user_id');
		$this->CI->db->from($this->table_name);
		$this->CI->db->where('user_id', $str);
		$count = $this->CI->db->count_all_results();
		
		if ($count > 0)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	function check_email($str, $id=false)
	{
		$this->CI->db->select('email');
		$this->CI->db->from($this->table_name);
		$this->CI->db->where('email', $str);
		if ($id)
		{
			$this->CI->db->where('user_id !=', $id);
		}
		$count = $this->CI->db->count_all_results();
		
		if ($count > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/*
	This function deletes an individual user
	*/
	function delete($id)
	{
		if ($this->check_id($id))
		{
			$user	= $this->get_user($id);
			$this->CI->db->where('user_id', $id);
			$this->CI->db->limit(1);
			$this->CI->db->delete($this->table_name);

			return $user->firstname.' '.$user->lastname.' has been removed.';
		}
		else
		{
			return 'The user could not be found.';
		}
	}
}
// END Auth class

/* End of file Auth.php */
/* Location: ./Application/libraries/Auth.php */

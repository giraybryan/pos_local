<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Abstracts {
	
	public $tbl_positions 	= 'positions';
	public $tbl_userlevels 	= 'userlevels';
	public $tbl_projects 	= 'projects';
	public $tbl_users	 	= 'users';
	public $tbl_status	 	= 'status';
	public $tbl_stages	 	= 'project_stages';
	
	var $CI;
	
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->database();
		
		$this->CI->load->library( 'access');
	}
	
	public function get_positions()
	{
		$result = $this->CI->db->get($this->tbl_positions)->result();
		$arr 	= array();
		foreach($result as $obj)
		{
			$arr[$obj->position_id] = $obj->position_name;
		}
		return $arr;
	}
	
	public function get_userlevels()
	{
		$result = $this->CI->db->get($this->tbl_userlevels)->result();
		$arr 	= array();
		foreach($result as $obj)
		{
			$arr[$obj->id] = $obj->userlevel_name;
		}
		return $arr;
	}
	
	public function get_projects()
	{
		$result = $this->CI->db->get($this->tbl_projects)->result();
		$arr 	= array();
		foreach($result as $obj)
		{
			$arr[$obj->project_id] = $obj->project_name;
		}
		return $arr;
	}
	
	public function get_users()
	{
		$result = $this->CI->db->get($this->tbl_users)->result();
		$arr 	= array();
		foreach($result as $obj)
		{
			$arr[$obj->user_id] = $obj->first_name." ".$obj->last_name;
		}
		return $arr;
	}
	
	public function get_users_rec()
	{
		$result = $this->CI->db->get($this->tbl_users)->result();
		$arr 	= array();
		foreach($result as $obj)
		{
			$user 	= array();
			$user["name"] 	= $obj->first_name." ".$obj->last_name;
			$user["level"] 	= $obj->userlevel_id;
			
			$arr[$obj->user_id] = (object)$user;
		}
		return $arr;
	}
	
	public function get_status()
	{
		$result = $this->CI->db->get($this->tbl_status)->result();
		$arr 	= array();
		foreach($result as $obj)
		{
			$arr[$obj->status_id] = $obj->status_name;
		}
		return $arr;
	}
	
	public function get_stages()
	{
		$result = $this->CI->db->get($this->tbl_stages)->result();
		$arr 	= array();
		foreach($result as $obj)
		{
			$arr[$obj->stage_id] = $obj->stage_name;
		}
		return $arr;
	}
	
	public function set_security_access($module)
	{	
		
		$data["CanView"]	= $this->CI->access->ValidateAccess($module, "view");
		$data["CanAdd"]		= $this->CI->access->ValidateAccess($module, "add");
		$data["CanEdit"]	= $this->CI->access->ValidateAccess($module, "edit");
		$data["CanDelete"]	= $this->CI->access->ValidateAccess($module, "delete");
		
		$tables = $this->CI->access->DynamicTables();
		
		foreach($tables as $table)
		{
			$data["CanView_".$table]	= $this->CI->access->ValidateAccess($table, "view");
		}
		
		return (object)$data;
	}
}
// END Abstracts class

/* End of file Abstracts.php */
/* Location: ./Application/libraries/Abstracts.php */

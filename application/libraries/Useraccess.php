<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* AUTHOR : Bryan GIRAY  
*/
class Useraccess {
	
	
	public function __construct(){
		
		$CI =& get_instance();
		$CI->load->database();
		$CI->load->library('encrypt');
	}
	
	
	function getUserlevels(){
		$CI =& get_instance();
		$CI->load->database();
		
		$userlevels_obj = $CI->db->get('useraccess_userlevels')->result_array();
		
		$userlevels = array();
		if(!empty($userlevels_obj))
		{
			foreach($userlevels_obj as $key_userlevel => $val_userlevel)
			{
				$userlevels[$key_userlevel] = $val_userlevel;
			}
		}
		
		return $userlevels;
	}
	
	function getModules()
	{
	
		$CI =& get_instance();
		$CI->load->database();
		
		$module_obj = $CI->db->get('useraccess_modules')->result_array();
		
		$modules = array();
		if(!empty($module_obj))
		{
			foreach($module_obj as $val_module)
			{
				
				$modules[$val_module['id']] = $val_module['module']; 
			}
		}
		//echo "<pre>"; print_r($modules); die;
		return $modules;
	}
	
	function getPermissionTypes()
	{
		$CI =& get_instance();
		$CI->load->database();
		
		$permission_types = $CI->db->get('useraccess_permission_types')->result_array();
		
		//echo '<pre>'; print_r($permission_types); 
		$permissions = array();
		if(!empty($permission_types))
		{
			foreach($permission_types as $key_permission => $val_permission)
			{
				$permissions[$key_permission] = $val_permission;
			}
		}
		// echo "<pre>"; print_r($permissions); die;
		return $permissions;

	}
	
	function is_list_stage($stage){
	
		$list_stage = array('list','contact');
		
		if(in_array($stage, $list_stage)){
			return 1;
		} else {
			return 0;
		}
	}
	
	function ValidateAccess($module, $permission_type,$tag = false){
	
		$CI =& get_instance();	
		$user = $CI->session->userdata('user');
		$userlevel_id = $user['userlevel_id'];
		$permissions = $this->getPermissionTypes();

		$permission_type_value = array_search($permission_type, $permissions); // $key = 2;
		//$userlevel_id = 1;
		
		if($userlevel_id == -1 || $userlevel_id == 0){
		
			return 1;
			
		} else {
	
			$result = $CI->db->select('*')
					->from('userlevel_permissions')
					->where('module_type', $module)
					->where('permission', $permission_type_value)
					->where('userlevel_id', $userlevel_id)
					->get()->row();
					//echo $CI->db->last_query();
			
			if($tag) {
				/* print_r($result);
				die($permission.' '.$permission_type); */
			}
			
			if(!empty($result)){
				return 1;
			} else {
				return false;
			}
		}
	}
	
	function getcurrent_permission($userlevel_id, $module_name, $permission){
	
		$module_name = str_replace(' ','_', $module_name);
		if($userlevel_id == -1)
			return 1;
	
		$CI =& get_instance();
		$CI->db->select('*')
				->from('userlevel_permissions')
				->where('userlevel_id', $userlevel_id)
				->where('module_type', $module_name)
				->where('permission', $permission);
		$result = $CI->db->get()->row();
		if(!empty($result)){
			return 1;
		}
		
	}
}
// END Access class

/* End of file Access.php */
/* Location: ./Application/libraries/Access.php */
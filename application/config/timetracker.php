<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// business theme
$config['theme']						= 'matcher';

// SSL support
$config['ssl_support']					= false;

// Business information
$config['company_name']					= 'Forward Time Tracker';
$config['address1']						= '';
$config['address2']						= '';
$config['country']						= ''; // use proper country codes only
$config['city']							= ''; 
$config['state']						= '';
$config['zip']							= '';
$config['email']						= 'jayson.sonido@ctsweb.fr';

// Store currency
$config['currency']						= 'PHP ';  // USD, EUR, etc
$config['currency_symbol']				= 'Php '; //'&euro;' '&#x20b1';
$config['currency_symbol_side']			= ''; // anything that is not "left" is automatically right
$config['currency_decimal']				= '.';
$config['currency_thousands_separator']	= ',';

// Shipping config units
$config['weight_unit']	    			= 'LB'; // LB, KG, etc
$config['dimension_unit']   			= 'IN'; // FT, CM, etc

//program name
$config['program_name'] 				= 'Forward';

$config['site_name'] 					= 'Forward';
$config['admin_folder'] 				= 'admin';

//file upload size limit
$config['size_limit']					= intval(ini_get('upload_max_filesize'))*1024;

//are new registrations automatically approved (true/false)
$config['new_customer_status']			= true;

//do we require customers to log in 
$config['require_login']				= false;

$config['image_upload_path'] 			= 'uploads/images/';

$config['default_images_no'] 			= 8;

$config['fb_app_id'] 					= '289603234519583';
$config['fb_app_secret'] 		    	= '638cd855f0401d2c88c0cacde04b6f0c';
$config['fb_app_cookie'] 		    	= true;

$config['rows_per_page']				= 20;
$config['website']						= "http://forward.ph/";

//path to where images is uploaded
$config['user_upload_path'] 			= 'uploads/users/';
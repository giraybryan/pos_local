<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$config['image_library']  = 'gd2';
$config['maintain_ratio'] = FALSE;

/* $config['width']  = 900;
$config['height'] = 570;
 */
/* $config['thumb_width']  = 265;
$config['thumb_height'] = 175; */
$config['thumb_width']  = 200;
$config['thumb_height'] = 200;


$config['map_img_width']  = 110;
$config['map_img_height'] = 90;

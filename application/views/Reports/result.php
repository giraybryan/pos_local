<div class="row">
	<div class="col-sm-2">Prepared By: </div><div class="col-sm-7"><?php echo $this->user_info['first_name'].' '.$this->user_info['last_name'] ?></div>
	<div class="col-sm-1">

		<?php  if(!empty($sales)): ?>
		<?php  $export_url =  base_url('reports/export_pdf').'?employee_id='.urlencode($_POST['employee_id']).'&shift='.urlencode($_POST['shift']).'&start_date='.urlencode($_POST['start_date']).'&end_date='.urlencode($_POST['end_date']); ?>
		<a href="<?php echo $export_url ?>" target="_blank" class="btn btn-primary">Export PDF</a>
		<?php endif; ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-2">Date: </div><div class="col-sm-9"><?php echo date('Y-m-d') ?></div>
</div>
<?php  if(!empty($sales)): ?>
		<?php $grand_total = 0; ?>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Employee</th>
					<th>Shift</th>
					<th>Date of Sales</th>
					<th>Total Sales</th>
				</tr>
			</thead>
		<?php foreach($sales as $sale):?>
				<tr>
					<td><?php echo $sale->first_name.' '.$sale->last_name ?></td>
					<td><?php echo ($sale->ShiftNumber == 1 ? 'Day Shift': 'Night shift') ?></td>
					<td><?php echo date('Y-m-d', strtotime($sale->date_created) )  ?></td>
					<td><?php echo format_currency($sale->TotalAmountDue) ?></td>
				</tr>

			<?php $grand_total = $grand_total + $sale->TotalAmountDue; ?>
		<?php endforeach;?>
			<tfoot>
				<tr>
					<th colspan="3">Grand total</th>
					<th > <?php echo format_currency($grand_total); ?></th>
				</tr>
			</tfoot>
		</table>

<?php else: ?>
	<div class="alert alert-warning">
		No sales for the filtered information
	</div>
<?php  endif; ?>
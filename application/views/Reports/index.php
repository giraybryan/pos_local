<div id="sales-list-wrapper" class="col-sm-12 right-panel">
	<div class="container-fluid widget-content list-container">
		<div class="page-title">
			<h4><?php echo $page?></h4>
		</div>
	<form method="POST" action="reports">
		<div class="row">
			<div class="col-sm-3">
				<select class="select" name="employee_id" placeholder="Select Employee">
					<option value=""></option>
					<?php foreach($employees as $employee): ?>
					<option <?php echo ( !empty($_POST['employee_id']) && $_POST['employee_id'] == $employee->id ? 'selected' : '') ?> value="<?php echo $employee->id?>"><?php echo $employee->first_name.' '.$employee->last_name ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="col-sm-3">
				<select class="select" placeholder="Select Shift" name="shift">
					<option value=""></option>
					<option value="1" <?php echo ( !empty($_POST['shift']) && $_POST['shift'] == 1 ? 'selected' : '') ?>>Day Shift</option>
					<option value="2" <?php echo ( !empty($_POST['shift']) && $_POST['shift'] == 2 ? 'selected' : '') ?>>Night Shift</option>
					
				</select>
			</div>
			<div class="col-sm-2">
				<input type="text" class="form-control datepick" name="start_date" placholder="Sales Starts from"/> 
			</div>
			<div class="col-sm-1">
				to
			</div>
			<div class="col-sm-2">
				<input type="text" class="form-control datepick" name="end_date" placholder="Sales to" /> 
			</div>
			<div class="col-sm-1">
				<input type="submit" value="Go" class="btn btn-primary"/>
			</div>
		</div>
	</form>


		<?php echo $this->load->view('Reports/result'); ?>
	</div>
</div>

 <link href="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.min.css')?>"  rel="stylesheet">
 <script type="text/javascript" src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.min.js');?>"></script>
 <script type="text/javascript" src="<?php echo base_url('assets/js/modules/reports.js');?>"></script>

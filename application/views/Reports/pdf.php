<style>
.row::before, .row::after {
    content: " ";
    display: table;
}
.row::before, .row::after {
    content: " ";
    display: table;
}
*, *::before, *::after {
    box-sizing: border-box;
}
.row::after {
    clear: both;
}
.row::before, .row::after {
    content: " ";
    display: table;
}
.row::after {
    clear: both;
}
.row::before, .row::after {
    content: " ";
    display: table;
}
*, *::before, *::after {
    box-sizing: border-box;
}
.row {
    margin-left: -15px;
    margin-right: -15px;
}
.col-sm-2 {
    width: 16.6667%;
}
.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
    float: left;
}
.col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
    min-height: 1px;
    padding-left: 15px;
    padding-right: 15px;
    position: relative;
}
.col-sm-7 {
    width: 58.3333%;
}
.col-sm-1 {
    width: 8.33333%;
}
.col-sm-9 {
    width: 75%;
}
.table-bordered {
    border: 1px solid #dddddd;
}
.table {
    margin-bottom: 20px;
    width: 100%;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}
table {
    background-color: transparent;
    max-width: 100%;
}
.table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > th, .table > caption + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > td, .table > thead:first-child > tr:first-child > td {
    border-top: 0 none;
}
.table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
    border-bottom-width: 2px;
}
.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid #dddddd;
}
.table > thead > tr > th {
    border-bottom: 2px solid #dddddd;
    vertical-align: bottom;
}
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    border-top: 1px solid #dddddd;
    line-height: 1.42857;
    padding: 8px;
    vertical-align: top;
}
.table thead th {
    background-repeat: repeat-x;
    border-color: transparent transparent #3ac8fd !important;
    border-style: solid none !important;
    border-width: 0 0 2px !important;
    color: #2b3138;
    font-family: open_sansregular;
    font-size: 12px;
    height: 16px !important;
    padding: 10px !important;
}
th {
    text-align: left;
}
.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid #dddddd;
}
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    border-top: 1px solid #dddddd;
    line-height: 1.42857;
    padding: 8px;
    vertical-align: top;
}
.table td {
    font-size: 12px;
    padding: 10px;
    vertical-align: inherit !important;
}

.alert-warning {
    background-color: #fcf8e3;
    border-color: #faebcc;
    color: #8a6d3b;
}
.alert {
    border: 1px solid transparent;
    border-radius: 4px;
    margin-bottom: 20px;
    padding: 15px;
}
</style>


<table style='margin-bottom: 10px'>
	<tr>
		<td>Prepare By: </td>
		<td><?php echo $this->user_info['first_name'].' '.$this->user_info['last_name'] ?> </td>
	</tr>
	<tr>
		<td>Date </td>
		<td><?php echo date('Y-m-d') ?></td>
	</tr>


</table>
<table class="table" style='margin-bottom: 10px'>
	<thead>
		<tr>
			<th colspan="2">Filtered By</th>
		</tr>
	</thead>

	<?php if( !empty($employee)):?>
	<tr>
		<td>Employee Sales</td>
		<td>
			<?php echo $employee->first_name.' '.$employee->last_name ?>
			
		</td>
	</tr>
	<?php endif; ?>

	<?php if( !empty($_GET['shift'])): ?>
	<tr>
		<td>Type of Shift</td>
		<td>
			<?php echo ($_GET['shift'] == 1 ? 'Day Shift' : 'Night Shift') ?>
			
		</td>
	</tr>
	<?php endif; ?>

	<?php if( !empty($_GET['start_date']) || !empty($_GET['end_date']) ):?>
	<tr>
		<td>Date Range</td>
		<td>
			<?php echo (!empty($_GET['start_date']) ? 'From '.date('Y-m-d', strtotime($_GET['start_date'])): '' ) ?>
			<?php echo (!empty($_GET['end_date']) ? 'to '.date('Y-m-d', strtotime($_GET['end_date'])): '' ) ?>
		</td>
	</tr>

	<?php endif; ?>
</table>
<h3>Sales</h3>
<?php  if(!empty($sales)): ?>
		<?php $grand_total = 0; ?>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Employee</th>
					<th>Shift</th>
					<th>Date of Sales</th>
					<th>Total Sales</th>
				</tr>
			</thead>
			<tbody>
		<?php foreach($sales as $sale):?>
				<tr>
					<td><?php echo $sale->first_name.' '.$sale->last_name ?></td>
					<td><?php echo ($sale->ShiftNumber == 1 ? 'Day Shift': 'Night shift') ?></td>
					<td><?php echo date('Y-m-d', strtotime($sale->date_created) )  ?></td>
					<td><?php echo format_currency($sale->TotalAmountDue) ?></td>
				</tr>

			<?php $grand_total = $grand_total + $sale->TotalAmountDue; ?>
		<?php endforeach;?>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="3" style='text-align: right'>Grand total</th>
					<th > <?php echo format_currency($grand_total); ?></th>
				</tr>
			</tfoot>
		</table>

<?php else: ?>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Employee</th>
				<th>Shift</th>
				<th>Date of Sales</th>
				<th>Total Sales</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="4">
					<div class="alert alert-warning">
						No sales 
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	
<?php  endif; ?>
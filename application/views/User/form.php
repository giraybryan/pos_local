<div class="form" id="user_form">
    <div class="row">
    <?php
		$attributes = array('class' => 'form-userlevel');
		
		$method = "edit";
		if($user->userlevel_id > 2):
			$method = "settings";
		endif;
		
		echo form_open_multipart( (isset($edit))? base_url($module.'/'.$method.'/'.$user_id) : base_url($module.'/add'), $attributes )
	?>
		<?php if(isset($edit)): ?>
        	<input type="hidden" required="" value="<?php echo $user_id; ?>" name="user_id" id="user_id" />
        <?php endif; ?>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-4">
                    <h1><?php echo $page_title; ?></h1> 
                </div>
                <div class="col-md-8">
                    <div class="pull-right">
                        <a href="<?php echo ($method != "settings") ? base_url($module) : base_url("tasks"); ?>" class="btn btn-default">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            Back to List
                        </a>
                    	<?php if(isset($edit)): ?>
							<?php if($security->CanDelete):?>
                                <a class="btn btn-default" href="<?php echo site_url($module.'/delete/' . $rec->user_id); ?>" onclick="return confirm('Do you want to delete this User?');">
                                	<span class="glyphicon glyphicon glyphicon-trash"></span> Delete
                                 </a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <table class="table table-stripped">
                <tbody>
                	<tr>
                        <td class="col-md-3" style="vertical-align: top !important"><b>Image</b></td>
                        <td class="col-md-9">
                            
                            <div id="upload_images">
                                <!-- start upload //-->	
                                <?php                                    
                                    if(isset($edit) && $rec->image_file){
                                ?>
                                
                                    <div class="fileupload fileupload-exists" data-provides="fileupload" style="margin: 0; width: 170px;">
                                        <div class="fileupload-preview fileupload-exits thumbnail" style="width: 159px; height: 110px;">
                                            <img src="<?php echo $img_path."/".$rec->image_file?>" />
                                            <input type="hidden" name="current_image" id="current_image" value="<?php echo $rec->image_file; ?>" />
                                        </div>
                                        <div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileupload-new">Select image</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" name="image_file" class="img_change" data-link="1"/>
                                            </span>
                                            <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" id="1">Remove</a>
                                        </div>
                                    </div>		

                                <?php }else{ ?>
                                
                                    <div class="fileupload fileupload-new" data-provides="fileupload" style="margin: 0; width: 170px;">
                                        <div class="fileupload-preview thumbnail" id="preview" style="width: 159px; height: 110px;"></div>
                                        <div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileupload-new">Select image</span>
                                                <span class="fileupload-exists btn-remove-img">Change</span>
                                                <input type="file" name="image_file"/>
                                            </span>
                                            <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>
                                                    
                                <?php } ?>
                                <!-- end upload //-->	
                            </div>
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-3"><b>Last Name:*</b></td>
                        <td class="col-md-9">
                            <input type="text" placeholder="Last Name" value="<?php echo $rec->last_name; ?>" name="last_name" id="last_name" class="form-control" required>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-3"><b>First Name:*</b></td>
                        <td class="col-md-9">
                            <input type="text" placeholder="First Name" value="<?php echo $rec->first_name; ?>" name="first_name" id="first_name" class="form-control" required>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-3"><b>Phone:</b></td>
                        <td class="col-md-9">
                            <input type="text" placeholder="Phone" value="<?php echo $rec->phone; ?>" name="phone" id="phone" class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-3"><b>Email:*</b></td>
                        <td class="col-md-9">
                            <input type="text" placeholder="Email" value="<?php echo $rec->email; ?>" name="email" id="email" class="form-control" required>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-3"><b>Position:</b></td>
                        <td class="col-md-9">
                            <select name="position_id" id="position_id" <?php if($user->userlevel_id > 2): ?> disabled <?php endif; ?>>
                                <option value=""></option>
                                <?php foreach($positions as $id => $obj): ?>
                                    <option value="<?php echo $id; ?>" <?php if($rec->position_id == $id): ?> selected="selected" <?php endif; ?>><?php echo $obj; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-3"><b>Userlevel:</b></td>
                        <td class="col-md-9">
                            <select name="userlevel_id" id="userlevel_id" <?php if($user->userlevel_id > 2): ?> disabled <?php endif; ?>>
                                <option value=""></option>
                                <?php foreach($userlevels as $id => $obj): ?>
                                    <option value="<?php echo $id; ?>" <?php if($rec->userlevel_id == $id): ?> selected="selected" <?php endif; ?>><?php echo $obj; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                	<tr>
                        <td class="col-md-3"><b>Username:*</b></td>
                        <td class="col-md-9">
                            <input type="text" name="username" id="username" value="<?php echo $rec->username; ?>" class="form-control" <?php if(isset($edit)): ?> disabled="disabled" <?php else: ?> required <?php endif; ?>>
                        </td>
                    </tr>
                    <?php if(isset($edit)): ?>
                    <tr>
                        <td class="col-md-3"><b>Current Password</b></td>
                        <td class="col-md-9">
                            <input type="password" name="current_password" id="current_password" value="" class="form-control">
                            <input type="hidden" name="password" id="password" value="<?php #echo $rec->password; ?>" class="form-control">
                        </td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td class="col-md-3"><b>New Password</b></td>
                        <td class="col-md-9">
                            <input type="password" name="new_password" id="new_password"  value="" class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-3"><b>Confirm Password</b></td>
                        <td class="col-md-9">
                            <input type="password" name="confirm_password" id="confirm_password" value="" class="form-control">
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                            <div class="pull-right">
                                <button class="btn btn-warning" type="submit" id="submit-btn">
                                    <span class="glyphicon glyphicon-ok"></span> Save <?php echo $module; ?>
                                </button>
                            </div>
                        </td>
                    </tr>
                    
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
        	<div class="row">
                <div class="col-md-6">
                    <h1>Assign Projects</h1> 
                </div>
                <div class="col-md-6">
                	<div class="pull-right">
		                <label for="select_all">
                        	<input type="checkbox" id="select_all"/> Select All
                        </label>
                    </div>
                </div>
            </div>

            <table class="table">
            	<?php
					$count 		= count($projects);
					$index 		= 0;
					$counter 	= 1;
					foreach($projects as $id => $obj):
						$index++;
						
						if($index == 1)
						{
							echo "<tr>";
						}
				?>
	                <td class="col-md-6">
                    	<label for="p_<?php echo $counter; ?>">
	                    	<input type="checkbox" class="chk" name="projects[]" value="<?php echo $id; ?>" id="p_<?php echo $counter; ?>" <?php if((is_array($projects_list)) || ($is_admin))if((in_array($id,$projects_list)) || ($is_admin)): ?> checked="checked" <?php endif; ?> />
                            <?php echo $obj; ?>
                        </label>
                    </td>
                <?php
						if(($count == $counter) && $index == 1)
						{
							echo '<td class="col-md-6">&nbsp;</td>';
						}
						if($index == 2)
						{
							echo "</tr>";
							$index = 0;
						}
						$counter++;
					endforeach;
				?>
            </table>
        </div>
    <?php echo form_close(); ?>
    </div>
</div>

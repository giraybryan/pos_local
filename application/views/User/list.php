<?php
function set_url($title, $field, $sort_order, $segment5, $segment6, $sort_by)
{
	if($sort_order == "DESC")
	{
		$sort 		= "<span class='glyphicon glyphicon-chevron-up'>";
		$sort_new 	= "ASC";
	}
	else
	{
		$sort 		= "<span class='glyphicon glyphicon-chevron-down'>";
		$sort_new 	= "DESC";
	}
	
	$uri = "";
	if($segment5)
	{
		$uri .= $segment5."/";
	}
	if($segment6)
	{
		$uri .= $segment6;
	}
	
	$link = base_url("user/index/".$field."/".$sort_new."/".$uri."/");
	echo "<a href=".$link.">".$title." ".(($field == $sort_by) ? $sort : "")."</a>";
}
?>

<div class="list">
	<div class="row">
        <div class="col-md-6">
            <h1><?php echo $page_title; ?></h1>
        </div>
        <div class="col-md-6">
            <div class="row" style="margin:0">
                <div class="col-md-9" style="padding: 0px 5px;">
                    <?php echo form_open(base_url($module.'/index/')) ?>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search Keyword" id="term" name="term">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <span class="glyphicon glyphicon-search"></span> 
                                    Search
                                </button>
                                <a href="<?php echo base_url($module); ?>" class="btn btn-default">
                                    <span class="glyphicon glyphicon-refresh"></span> 
                                    Reset
                                </a>
                            </span>
                        </div>
                    <?php echo form_close(); ?>
                </div>
                <div class="col-md-3" style="padding: 0px;">
                    <?php if($security->CanAdd):?>
                        <a href="<?php echo base_url($module.'/add'); ?>" class="btn btn-default btn-add">
                            <span class="glyphicon glyphicon-plus"></span>
                            Add New User
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
	</div>
    <table class="table">
		<thead>
			<tr> 
				<th class="col-md-2">
                	<?php echo set_url("Last Name", "last_name", $sort_order, $this->uri->segment(5), $this->uri->segment(6), $sort_by); ?>
                </th>
				<th class="col-md-2">
                	<?php echo set_url("First Name", "first_name", $sort_order, $this->uri->segment(5), $this->uri->segment(6), $sort_by); ?>
                </th>
                <th class="col-md-2">
                	<?php echo set_url("Email", "email", $sort_order, $this->uri->segment(5), $this->uri->segment(6), $sort_by); ?>
                </th>
                <th class="col-md-2">
                	<?php echo set_url("Position", "position_name", $sort_order, $this->uri->segment(5), $this->uri->segment(6), $sort_by); ?>
                </th>
                <th class="col-md-2">
                	<?php echo set_url("User Level", "userlevel_name", $sort_order, $this->uri->segment(5), $this->uri->segment(6), $sort_by); ?>
                </th>
                <th class="col-md-2"></th>
			</tr>
		</thead>
		<tbody>
		<?php if(!empty($users)): ?>
			<?php foreach($users as $obj): ?>
			<tr>
				<td><?php echo $obj->last_name ?></td>
                <td><?php echo $obj->first_name ?></td>
                <td><?php echo $obj->email ?></td>
                <td><?php echo $obj->position_name; #$positions[$obj->position_id] ?></td>
                <td><?php echo $obj->userlevel_name; #$userlevels[$obj->userlevel_id] ?></td>
				<td>
                	<div class="pull-right">
                    	<?php /*if($CanView):?>
                            <a class="btn btn-default edit_row" href="<?php echo base_url($module.'/view/'.$obj->user_id); ?>">View</a>
                        <?php endif;*/ ?>
						<?php if($security->CanEdit):?>
                            <a class="btn btn-default edit_row" href="<?php echo base_url($module.'/edit/'.$obj->user_id); ?>">
                            	<span class="glyphicon glyphicon-pencil"></span>&nbsp; Edit
                            </a>
                        <?php endif; ?>
                        <?php if($security->CanDelete):?>
                            <a class="btn btn-default delete_row" href="<?php echo site_url($module.'/delete/' . $obj->user_id); ?>">
                            	<span class="glyphicon glyphicon-trash"></span>&nbsp; Delete
                            </a>
                        <?php endif; ?>
                    </div>
				</td>
			</tr>
			<?php endforeach; ?>			
		<?php else: ?>
			<tr>
				<td colspan="3" class="text-center">No existing record</td>
			</tr>
		<?php endif; ?>
		</tbody>
	</table>
    
    <div>
    	<?php echo $pagination_link; ?>
    </div>
</div>

<script type="text/javascript">
$(function() {
	$('.delete_row').on('click', function() {
		var o = $(this);
		var link = o.attr('href');
		if(confirm('Do you want to delete this User?')) {
			$.ajax({
				url: link,
				type: 'GET',
				dataType: 'json',
				success: function(data) {
					if(data.result) {
						o.parent().parent().parent().remove();
					}
				},
				error: function(data) {
					console.log('Error');
					console.log(data)
				}
			});
		}
		return false;
	});
});
</script>
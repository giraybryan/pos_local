<?php foreach($items as $item):?>
<li data-id='<?php echo $item->id ?>' data-qty="1">
	<i class="js-remove "><span class="glyphicon glyphicon-remove pull-right"></span></i>
		<span class="glyphicon glyphicon-minus-sign exclude-btn" data-toggle="tooltip" data-placement="bottom" title="add to exclude list"></span>
	<div class='large-image-container'>
		
		<?php 
			if(!empty($item->image)){
				$image_url = base_url('uploads/item_image/'.$item->image);
			}  else {
				$image_url = base_url('assets/img/default.png');
			}
		?>
		<img src='<?php echo $image_url; ?>' class=''/>
	</div>
	<div class='tile-title'><?php echo $item->item?></div>
	<div class='item_details'>
		
	</div>
	
</li>
<?php endforeach; ?>
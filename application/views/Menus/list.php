<thead class="center-text">
		<tr>
			<th>Image </th>
			<th><a onclick="event.preventDefault(); $('#list_container').fadeOut();  load_pageloader('list_container'); $('#list_container').load('<?php echo base_url('menus/paginate/'.$category_id.'/'.$keywords.'/'.$status_id.'/title/'.$sort_order.'/'.$limit.'/'.$offset) ?>').fadeIn();">Menu Name</a> </th>
			<th><a onclick="event.preventDefault(); $('#list_container').fadeOut();  load_pageloader('list_container'); $('#list_container').load('<?php echo base_url('menus/paginate/'.$category_id.'/'.$keywords.'/'.$status_id.'/description/'.$sort_order.'/'.$limit.'/'.$offset) ?>').fadeIn();">Description </a></th>
			<th><a onclick="event.preventDefault(); $('#list_container').fadeOut();  load_pageloader('list_container'); $('#list_container').load('<?php echo base_url('menus/paginate/'.$category_id.'/'.$keywords.'/'.$status_id.'/price/'.$sort_order.'/'.$limit.'/'.$offset) ?>').fadeIn();">Price </a></th>
			<th><a onclick="event.preventDefault(); $('#list_container').fadeOut();  load_pageloader('list_container'); $('#list_container').load('<?php echo base_url('menus/paginate/'.$category_id.'/'.$keywords.'/'.$status_id.'/status_name/'.$sort_order.'/'.$limit.'/'.$offset) ?>').fadeIn();">Status </a></th>
			<th></th>
		</tr>
	</thead>
	<tbody class="center-text" >
<?php if(!empty($menus)):?>
	<?php foreach($menus as $menu):?>
	<tr>
		<td>
			<?php  ?>
			<div class="image-container">
				<?php
					if(!empty($menu->image)){
						$image_url = base_url('uploads/menu/'.$menu->image);
					}  else {
						$image_url = base_url('assets/img/default.png');
					}
				 ?>
				<img class="" src="<?php echo $image_url ?>"/>
			</div>
		</td>
		<td><?php echo $menu->title?></td>
		<td> <?php echo $menu->description?></td>
		<td> P <?php echo $menu->price?></td>
		<td><?php echo $menu->status_name?></td>
		<td>
			<div class="input-group inputgroup-gray">
				
				<a data-toggle="tooltip" data-placement="bottom" title="Modify Menu Ingredients"  href="<?php echo base_url('menus/menu_ingredients/'.$menu->id);?>"><button class="btn modify-item" data-id="<?php echo $menu->id ?>" data-encode='<?php echo json_encode($menu); ?>'><span class="glyphicon glyphicon-list-alt"></span></button></a>
				<!--<button class="btn edit-item" data-id="<?php echo $menu->id?>" data-encode='<?php echo json_encode($menu); ?>' data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button> -->
				<!-- <button class="btn delete-item" data-id="<?php echo $menu->id?>" data-toggle="tooltip" data-placement="bottom" title="Delete" ><span  class="glyphicon glyphicon-trash"></span></button> -->

				
				
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="6"><?php echo $pagination_link; ?></td>
	</tr>
<?php else: ?>

	<tr>
		<td colspan="5">
			<div class="alert alert-warning alert-dismissible fade in">
				<h5> There are no menu right now! </h5>
			</div>
		</td>
	</tr>
<?php endif;?>
	</tbody>
		
<script type="text/javascript" src="<?php echo base_url('assets/js/append.js'); ?>"></script>	
<script type="text/javascript">
	$(document).ready(function(){
		
		
		$('[data-toggle="tooltip"]').tooltip();
		$('.delete-item').click(function(){
			id = $(this).attr('data-id');
			
			console.log(id);
			
			swal({   
				title: "Are you sure?",   
				text: "You will deleting this menu.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Delete it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'menus/delete',
							data: {id : id},
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
							
								if(response.result == 1){
									swal("Deleted!", response.msg, "success");  
									$('.back-list').trigger('click');
									get_list();
								} else {
									swal("Cancelled", response.msg, "error");   
								}
									
									
								
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		});
		
	});
</script>
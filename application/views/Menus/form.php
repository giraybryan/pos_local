<div id="menu-form-wrapper" class="col-sm-8 right-panel hide">
	
	<div class="container-fluid widget-content">
	<button class="btn btn-default back-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button>
	<form id="menu-form"  class="form-horizontal">
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Menu Name:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="title" name="title" placeholder="Menu Title">
				<input type="hidden" class="form-control" id="id" name="id" placeholder="">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Description:</label>
			<div class="col-sm-9">
				<textarea name="description" id="description" class="form-control"></textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Price:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="price" name="price" placeholder="Price">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Category:</label>
			<div class="col-sm-9">
				<select name="menu_category_id" id="menu_category_id" placeholder="Select Category" class="select">
					<option value=""></option>
					<?php foreach ($categories as $category): ?>
						<option value="<?php echo $category->id ?>"><?php echo $category->menu_category_name?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Status:</label>
			<div class="col-sm-9">
				<select name="status_id" id="status_id" placeholder="Select Status" class="select">
					<option value=""></option>
					<?php foreach ($status as $stat): ?>
						<option value="<?php echo $stat->id ?>"><?php echo $stat->status_name?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Image</label>
			<div class="col-sm-9">
				<input type="file" name="menu" id="menu" />

				<div class="current_file">
					Current file: <img src="" id="current_image" style='width: 50px'/>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-primary">Save</button>
				<a href="" class="btn btn-warning hide" id="modify_btn">Modify Ingredients</a>
			</div>
		</div>
		
	</form>
	</div>
</div>
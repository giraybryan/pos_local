<?php 
	if(!empty($ingredients)):
	foreach($ingredients as $item):?>
<li data-id='<?php echo $item->id ?>' data-qty="<?php echo (empty($item->qty) ? 1 : $item->qty ) ?>" data-exclude="<?php echo (in_array($item->id, $exclude_ids) ? 1 : 0 )?>" class="<?php echo ($item->stock < 1 ? 'danger': '' )?>">
	<i class="js-remove "><span class="glyphicon glyphicon-remove pull-right"></span></i>
		<span class="glyphicon glyphicon-minus-sign exclude-btn <?php echo (in_array($item->id, $exclude_ids) ? 'active' : '' )?>" data-toggle="tooltip" data-placement="bottom" title="add to exclude list"></span>
	<div class='large-image-container'>
		
		<?php 
			if(!empty($item->image)){
				$image_url = base_url('uploads/item_image/'.$item->image);
			}  else {
				$image_url = base_url('assets/img/default.png');
			}
		?>
		<img src='<?php echo $image_url; ?>' class='img-responsive'/>
	</div>
	<div class='tile-title'><?php echo $item->item?></div>
	<div class='item_details'>

		<input type="hidden" value="<?php echo $item->id ?>" name="ingredient[<?php echo $item->id ?>][ingredient_id]" class="item_qty form-control"/>
		<input type="text" value="<?php echo (empty($item->qty) ? 1 : $item->qty ) ?>" name="ingredient[<?php echo $item->id ?>][qty]" class="item_qty form-control"/>
	</div>
	

</li>
	
<?php endforeach; 
	endif;
?>
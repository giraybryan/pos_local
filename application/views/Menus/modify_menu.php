<form id="modify-form">
<div class='row'>
	<div class='col-sm-4'>
		<div class="container-fluid container-wrapper list-container">
			<?php 
			if(!empty($menu->image)){
				$img_url = base_url('uploads/menu/'.$menu->image);
			}  else {
				$img_url = base_url('assets/img/default.png');
			}
			?>
			<img src="<?php echo $img_url ?>" class="img-responsive"/>

			<input type="hidden" name="ingredients" id="ingredients"/>
			<input type="hidden" name="addons" id="addons"/>
			<input type="hidden" name="excludes" id="excludes"/>
			<input type="hidden" name="menu_id" id="menu_id" value='<?php echo $menu->id ?>'/>
		</div>
		
	</div>
	<div class='col-sm-8'>
		<div class="container-fluid container-wrapper list-container">
			<div class="row">
				<div class="col-sm-6"><a href="<?php echo base_url('menus');?>" class="btn btn-default">BACK TO LIST</a></div>
				<!--<div class="col-sm-6"><button type='submit' class="btn btn-warning pull-right">UPDATE</button></div> -->

			</div>
			
			<div class='v-padding-wrapper'>
				<div class="row">
					
					<div class="col-sm-6">
						<h3 style='margin-top: 0px;'><?php echo $menu->title?></h3>
						<p><?php echo $menu->description?></p>
						<span><label><?php echo format_currency($menu->price) ?></label></span>
						<div class="status_label"><label>Status:</label>  <span class="status_value"><?php echo $menu->status_name ?></span></div>
						
						<div class=""><label>Category: </label> <?php echo $menu->menu_category_name?></div>
						<div class='category-options'>
							
						</div>
					</div>
					<div class="col-sm-6">
							<label>Category Options:</label>
							<ul>
								<?php if(!empty($category_options)): ?>
								<?php foreach($category_options as $option): ?>
								<li><?php echo $option->option_name?> (Php <?php echo $option->price?>)</li>	
								<?php endforeach;?>
								<?php else:?>
								<li>No Option(s)</li>
								<?php endif;?>
							</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	



<div class="row" id='advanced'>
	<!--<div class="col-sm-4">
		<div class="widget-container">
			<div class="widget-title">
				<h4>Filter Items</h4>
			</div>
			<div class="widget-body">
					<label>Category</label>
					<select class="select" id="filter_category" placeholder="Select Category">
						<option value=""></option>
						<?php foreach($categories as $category): ?>
						<option value="<?php echo $category->id?>"><?php echo $category->name ?></option>
						<?php endforeach;?>
					</select>
			</div>
		</div>
		
		<div class="panel panel-default">
			
			<div class="panel-heading widget-title modify-menu-title">Inventory Item</div>
			
			<div class="" id="advance-1">

			</div>
			<ul id="advanced-1" class="list">
				<?php $this->load->view('Menus/inventory_item');?>
			</ul>
			
		</div>
	</div>-->
	<div class="col-sm-8">
		<div class="panel panel-default">
			
			<div class="panel-heading widget-title modify-menu-title">Menu Ingredients</div>
			<!-- List group -->
			
			<ul id="advanced-2" class='list'>
				<?php $this->load->view('Menus/ingredient_items'); ?>
			</ul>
			
		</div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-default">
			
			<div class="panel-heading widget-title modify-menu-title">Addons:</div>
			<!-- List group -->
			<ul id="advanced-3" class="list">
				<?php $this->load->view('Menus/addons_items'); ?>
			</ul>
			
			
		</div>
	</div>
</div>

</form>
<style>
	.list li {
	    border: 1px solid #f1f1f1;
	    display: inline-block;
	    float: none;
	    margin: 5px;
	    max-width: 106px;
	}
</style>

<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/sortable/Sortable.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/modules/menu_ingredients.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/append.js');?>"></script>
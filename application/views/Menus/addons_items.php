<?php 
	if(!empty($addons)):
	foreach($addons as $item):?>
<li data-id='<?php echo $item->id ?>'>
	<div class='img-container'>
		<i class="js-remove "><span class="glyphicon glyphicon-remove pull-right"></span></i>
		<?php 
			if(!empty($item->image)){
				$image_url = base_url('uploads/item_image/'.$item->image);
			}  else {
				$image_url = base_url('assets/img/default.png');
			}
		?>
		<img src='<?php echo $image_url; ?>' class='img-responsive'/>
	</div>
	<div class='tile-title'><?php echo $item->item?></div>
</li>
<?php endforeach; 
	endif;
?>
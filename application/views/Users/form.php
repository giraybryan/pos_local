<div id="user-form-wrapper" class="col-sm-8 right-panel hide">
	
	<div class="container-fluid widget-content">
	<button class="btn btn-default back-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button>
	<form id="user-form"  class="form-horizontal">
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Image:</label>
			<div class="col-sm-9">
				<input type="file" id="users" name="users" placeholder="">
				<div class="current_file">
					Current file: <img src="" id="current_image" style='width: 50px'/>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Last Name:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
				<input type="hidden" class="form-control" id="id" name="id" placeholder="">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">First Name:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control datepick" id="first_name" name="first_name" placeholder="First Name">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Email:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control datepick" id="email" name="email" placeholder="Email">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Phone</label>
			<div class="col-sm-9">
				<input type="text" name="phone" id="phone" class="form-control" placeholder="Phone" />
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Password</label>
			<div class="col-sm-9">
				<input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
			</div>
		</div>

		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Position</label>
			<div class="col-sm-9">
				<select name="position_id" id="position_id" placeholder="Select Position" class="select">
					<option value=""></option>
					<?php foreach($positions as $position):?>
					<option value='<?php echo $position->id?>'><?php echo $position->position_name?></option>
					<?php endforeach;?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Userlevel</label>
			<div class="col-sm-9">
				<select name="userlevel_id" id="userlevel_id" placeholder="Select Userlevel" class="select">
					<option value=""></option>
					<?php foreach($userlevels as $userlevel):?>
					<option value='<?php echo $userlevel->userlevel_id?>'><?php echo $userlevel->userlevel_name?></option>
					<?php endforeach;?>
				</select>	
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Branch:</label>
			<div class="col-sm-9">
				<select name="branch_id" id="branch_id" placeholder="Select Branch" class="select">
					<option value=""></option>
					<?php foreach($branches as $branch):?>
					<option value='<?php echo $branch->id?>'><?php echo $branch->branch_name?></option>
					<?php endforeach;?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
		</div>
		
	</form>
	</div>
</div>
<div class="row">
	<?php $this->load->view($module.'/quick_search');?>
	<div id="user-list-wrapper" class="col-sm-9 right-panel">
		
		<div class="container-fluid widget-content list-container">
			<div class="">
			</div>
			<div class="page-title">
				<h4><?php echo $page?></h4>
			</div>
			<button id="add-item" class="btn btn-default add-item">Add User</button>
			
			<table class="table table-stripes">
				<thead class="center-text">
					<tr>
						<th>Image </th>
						<th>Last Name </th>
						<th>First Nmae </th>
						<th>Position </th>
						<th>Userlevel </th>
						
						
						<th></th>
					</tr>
				</thead>
				<tbody class="center-text" id="list_container">
				<?php $this->load->view($module.'/list');?>
				</tbody>
			</table>
		</div>

		<div class="container-fluid widget-content view-container hide">
			
			<table class="table table-bordered">
				<tr>
					<td><button class="btn btn-default back-to-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button></td>
					<td><input type="button"  class="btn btn-default edit-item" value='EDIT'  data-encode="" data-id="" id="id_view"/></td>
				</tr>
				<tr>
					<td style=''>Image</td>
					<td><img src="" id="image_view" style='max-width: 300px'/></td>
				</tr>
				<tr>
					<td style=''>Last Name</td>
					<td id="last_name_view"></td>
				</tr>
				<tr>
					<td style=''>First Name</td>
					<td id="first_name_view"></td>
				</tr>
				<tr>
					<td style=''>Email</td>
					<td id="email_view"></td>
				</tr>
				<tr>
					<td>Username View</td>
					<td id="username_view"></td>
				</tr>
				<tr>
					<td>Position</td>
					<td id="position_view"></td>
				</tr>
				<tr>
					<td>Branch</td>
					<td id="branch_name_view">

					</td>
				</tr>
				
			
				
				
				
			</table>
 
		</div>
	</div>


	<?php $this->load->view($module.'/form');?>
	
</div>



<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/modules/users.js');?>"></script>
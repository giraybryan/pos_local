 <?php if(!empty($users)):?>
	<?php foreach($users as $user):?>
	<tr>
		<td>
			
			<div class="image-container">
				<?php
					if(!empty($user->image)){ 
						$image_url = base_url('uploads/users/'.$user->image);
					}  else {
						$image_url = base_url('assets/img/default.png');
					}
				 ?>
				<img class="" src="<?php echo $image_url ?>"/>
			</div>
		</td>
		<td><?php echo $user->last_name?></td>
		<td> <?php echo $user->first_name?></td>
		<td> <?php echo $user->position_name?></td>
		<td><?php echo $user->userlevel_name?></td>
		<td>
			<div class="input-group inputgroup-gray">
				<button class="btn view-item" data-id="<?php echo $user->id ?>" data-encode='<?php echo json_encode($user); ?>' data-toggle="tooltip" data-placement="bottom" title="View Details" ><span class="glyphicon glyphicon-eye-open"></span></button>
				<button class="btn edit-item" data-id="<?php echo $user->id?>" data-encode='<?php echo json_encode($user); ?>' data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button>
				<button class="btn delete-item" data-id="<?php echo $user->id?>" data-toggle="tooltip" data-placement="bottom" title="Delete" ><span  class="glyphicon glyphicon-trash"></span></button>

				
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="6"><?php echo $pagination_link; ?></td>
	</tr>
<?php else: ?>

	<tr>
		<td colspan="5">
			<div class="alert alert-warning alert-dismissible fade in">
				<h5> There are no users right now! </h5>
			</div>
		</td>
	</tr>
<?php endif;?>
		
		
<script type="text/javascript" src="<?php echo base_url('assets/js/append.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		
		$('[data-toggle="tooltip"]').tooltip();
		
		$('.delete-item').click(function(){
			id = $(this).attr('data-id');
			
			console.log(id);
			
			swal({   
				title: "Are you sure?",   
				text: "You will deleting this user.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Delete it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'users/delete',
							data: {id : id},
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
							
								if(response.result == 1){
									swal("Deleted!", response.msg, "success");  
									$('.back-list').trigger('click');
									get_list();
								} else {
									swal("Cancelled", response.msg, "error");   
								}
									
									
								
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		});
		
	});
</script>
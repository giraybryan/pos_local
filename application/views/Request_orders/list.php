<?php if(!empty($pos)):?>
	<?php foreach($pos as $po):?>
	<tr>
		
		<td><?php echo $po->first_name.' '.$po->last_name ?></td>
		<td><?php echo $po->RequestCode ?></td>
		<td> <?php echo $po->branch_name?></td>
		<td><?php echo date('Y-m-d' , strtotime($po->RequestData))?></td>
		<td><?php echo ($po->Delivered == 1 ? 'Delivered': 'Pending')?></td>
		<td>
			<div class="input-group inputgroup-gray">
				<button class="btn view-item" data-id="<?php echo $po->id ?>" data-encode='<?php echo json_encode($po); ?>'>Details</button>
				<a href="<?php echo base_url('request_orders/update_request/'.$po->id);?>"><button class="btn update-item" data-id="<?php echo $po->id ?>" data-encode='<?php echo json_encode($po); ?>'><?php echo ($po->Delivered == 1 ? 'View': 'Update')?></button></a>
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="5"><?php echo $pagination_link; ?></td>
	</tr>
<?php else: ?>

	<tr>
		<td colspan="5">
			<div class="alert alert-warning alert-dismissible fade in">
				<h5> There are no pending R.O request right now! </h5>
			</div>
		</td>
	</tr>
<?php endif;?>
		
		
<script type="text/javascript">
	$(document).ready(function(){
		
		
		
		
		
	});
</script>
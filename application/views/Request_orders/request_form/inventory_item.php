<?php foreach($items as $item):?>
<li data-id='<?php echo $item->id ?>' data-qty="1">
	<i class="js-remove "><span class="glyphicon glyphicon-remove pull-right"></span></i>
	
	<div class='list_view'>	
		<div class='image-container'>
			
			<?php 
				if(!empty($item->image)){
					$image_url = base_url('uploads/item_image/'.$item->image);
				}  else {
					$image_url = base_url('assets/img/default.png');
				}
			?>
			<img src='<?php echo $image_url; ?>' class=''/>
		</div>
		<div class='tile-title'><?php echo $item->item?></div>
		<div class='item_details'>
			
		</div>
	</div>
	<div class="form_list">
		<div class="row">
				<div class="col-sm-4">
					<div class="row">
						<div class="col-sm-12">
							<div class="image-container">
								<img class="" src="<?php echo $image_url ?>">
							</div>
						</div>
						<div style="text-align: center" class="col-sm-12">
							<?php echo $item->item?>						
						</div>
					</div>
					
				</div>
				<div class="col-sm-8 request_info">
					<div class="row">
						<div class="col-sm-12">

							<span class="error_msg"></span>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">Current Stock:</div>
						<div class="col-sm-8"><?php echo $item->stock?><br></div>
					</div>
					<div class="row">
						<div class="col-sm-4">Request Qty: </div>
						<div class="col-sm-8">
							<input type="text" name="item[<?php echo $item->id ?>][RequestQty]" value="0" class="form-control deliver_qty ">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">Delivered Qty:</div>
						<div class="col-sm-8">
							
							<input type="hidden" name="item[<?php echo $item->id ?>][IngredientID]" value="<?php echo $item->id ?>" class="form-control item_id">
							<input type="text" readonly name="item[<?php echo $item->id ?>][DeliveredQty]" value="0" class="form-control deliver_qty ">
							<input type="hidden" name="item[<?php echo $item->id ?>][current_stock]" value="<?php echo $item->stock ?>" class="form-control current_stock">
							<input type="hidden" name="item[<?php echo $item->id ?>][IngredientCode]" value="<?php echo $item->item_code ?>" class="form-control current_stock">
							<input type="hidden" name="item[<?php echo $item->id ?>][PartialQty]" value="0" class="form-control">
													</div>
					</div>
				</div>
			</div>

	</div>
	
</li>
<?php endforeach; ?>
<div class="col-sm-3">
	<div class="widget-container">
		<div class="widget-title">
			<h4>Inventory</h4>
		</div>

		<div class="widget-body">
			<label>Sort by Category</label>
			
			<select class="select" id="filter_category" placeholder="Select Category">
					<option value=""></option>
					<?php foreach($categories as $category): ?>
					<option value="<?php echo $category->id?>"><?php echo $category->name ?></option>
					<?php endforeach;?>
			</select>
			<input type="checkbox" id="filter_danger" name="filter_danger" style="margin-right: 19px; "/><label>Danger Threshold</label>
		</div>
			
	</div>


	<div class="panel panel-default">
			
			<div class="panel-heading widget-title modify-menu-title">Inventory Item</div>
			<!-- List group -->
			<div class="" id="advance-1">

			</div>
			<ul id="advanced-1" class="list">
				<?php $this->load->view('Request_orders/request_form/inventory_item');?>
			</ul>

	</div>	
	
	
	
</div>



<form method="post" name="ro_form" id="ro_form">
	<div id="request-list-wrapper" class="col-sm-7 right-panel">
		<div class="widget-title">
				<h4>Request Item(s)</h4>
			</div>
		<div class="container-fluid widget-content view-container">
			<input type="submit" class='btn btn-primary pull-rig' value="Submit"/>
			<input type="hidden" value="<?php echo (!empty($request) ? $request->id : '' )?>" name="id" id="id"/>
		</div>
		<ul class="container-fluid widget-content view-container request_items"  style='min-height:100px; padding-bottom: 10px;'id="advanced-2">
			<?php $this->load->view('Request_orders/request_form/requested_list');?>
		</ul>
	</div>

	
	<ul id="advanced-3" class="list hide">
				
			</ul>
</form>

<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/sortable/Sortable.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/modules/request_local.js'); ?>"></script>
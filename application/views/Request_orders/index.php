
<div class="row">
	<?php $this->load->view($module.'/quick_search');?>
	<div id="sales-list-wrapper" class="col-sm-9 right-panel">
	
		<div class="container-fluid widget-content list-container">
			<div class="">
			</div>
			<div class="page-title">
				<h4><?php echo $page?></h4>
			</div>
			<a href="<?php echo base_url('request_orders/request_inventory'); ?>"><button id="add-item" class="btn btn-default add-item">Send Request</button> </a>
			<table class="table table-stripes">
				<thead class="center-text">
					<tr>
						<th>Request By</th>
						<th>Request Code</th>
						<th>Branch</th>
						<th>Date Requested </th>
						<th>Status </th>
						<th></th>
					</tr>
				</thead>
				<tbody class="center-text" id="list_container">
				<?php $this->load->view($module.'/list');?>
				</tbody>
			</table>
		</div>

		<div class="container-fluid widget-content view-container hide">
			
			<table class="table table-bordered">
				<tr>
					<td colspan="2"><button class="btn btn-default back-to-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button></td>
					
				</tr>
				<tr>
					<td style=''>Reqest By</td>
					<td><span id="first_name_view"></span> <span id="last_name_view"></span></td>
				</tr>
				<tr>
					<td>Request Code </td>
					<td id="RequestCode_view"></td>
				</tr>
				<tr>
					<td>Request Date </td>
					<td id="RequestData_view"></td>
				</tr>
				<tr>
					<td>Date Received </td>
					<td id="date_created_view"></td>
				</tr>
				<tr>
					<td>Delivered Status </td>
					<td id="Delivered_view"></td>
				</tr>
				<tr>
					<td>Branch </td>
					<td id="branch_name_view"></td>
				</tr>
				
				
				
				
			</table>
			
			<div id="transaction_wrapper">

			</div>
		</div>
	</div>


	
</div>



<script type="text/javascript" src="<?php echo base_url('assets/js/modules/request_orders.js');?>"></script>


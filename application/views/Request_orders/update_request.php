<form method="post" name="ro_form" id="ro_form">

	<div class="col-sm-5">
		<div class="widget-container">
			<div class="widget-title">
				<h4>Request Info</h4>
			</div>

			<div class="widget-body">
				<div class="row">
					<div class="col-sm-5">
						<a href="<?php echo base_url('request_orders'); ?>" class="btn btn-default">Back</a>
					</div>
					
					<div class="col-sm-7">	
						
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5">
						<label>Request by: </label>
					</div>
					
					<div class="col-sm-7">	
						<span> <?php echo $request_info->first_name.' '.$request_info->last_name ?></span> 
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5">
						<label>Date Requested: </label>
					</div>
					<div class="col-sm-7">	
						 <span> <?php echo date('Y-m-d', strtotime($request_info->date_created))?></span> 
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5">
						<label>Device ID: </label>
					</div>
					<div class="col-sm-7">	
						 <span> <?php echo $request_info->DeviceID ?></span> 
						 <input type="hidden" value="<?php echo $request_info->DeviceID ?>" name="DeviceID"/>
						 <input type="hidden" value="<?php echo $request_info->id ?>" name="id"/>
						 <input type="hidden" value="<?php echo $request_info->Delivered ?>" name="Delivered"/>
						 <input type="hidden" value="<?php echo $request_info->BranchID ?>" name="BranchID"/>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5">
						<label>Request Code: </label> 
					</div>
					<div class="col-sm-7">	
						<span> <?php echo $request_info->RequestCode ?></span>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5">
						<label>Status: </label> 
					</div>
					<div class="col-sm-7">	
						<?php 
							if($request_info->Delivered == 1 ):
								?> Delivered <div class="btn btn-primary" id="redeliver" date-id="<?php echo $request_info->id ?>">Re-Deliver</div><?php
							else: 
							echo "Pending";
							endif;
						?>

						
					</div>
				</div>
			</div>	
		</div>
	</div>


	<div id="sales-list-wrapper" class="col-sm-7 right-panel">
		<div class="widget-title">
				<h4>Request Item(s)</h4>
			</div>
		<div class="container-fluid widget-content view-container">
			<?php if( !empty($request_items) ): ?>
			<?php $outstock_status = 0; ?>
			<?php foreach($request_items as $item):?>
			<div class="row">
				<div class="col-sm-4">
					<div class="row">
						<div class="col-sm-12">
							<div class="image-container">
								<?php $image_url = (!empty($item->image) ? base_url('uploads/item_image/'.$item->image) : base_url('assets/assets/img/default.png'))?>
								<img src="<?php echo $image_url; ?>" class="">
							</div>
						</div>
						<div class="col-sm-12" style='text-align: center'>
							<?php echo $item->item?>
						</div>
					</div>
					
				</div>
				<div class="col-sm-8 request_info">
					<div class="row">
						<div class="col-sm-12">

							<span class="error_msg"><?php if($item->current_stock < $item->RequestQty && $request_info->Delivered == 0) {  echo 'Not enough remaining stocks'; $outstock_status++; } ?></span>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">Current Stock:</div>
						<div class="col-sm-8"><?php echo $item->current_stock; ?><br/></div>
					</div>
					<div class="row">
						<div class="col-sm-4">Request Qty: </div>
						<div class="col-sm-8"><?php echo $item->RequestQty; ?> <br/></div>
					</div>
					<div class="row">
						<div class="col-sm-4">Delivered Qty:</div>
						<div class="col-sm-8">
							<?php if($request_info->Delivered == 0): ?>

							<input type="hidden" class="form-control item_id" value="<?php echo $item->IngredientID; ?>" name="item[<?php echo $item->id ?>][IngredientID]"/>
							<input type="text" class="form-control deliver_qty <?php echo ($item->current_stock < $item->RequestQty ? 'invalid' : '')?>" value="<?php echo $item->DeliveredQty; ?>" name="item[<?php echo $item->id ?>][DeliveredQty]"/>
							<input type="hidden" class="form-control request_qty" value="<?php echo $item->RequestQty; ?>" name="item[<?php echo $item->id ?>][RequestQty]"/>
							<input type="hidden" class="form-control current_stock" value="<?php echo $item->current_stock; ?>" name="item[<?php echo $item->id?>][current_stock]"/>
							<input type="hidden" class="form-control" value="<?php echo (!empty($item->PartialQty)  ? $item->PartialQty : 0) ; ?>" name="item[<?php echo $item->id ?>][PartialQty]"/>
							<?php else: ?>
								<?php echo $item->DeliveredQty; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
			<?php endif; ?>

			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-8">
					<?php if($request_info->Delivered == 0): ?>
					<input type="submit" value="Update" class="btn btn-primary" id="submit" <?php echo (!empty($outstock_status) ? 'disabled' : '')?>/>
					<?php endif; ?>
				</div>
			</div>		
			

		</div>
	</div>
</form>


<div class="hide" id="redeliver_modal">
	<form class="" id="redeliver_form">
		 <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Admin Password</h4>
	      </div>
	      <div class="modal-body" id="modal-body">
	       	<input type="password" class="form-control" id="password" name="password" />
	       	<input type="hidden" class="form-control" id="id" value="<?php echo $request_info->id?>"  name="id" />
	      </div>
	      <div class="modal-footer">
	        <input type="submit" class="btn btn-primary" value="Re-Deliver"/>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
     </form>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/modules/update_reqest.js') ?>"></script>
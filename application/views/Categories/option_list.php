<table class='table table-bordered'>
	<thead>
		<tr>
			<th>Option name</th>
			<th>Price</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php if(!empty($options)): ?>
		<?php foreach ($options as $option): ?>
			<tr>
				<td><?php echo $option->option_name ?></td>
				<td><?php echo $option->price ?></td>
				<td>
					<div class="input-group inputgroup-gray">
						<button data-id='<?php echo $option->id?>' class='btn edit-option' data-encode='<?php echo json_encode($option)?>'>Edit</button>
						<button data-id="<?php echo $option->id?>" data-category="<?php echo $option->menu_category_id ?>" class="btn delete-option">Remove</button>
					</div>
				
				</td>
			</tr>
		<?php endforeach; ?>


	<?php else: ?>
	
		<tr>
			<td colspan='3'>
				<div class="alert alert-warning alert-dismissible fade in">
					<h5> There are no option(s) right now! </h5>
				</div>
			</td>
		</tr>
	<?php endif;

?>
	</tbody>
</table>


<script type='text/javascript'>
	$(document).ready(function(){
		
		$('.edit-option').click(function(){

			data_encode = $(this).attr('data-encode');
			data = jQuery.parseJSON($(this).attr('data-encode')) ;
			$.each(data, function(column, value){
				if(column == 'id'){
					$('#option_id').val(value);
				} else {
					$('#'+column).val(value);
				}

			});

			console.log()

			$('.right-panel').addClass('hide');
			$('#option-category-form-wrapper').removeClass('hide');

		});

		$('.delete-option').click(function(){

			id = $(this).attr('data-id');
			category_id = $(this).attr('data-category');
			
			console.log(id);
			
			swal({   
				title: "Are you sure?",   
				text: "You will deleting this option.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Delete it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'categories/delete_option',
							data: {id : id},
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								
								if(response.result == 1){
									swal("Deleted!", response.msg, "success");  
									$('.back-list').trigger('click');
									get_options(category_id);
								} else {
									swal("Cancelled", response.msg, "error");   
								}
								
									
									
								
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});

		});

	});
</script>
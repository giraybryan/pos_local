<div id="category-form-wrapper" class="col-sm-9 right-panel hide">
	
	<div class="container-fluid widget-content">
	<button class="btn btn-default back-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button>
	<form id="menu-category-form"  class="form-horizontal">
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Category Name:</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="menu_category_name" name="menu_category_name" placeholder="Menu Category Name">
				<input type="hidden" class="form-control" id="id" name="id" placeholder="Item">
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Image</label>
			<div class="col-sm-10">
				<input type="file" name="menu_categories" id="menu_categories" />

				<div class="current_file">
					Current file: <img src="" id="current_image" style='width: 50px'/>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
		</div>
		
	</form>
	</div>
</div>
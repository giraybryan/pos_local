<div id="option-category-form-wrapper" class="col-sm-8 right-panel hide">
	
	<div class="container-fluid widget-content">
	<button class="btn btn-default back-to-details"><span class="glyphicon glyphicon-chevron-left"></span>Back</button>
	<form id="option-category-form"  class="form-horizontal">
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Option Name:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="option_name" name="option_name" placeholder="Menu Category Name">
				<input type="hidden" class="form-control" id="option_id" name="option_id" placeholder="Item">
				<input type="hidden" class="form-control" id="menu_category_id" name="menu_category_id" placeholder="">
			</div>
		</div>
			<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Price</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="price" name="price" placeholder="Option Price">
			</div>
		</div>
		
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
		</div>
		
	</form>
	</div>
</div>
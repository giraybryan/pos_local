<div class="col-sm-12">
	<div class="container-fluid widget-content">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading widget-title">
				RECENTLY Requested Orders 
				<a href="<?php echo base_url('request_orders'); ?>"><span class="pull-right">View All</span></a>
			</div>
			<!-- List group -->
			
			<table class="table table-striped no-margin-bottom">
				<thead>
					<tr>
						<th>ID</th>
						<th>Request Code</th>
						<th>Requested By</th>
						<th>Store Branch</th>
						<th>Date Created</th>
						<th>Status</th>
					</tr> 
				</thead>
				<tbody>
				<?php if(!empty($request_orders)): ?>
					<?php foreach($request_orders as $ro): ?>
					<tr>
						<td><?php echo $ro->id?></td>
						<td><?php echo $ro->RequestCode; ?></td>
						<td><?php echo $ro->first_name.' '.$ro->last_name; ?></td>
						<td><?php echo $ro->branch_name; ?></td>
						<td><?php echo date( 'Y-m-d', strtotime($ro->date_created) ); ?></td>
						<td><?php echo (!empty($ro->Delivered ) ? 'Delivered' : 'Pending' ) ?></td>
					</tr>
					<?php endforeach;?>
				<?php else:?>
					<tr>
						<td colspan="5">No request available</td>
					</tr>
				<?php endif;?>
				</tbody>
			</table>
			
		</div>	
	</div>
</div>




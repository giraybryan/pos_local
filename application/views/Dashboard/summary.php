<div class="col-sm-5">
	<div class="container-fluid widget-content">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading widget-title">Summary</div>
			<!-- List group -->
			
			<table class="table table-striped no-margin-bottom">
				<tr><td>Today</td><td>= <?php echo format_currency($sales['today']->total_sales);?></td></tr>
				<tr><td>Yesterday</td><td>= <?php echo format_currency($sales['yesterday']->total_sales);?></td></tr>
				<tr><td>Last 7days</td><td>= <?php echo format_currency($sales['week']->total_sales);?></td></tr>
				<tr><td>7days before</td><td>= <?php echo format_currency($sales['week2']->total_sales);?></td></tr>
				<tr><td>February</td><td>= <?php echo format_currency($sales['month']->total_sales);?></td></tr>
				
			</table>
			
		</div>	
	</div>
</div>
<div class="col-sm-12">
	<div class="container-fluid widget-content">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading widget-title">
				Inventory Out of stock
				<a href="<?php echo base_url('inventories'); ?>"><span class="pull-right">View All</span></a>
			</div>
			<!-- List group -->
			
			<div class="row">
				<?php foreach ($inventories_out as $key => $inventory): ?>
				<div class="col-sm-2">
					<div class="product-tile">
						<div class="img-container">
								<?php $image_url = (!empty($inventory->image) ? base_url('uploads/item_image/'.$inventory->image) : base_url('assests/img/default.png') ); ?>
								<img class="img-responsive" src="<?php echo $image_url; ?>">
						</div>
						<div class="tile-captions">
							<div class="tile-title"><?php echo $inventory->item?></div>
							<div class="tile-ro-code">Code: <?php echo $inventory->item_code?></div>
						</div>
					</div>
				</div>
				<?php endforeach?>
			</div>
			
		</div>	
	</div>
</div>
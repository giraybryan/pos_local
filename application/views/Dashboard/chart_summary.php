<div class="col-sm-7">
	<div class="container-fluid widget-content">

		<div class="panel panel-default">
			<!-- Default panel contents -->
			
			<div class="panel-body">
				<div style="width:100%">
					<div>
						<div id="canvas" style='height:175px; width: 100%'></div>
					</div>
				</div>
			</div>

			
		</div>	

	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/highchart/highcharts.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/highchart/modules/data.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/highchart/modules/drilldown.js'); ?>"></script>

<?php 
	$month_sale = round( (!empty($sales['month']->total_sales) ? $sales['month']->total_sales : 0) , 3);
	$lastmonth_sale = round( (!empty($sales['lastmonth']->total_sales) ? $sales['lastmonth']->total_sales : 0) , 3);
	$last2month_sale =  round( (!empty($sales['last2months']->total_sales) ? $sales['last2months']->total_sales : 0) , 3);
	$monthly_transactions = array(

			array('name' => 'Monthly Sales','data' => array($last2month_sale,$lastmonth_sale,$month_sale)),
			
		);

	

	$current = date('F');
	$last_month = date('F', strtotime("-1 month"));
	$last2months = date('F', strtotime("-2 month"));
	$monthly_range = array($last2months,$last_month,$current);
	
?>

<script type="text/javascript">
$(document).ready(function(){
	monthly_summary = jQuery.parseJSON('<?php echo json_encode($monthly_transactions);?>');
	monthly_range = jQuery.parseJSON('<?php echo json_encode($monthly_range);?>');

	var chart4 = new Highcharts.Chart({
		chart: {
            renderTo: 'canvas',
            shadow: true,
          
        },
        title: {
            text: 'Monthly Average Sales',
            x: -20 //center
        },
         subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: monthly_range
        },
        yAxis: {
            title: {
                text: ''
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
             pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>Php {point.y} </b></td></tr>',
            footerFormat: '</table>',
        },
        plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: 'Php {point.y}'
				}
			}
		},
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series:monthly_summary,
    });
});	


</script>
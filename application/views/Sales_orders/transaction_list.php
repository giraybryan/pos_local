<h3>Transactions</h3>

<table class="table table-bordered">
	<thead>
		<tr>
			<th>Discount Total</th>
			<th>Discount Amount</th>
			<th>Promo Amount</th>
			<th>Promo total</th>
			<th>Transaction Type</th>
			<th>Date Receipt</th>
			<th>Grand Total</th>
			<th>Cash</th>
			<th>Change</th>
			<th>Details</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($transactions as $transaction):?>
		<tr>
			<td><?php echo $transaction->DiscountTotal ?></td>
			<td><?php echo $transaction->DiscountPercent ?></td>
				<td><?php echo $transaction->PromoPercent ?></td>
			<td><?php echo $transaction->PromoTotal ?></td>
			<td><?php echo $transaction->TransactionType ?></td>
			<td><?php echo $transaction->ReceiptDateIssued ?></td>
		
			<td><?php echo  format_currency($transaction->TotalAmountDue) ?></td>
			<td><?php echo $transaction->Cash ?></td>
			<td><?php echo $transaction->Change ?></td>
			<td><div class="input-group inputgroup-gray">
				<button class="btn view-transaction form-control" data-id="<?php echo $transaction->id ?>" data-encode='<?php echo json_encode($sale_tran); ?>'>Details</button>
			</div></td>
			
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<?php if(!empty($sales)):?>
	<?php foreach($sales as $sale_tran):?>
	<tr>
		<td>
			<?php echo $sale_tran->id?>
		</td>
		<td> <?php echo format_currency($sale_tran->TotalAmount, true)?></td>
		<td> <?php echo  format_currency($sale_tran->TotalAmountDue) ?></td>
		<td><?php echo date('Y-m-d' , strtotime($sale_tran->date_created))?></td>
		<td>
			<div class="input-group inputgroup-gray">
				<button  style='width: 100%; ' class="btn view-item" data-id="<?php echo $sale_tran->id ?>" data-encode='<?php echo json_encode($sale_tran); ?>'>Details</button>
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="5"><?php echo $pagination_link; ?></td>
	</tr>
<?php else: ?>

	<tr>
		<td colspan="5">
			<div class="alert alert-warning alert-dismissible fade in">
				<h5> There are no sales right now! </h5>
			</div>
		</td>
	</tr>
<?php endif;?>
		
		
<script type="text/javascript">
	$(document).ready(function(){
		
		
		
		
		
	});
</script>
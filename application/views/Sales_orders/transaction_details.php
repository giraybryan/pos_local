<table class="table table-stripes">
<thead>
	<tr>
		<th>Quantity</th>
		<th>Item</th>
		<th>Price</th>
		
		<th>Subtotal</th>
	</tr>
</thead>
<tbody>
<?php if(!empty($details)):?>
	<?php foreach($details as $detail):?>


	
	<tr>
		<td><?php echo $detail->itemQty ?></td>	
		<td><?php echo $detail->title ?>
			
		</td>			
		<td><?php echo format_currency($detail->itemPrice) ?></td>
		
		<td><?php echo format_currency($detail->ItemSubTotal) ?></td>		
	</tr>
	<?php 
				if(!empty($detail->SizeOptionID) ):
					$menu_option = get_menu_option($detail->SizeOptionID); ?>
					<tr>
						<td></td>
						<td class="indent-20">Size : <?php echo $menu_option->option_name ?></td>
						<td><?php echo format_currency($menu_option->price) ?></td>
						<td></td>
					</tr>
		<?php  endif;
			?>
	<?php 

		//Added ITEMS
		if(!empty($detail->AdditionalInformation)):
			
			$added_items	=  explode('@', $detail->AdditionalInformation); 

			foreach($added_items as $key => $added_item):

				$_item = explode(',', $added_item);

			
			$invent_item = get_ingredient_info($_item[0]);	
	?>
		
			<tr>
				<td></td>
				<td class="indent-20">
					+ <?php echo $_item[1].' '.$invent_item->item ?>
				</td>
				<td><?php echo format_currency($invent_item->price)?></td>
				<td><?php echo format_currency($invent_item->price * $_item[1])?></td>
			</tr>

	<?php 
			endforeach;

		endif; ?>

	<?php 

		//EXCLUDED ITEMS
		if(!empty($detail->RemoveInformation) && $detail->RemoveInformation != '()' ):
			
			$detail->RemoveInformation = str_replace(array( '(', ')' ), '', $detail->RemoveInformation);
			$exclude_ids	=  explode(',', $detail->RemoveInformation); 

			foreach($exclude_ids as $key => $exclude_id):

			$invent_item = get_ingredient_info($exclude_id);
	?>
		
			<tr>
				<td></td>
				<td class="indent-20">
					- <?php echo $invent_item->item ?>
				</td>
				<td></td>
				<td></td>
			</tr>

	<?php 
			endforeach;

		endif; ?>

	


	<?php endforeach; ?>



	<?php if($transaction->DiscountID): ?>
	<tr>
		<td>Discount Availed </td>
		<td>Discount: <?php echo $transaction->discount_title?> 
			
		</td>
		<td>
			<?php echo get_discount_value($transaction->discount_amount, $transaction->discount_type)?>
		</td>
		<td></td>
	</tr>
	<?php endif; ?>


	<?php if($transaction->CouponCode): ?>
	<tr>
		<td>Coupon </td>
		<td>Code: <?php echo $transaction->CouponCode?> <br/>
			Coupon title : <?php echo $transaction->coupon_title?>
		</td>
		<td>
			<?php echo get_discount_value($transaction->coupon_amount, $transaction->coupon_type)?>
		</td>
		<td></td>
	</tr>
	<?php endif; ?>


	<?php if($transaction->promo_title): ?>
	<tr>
		<td>Promo Availed </td>
		<td><br/>
			Description : <?php echo $transaction->promo_description?> <br/>

			<?php 
				$item_ids = array();
				//echo "<pre>"; print_R($transaction).' sd'; 
				if(!empty($transaction->promo_item_ids)){

				$item_ids = explode(',', $transaction->promo_item_ids);

				}
			?>
			Promo: <?php echo get_promo_value($transaction->promo_amount, $transaction->promo_type, $item_ids )?>
		</td>
		<td>
			
		</td>
		<td></td>
	</tr>
	<?php endif; ?>

	<tr>
		<td></td>
		<td></td>
		<td>Grand Total</td>
		<td><?php echo format_currency($transaction->TotalAmountDue) ?></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td>Cash</td>
		<td><?php echo format_currency($transaction->Cash) ?></td>
	</tr>
		<tr>
		<td></td>
		<td></td>
		<td>Change</td>
		<td><?php echo format_currency($transaction->Change) ?></td>
	</tr>
	<!--<tr>
		<td colspan="4"><?php echo $pagination_link; ?></td>
	</tr>-->
<?php else: ?>

	<tr>
		<td colspan="4">
			<div class="alert alert-warning alert-dismissible fade in">
				<h5> There are no details for this transaction! </h5>
			</div>
		</td>
	</tr>
<?php endif;?>
</tbody>
</table>
		

<script type="text/javascript">
$(function() {
	$("#advance_search #project_id").select2({
		placeholder: "Select Project",
		allowClear: true
	});
	
	$("#advance_search #user_id").select2({
		placeholder: "Select Employee",
		allowClear: true
	});
	
	$("#advance_search #position_id").select2({
		placeholder: "Select Position",
		allowClear: true
	});
	$("#advance_search #status_id").select2({
		placeholder: "Select Status",
		allowClear: true
	});
	
	$("#advance_search #stage_id").select2({
		placeholder: "Select Stage",
		allowClear: true
	});
	
	$("#user_form #position_id").select2({
		placeholder: "Select Position",
		allowClear: true
	});
	
	$("#user_form #userlevel_id").select2({
		placeholder: "Select Userlevel",
		allowClear: true
	});
	
	$("#select_all").click(function(){
		 if(this.checked){
			$(".chk").each(function(){
				this.checked=true;
			})             
		}else{
			$(".chk").each(function(){
				this.checked=false;
			})             
		}
	});

	$(".chk").click(function () {
		if (!$(this).is(":checked")){
			$("#select_all").prop("checked", false);
		}else{
			var flag = 0;
			$(".chk").each(function(){
				if(!this.checked)
				flag=1;
			})             
			if(flag == 0){ $("#select_all").prop("checked", true);}
		}
	});
});

function areyousure()
{
	return confirm("Do you really want to delete this client record?");
}
</script>
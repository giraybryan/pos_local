

<div class="row">
	
	<div id="company-list-wrapper" class="col-sm-12 right-panel">
	
		<div class="container-fluid widget-content list-container">
			<div class="">
			</div>
			
			<div class="page-title">
				<h4><?php echo $page?></h4>
			</div>
			<form id="company-form"  class="form-horizontal">
				
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Logo</label>
					<div class="col-sm-10">
						<input type="file" name="company_logo" id="company_logo" />
						<?php if( !empty( $info->image ) ): ?>
							<div class="current_file">
								<img id="current_image" style='width: 150px' src="<?php echo base_url('uploads/company_logo/'.$info->image)?>"/>
							</div>
						<?php endif; ?>
					</div>
				</div>

				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Company Name:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="name" name="name" value="<?php echo ($info->name ? $info->name: '')?>" placeholder="Company Name">
						<input type="hidden" class="form-control" id="id" name="id" value="<?php echo ($info->id ? $info->id: '')?>" placeholder="Company Name">
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Slogan</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="slogan" name="slogan" value="<?php echo ($info->slogan ? $info->slogan: '')?>"  placeholder="Slogan">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Phone number</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="phone" name="phone" value="<?php echo ($info->slogan ? $info->phone: '')?>"  placeholder="Phone number">
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Address:</label>
					<div class="col-sm-10">
						<textarea name="address" id="address" class="form-control" placeholder="Address"><?php echo ($info->address ? $info->address: '')?></textarea> 
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Address2:</label>
					<div class="col-sm-10">
						<textarea name="address2" id="address2" class="form-control" placeholder="Address"><?php echo ($info->address ? $info->address2: '')?></textarea> 
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Receipt Offical Message</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="receipt_official_msg" name="receipt_official_msg" value="<?php echo ($info->receipt_official_msg ? $info->receipt_official_msg: '')?>"  placeholder="Official Receipt Message">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Receipt Greetings Message</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="receipt_official_msg" name="receipt_greeting_msg" value="<?php echo ($info->receipt_greeting_msg ? $info->receipt_greeting_msg: '')?>"  placeholder="Greetings ">
					</div>
				</div>
				
				
				
				
				
				
				
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label"></label>
					<div class="col-sm-10">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
				
			</form>	
		</div>

	
	</div>

</div>



<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/modules/company.js');?>"></script>
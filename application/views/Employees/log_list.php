<table class="table table-stripes">
<thead>
	<tr>
		<th>DeviceID</th>
		<th>Login</th>
		<th>Logout</th>
		<th>Change Fund</th>
	</tr>
</thead>
<tbody>
<?php if(!empty($logs)):?>
	<?php foreach($logs as $log):?>


	
	<tr>
		<td><?php echo $log->DeviceSerialID ?></td>
		<td><?php echo $log->Login ?></td>
		<td> <?php echo ($log->Logout != '1970-01-01 01:00:00' ? date('Y-m-d H:i:s', strtotime($log->Logout)) : '') ?></td>
		<td> <?php echo $log->ChangeFund ?></td>
		
	</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="4"><?php echo $pagination_link; ?></td>
	</tr>
<?php else: ?>

	<tr>
		<td colspan="4">
			<div class="alert alert-warning alert-dismissible fade in">
				<h5> There are no logs right now! </h5>
			</div>
		</td>
	</tr>
<?php endif;?>
</tbody>
</table>
		

<div id="employee-form-wrapper" class="col-sm-8 right-panel hide">
	
	<div class="container-fluid widget-content">
	<button class="btn btn-default back-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button>
	<form id="employee-form"  class="form-horizontal">
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">First name:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
				<input type="hidden" class="form-control" id="id" name="id" placeholder="">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Last Name:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">phone:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="phone" name="phone" placeholder="Phone">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Email:</label>
			<div class="col-sm-9">
				<input type="email" class="form-control" id="email" name="email" placeholder="Email" />
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Password:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="password" name="password" placeholder="Password" />
			</div>
		</div>
		
			<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Shift:</label>
			<div class="col-sm-9">
				<select name="shift" id="shift" placeholder="Select Shift" class="select">
					<option value=""></option>
					<?php foreach($shifts as $shift):?>
					<option value='<?php echo $shift->id?>'><?php echo $shift->shift_name?></option>
					<?php endforeach;?>
					
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Branch:</label>
			<div class="col-sm-9">
				<select name="branch_id" id="branch_id" placeholder="Select Branch" class="select">
					<option value=""></option>
					<?php foreach($branches as $branch):?>
					<option value='<?php echo $branch->id?>'><?php echo $branch->branch_name?></option>
					<?php endforeach;?>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
		</div>
		
	</form>
	</div>
</div>
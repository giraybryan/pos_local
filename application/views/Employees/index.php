<link href="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.min.css')?>"  rel="stylesheet">
<div class="row">
	<?php $this->load->view($module.'/quick_search');?>
	<div id="employee-list-wrapper" class="col-sm-9 right-panel">
		
		<div class="container-fluid widget-content list-container">
			<div class="">
			</div>
			<div class="page-title">
				<h4><?php echo $page?></h4>
			</div>
			<button id="add-item" class="btn btn-default add-item">Add Employee</button>
			
			<table class="table table-stripes">
				<thead class="center-text">
					<tr>
						<th>First Name</th>
						<th>Last Name </th>
						<th>Branch</th>	
						<th></th>
					</tr>
				</thead>
				<tbody class="center-text" id="list_container">
				<?php $this->load->view($module.'/list');?>
				</tbody>
			</table>
		</div>

		<div class="container-fluid widget-content view-container hide">
			
			<table class="table table-bordered">
				<tr>
					<td><button class="btn btn-default back-to-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button></td>
					<td><input type="button"  class="btn btn-default edit-item" value='EDIT'  data-encode="" data-id="" id="id_view"/></td>
				</tr>
				<tr>
					<td style=''>First Name</td>
					<td id="first_name_view"></td>
				</tr>
				<tr>
					<td style=''>Last Name</td>
					<td id="last_name_view"></td>
				</tr>
				<tr>
					<td>Phone</td>
					<td id="phone_view"></td>
				</tr>
				<tr>
					<td>Email</td>
					<td id="email_view"></td>
				</tr>
			
				
				<tr>
					<td>Shift</td>
					<td id="shift_name_view">
						
					</td>
				</tr>
				<tr>
					<td>Branch</td>
					<td id="branch_name_view">
						
					</td>
				</tr>
			
				
				
			</table>

		</div>
	</div>


	<?php $this->load->view($module.'/form');?>
	
</div>

<div class="hide" id="log_modal">
	 <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">LOGS</h4>
      </div>
      <div class="modal-body" id="modal-body">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
</div>


<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/modules/employees.js');?>"></script>
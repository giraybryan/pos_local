<div class="col-sm-3">
	<div class="widget-container">
		<div class="widget-title">
			<h4>Quick Search</h4>
		</div>

		<div class="widget-body">
			<label>Keyword</label>
			
				<input type="text" class="form-control" id="filter_keyword" placeholder="Search Keyword"/>
			

			<label>Category</label>
				<select class="select" id="filter_category" placeholder="Select Category">
					<option value=""></option>
					<?php foreach($categories as $category): ?>
					<option value="<?php echo $category->id?>"><?php echo $category->name ?></option>
					<?php endforeach;?>
				</select>
			<label>Branch</label>
				<select class="select" id="filter_branch" placeholder="Select Branch">
					<option value=""></option>
					<?php foreach($branches as $branch): ?>
					<option value="<?php echo $branch->id?>"><?php echo $branch->branch_name ?></option>
					<?php endforeach;?>
			</select>
		</div>
			
	</div>
	
	
	
	
</div>
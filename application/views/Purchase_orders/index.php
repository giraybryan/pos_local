
<div class="row">
	<?php $this->load->view($module.'/quick_search');?>
	<div id="sales-list-wrapper" class="col-sm-9 right-panel">
	
		<div class="container-fluid widget-content list-container">
			<div class="">
			</div>
			<div class="page-title">
				<h4><?php echo $page?></h4>
			</div>
		
			<table class="table table-stripes">
				<thead class="center-text">
					<tr>
						<th>Image </th>
						<th>Item  </th>
						<th>Branch</th>
						<th>Request Quantity</th>
						<th>Date Requested </th>
						<th>Status </th>
						<th></th>
					</tr>
				</thead>
				<tbody class="center-text" id="list_container">
				<?php $this->load->view($module.'/list');?>
				</tbody>
			</table>
		</div>

		<div class="container-fluid widget-content view-container hide">
			
			<table class="table table-bordered">
				<tr>
					<td colspan="2"><button class="btn btn-default back-to-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button></td>
					
				</tr>
				<tr>
					<td style=''>Total Vat</td>
					<td id="TotalVat_view"></td>
				</tr>
				<tr>
					<td>TotalDiscount </td>
					<td id="TotalDiscount_view"></td>
				</tr>
				<tr>
					<td>Total Promo </td>
					<td id="TotalPromo_view"></td>
				</tr>
				<tr>
					<td>TotalDiscount </td>
					<td id="TotalDiscount_view"></td>
				</tr>
				<tr>
					<td>Total Amount </td>
					<td id="TotalAmount_view"></td>
				</tr>
				<tr>
					<td>Total Amount Due </td>
					<td id="TotalAmountDue_view"></td>
				</tr>
				<tr>
					<td>Total Senior Discount </td>
					<td id="TotalSeniorDiscount_view"></td>
				</tr>
				<tr>
					<td>Cashier</td>
					<td><span id="first_name_view"></span> <span id="last_name_view"></span></td>
				</tr>
				
				
				
			</table>
			
			<div id="transaction_wrapper">

			</div>
		</div>
	</div>


	
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/modules/purchase_orders.js');?>"></script>
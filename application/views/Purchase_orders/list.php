<?php if(!empty($pos)):?>
	<?php foreach($pos as $po):?>
	<tr>
		<td>
			<?php  ?>
			<div class="image-container">
				<?php
					if(!empty($po->image)){
						$image_url = base_url('uploads/item_image/'.$po->image);
					}  else {
						$image_url = base_url('assets/img/default.png');
					}
				 ?>
				<img class="" src="<?php echo $image_url ?>"/>
			</div>
		</td>
		<td><?php echo $po->item?></td>
		<td> <?php echo $po->branch_name?></td>
		<td> <?php echo $po->RequestQty?></td>
		<td><?php echo date('Y-m-d' , strtotime($po->RequestData))?></td>
		<td><?php echo ($po->Delivered == 1 ? 'Delivered': 'Pending')?></td>
		<td>
			<div class="input-group inputgroup-gray">
				<button class="btn view-item" data-id="<?php echo $po->id ?>" data-encode='<?php echo json_encode($po); ?>'>Details</button>
				<button class="btn update-item" data-id="<?php echo $po->id ?>" data-encode='<?php echo json_encode($po); ?>'>Update</button>
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="5"><?php echo $pagination_link; ?></td>
	</tr>
<?php else: ?>

	<tr>
		<td colspan="5">
			<div class="alert alert-warning alert-dismissible fade in">
				<h5> There are no pending P.O request right now! </h5>
			</div>
		</td>
	</tr>
<?php endif;?>
		
		
<script type="text/javascript">
	$(document).ready(function(){
		
		
		
		
		
	});
</script>
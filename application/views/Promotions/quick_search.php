<div class="col-sm-3">
	<div class="widget-container">
		<div class="widget-title">
			<h4>Quick Search</h4>
		</div>
		<div class="widget-body">
			<label>Keyword Search</label>
			<input type="text" class="form-control" id="filter_keyword" placeholder="Search Keyword"/>
		</div>
	</div>
	
	<div class="widget-container">
		
		<div class="widget-body">
			
			<label>Branch</label>
			<select class="select" id="filter_branch" placeholder="Filter Branch">
				<option value=""></option>
				<?php foreach($branches as $branch): ?>
				<option value="<?php echo $branch->id?>"><?php echo $branch->branch_name ?></option>
				<?php endforeach;?>
			</select>
			
			
			
		</div>
	</div>
	
	
</div>
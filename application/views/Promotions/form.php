<div id="promotion-form-wrapper" class="col-sm-8 right-panel hide">
	
	<div class="container-fluid widget-content">
	<!--<button class="btn btn-default back-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button>-->
	<form id="promotion-form"  class="form-horizontal">
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Promo Name:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="title" name="title" placeholder="Coupon Title">
				<input type="hidden" class="form-control" id="id" name="id" placeholder="">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Description:</label>
			<div class="col-sm-9">
				<textarea name="description" id="description" class="form-control"></textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Promo Item(s):</label>
			<div class="col-sm-9">
				<input type="hidden" id="promo_item_ids" name="promo_item_ids" style="width:300px" class="input-xlarge" />
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Start Date:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control datepick" id="start_date" name="start_date" placeholder="Start Date">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">End Date:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control datepick" id="end_date" name="end_date" placeholder="End Date">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Promo Type:</label>
			<div class="col-sm-9">
				<select name="reduction_type" id="reduction_type" placeholder="Select Discount Type" class="select">
					<option value=""></option>
					<?php foreach($promotion_types as $promotion_type):?>
							<option value="<?php echo $promotion_type->id ?>"><?php echo $promotion_type->promotion_type_name?></option>
					<?php endforeach;?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Promo Amount:</label>
			<div class="col-sm-9">
				<input type="text" name="reduction_amount" class="form-control" id="reduction_amount" placeholder="Promo Amount" />
			</div>
		</div>

		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Max Use Coupon:</label>
			<div class="col-sm-9">
				<input type="text" name="max_use" class="form-control" id="max_use" placeholder="Max no. of use" />	
			</div>
		</div>

		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Required Items:</label>
			<div class="col-sm-9">
				<input type="hidden" id="required_item_ids" name="required_item_ids" style="width:300px" class="input-xlarge" />
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Required Quantity:</label>
			<div class="col-sm-9">
				<input type="text" name="required_quantity" class="form-control" id="required_quantity" placeholder="Promo Required Item quantity"/>	
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Required Amount:</label>
			<div class="col-sm-9">
				<input type="text" name="required_amount" class="form-control" id="required_amount" placeholder="Promo Required Amount" />
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Branch:</label>
			<div class="col-sm-9">
				<select name="branch_id" id="branch_id" placeholder="Select Branch" class="select">
					<option value=""></option>
					<?php foreach($branches as $branch):?>
					<option value='<?php echo $branch->id?>'><?php echo $branch->branch_name?></option>
					<?php endforeach;?>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
		</div>
		
	</form>
	</div>
</div>
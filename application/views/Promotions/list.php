<?php if(!empty($promotions)):?>
	<?php foreach($promotions as $promotion):?>


	<?php 
		$promotion->required_item_ids = get_field_menu_value($promotion->required_item_ids);
		$promotion->promo_item_ids = get_field_menu_value($promotion->promo_item_ids);
	?>
	<tr>
		
		<td><?php echo $promotion->promo_code?></td>
		<td><?php echo $promotion->title?></td>
		<td> <?php echo $promotion->promotion_type_name?></td>
		<td><?php 
			switch($promotion->reduction_type){
				case 1:
					echo $promotion->reduction_amount;
					break;
				case 2:
					 echo format_currency($promotion->reduction_amount);
					break;
				case 3: 
				echo $promotion->reduction_amount.'%';
				
					break;

			}

		 ?></td>
		<td><?php echo $promotion->branch_name?></td>
		<td><?php echo $promotion->start_date?></td>
		<td><?php echo $promotion->end_date?></td>
		<td>
			
				<button class="btn view-item" data-id="<?php echo $promotion->id ?>" data-encode='<?php echo json_encode($promotion); ?>' data-toggle="tooltip" data-placement="bottom" title="View Details" ><span class="glyphicon glyphicon-eye-open"></span></button>
				<!--<button class="btn edit-item" data-id="<?php echo $promotion->id?>" data-encode='<?php echo json_encode($promotion); ?>' data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button>
				<button class="btn delete-item" data-id="<?php echo $promotion->id?>" data-toggle="tooltip" data-placement="bottom" title="Delete" ><span  class="glyphicon glyphicon-trash"></span></button> -->

				
			
		</td>
	</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="6"><?php echo $pagination_link; ?></td>
	</tr>
<?php else: ?>

	<tr>
		<td colspan="5">
			<div class="alert alert-warning alert-dismissible fade in">
				<h5> There are no menu right now! </h5>
			</div>
		</td>
	</tr>
<?php endif;?>
		
		
<script type="text/javascript">
	$(document).ready(function(){
		
		$('[data-toggle="tooltip"]').tooltip();
		
		
		$('.delete-item').click(function(){
			id = $(this).attr('data-id');
			
			console.log(id);
			
			swal({   
				title: "Are you sure?",   
				text: "You will deleting this promotion.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Delete it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'promotions/delete',
							data: {id : id},
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
							
								if(response.result == 1){
									swal("Deleted!", response.msg, "success");  
									$('.back-list').trigger('click');
									get_list();
								} else {
									swal("Cancelled", response.msg, "error");   
								}
									
									
								
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		});
		
	});
</script>
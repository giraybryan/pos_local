<?php if(!empty($discounts)):?>
	<?php foreach($discounts as $discount):?>
	<tr>
		
		<td><?php echo $discount->title?></td>
		<td>  <?php echo $discount->discount_type_name?></td>
		<td><?php 
				switch($discount->reduction_type){
					case 1:
					echo $discount->reduction_amount.'%';
						break;
					case 2:
					echo format_currency($discount->reduction_amount);
						break;
					case 3: 
					echo '% '.$discount->reduction_amount;
						break;

				}
				

			?></td>
		<td>
			<div class="input-group inputgroup-gray">
				
			<!--	<button class="btn edit-item" data-id="<?php echo $discount->id?>" data-encode='<?php echo json_encode($discount); ?>'>Edit</button>
				<button class="btn delete-item" data-id="<?php echo $discount->id?>" >Remove</button> -->
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="6"><?php echo $pagination_link; ?></td>
	</tr>
<?php else: ?>

	<tr>
		<td colspan="5">
			<div class="alert alert-warning alert-dismissible fade in">
				<h5> There are no menu right now! </h5>
			</div>
		</td>
	</tr>
<?php endif;?>
		
		
<script type="text/javascript">
	$(document).ready(function(){
		
		
		
		$('.delete-item').click(function(){
			id = $(this).attr('data-id');
			
			console.log(id);
			
			swal({   
				title: "Are you sure?",   
				text: "You will deleting this discount.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Delete it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'discounts/delete',
							data: {id : id},
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
							
								if(response.result == 1){
									swal("Deleted!", response.msg, "success");  
									$('.back-list').trigger('click');
									get_list();
								} else {
									swal("Cancelled", response.msg, "error");   
								}
									
									
								
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		});
		
	});
</script>
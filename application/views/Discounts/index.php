
<div class="row">
	<?php $this->load->view($module.'/quick_search');?>
	<div id="category-list-wrapper" class="col-sm-9 right-panel">
	
		<div class="container-fluid widget-content list-container">
			<div class="">
			</div>
			
			<div class="page-title">
				<h4><?php echo $page?></h4>
			</div>
			
			<!--<button id="add-item" class="btn btn-default add-item">Add Discount</button> -->
			
			<table class="table table-stripes">
				<thead class="center-text">
					<tr>
						<th>Title </th>
						<th>Discount Type </th>
						<th>Discount Amount </th>
						
						<th></th>
					</tr>
				</thead>
				<tbody class="center-text" id="list_container">
				<?php $this->load->view($module.'/list');?>
				</tbody>
			</table>
		</div>

		<div class="container-fluid widget-content view-container hide">
			
			<table class="table table-bordered">
				<tr>
					<td><button class="btn btn-default back-to-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button></td>
					<td><input type="button"  class="btn btn-default edit-item" value='EDIT'  data-encode="" data-id="" id="id_view"/></td>
				</tr>
				<tr>
					<td style=''>Title</td>
					<td id="title_type_view"></td>
				</tr>
				<tr>
					<td>Discount Type</td>
					<td id="reduction_type_view"></td>
				</tr>
				<tr>
					<td>Discount Amount</td>
					<td id="reduction_amount_view"></td>
				</tr>
				
				
				
			</table>

		</div>
	</div>


	<?php $this->load->view($module.'/form');?>
	
</div>



<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/modules/discounts.js');?>"></script>
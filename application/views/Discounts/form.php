<div id="discount-form-wrapper" class="col-sm-8 right-panel hide">
	
	<div class="container-fluid widget-content">
	<button class="btn btn-default back-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button>
	<form id="discount-form"  class="form-horizontal">
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Discount Title:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="title" name="title" placeholder="Menu Title">
				<input type="hidden" class="form-control" id="id" name="id" placeholder="">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Discount Type:</label>
			<div class="col-sm-9">

				<select name="reduction_type" id="reduction_type" placeholder="Select Discount Type" class="select">
					<option value=""></option>

					<?php foreach($discount_types as $discount_type):?>
						<?php if($discount_type->id != 3 ):?>
							<option value="<?php echo $discount_type->id ?>"><?php echo $discount_type->discount_type_name?></option>
						<?php endif; ?>
					<?php endforeach;?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Discount Amount:</label>
			<div class="col-sm-9">
				<input type="text" name="reduction_amount" class="form-control" id="reduction_amount" />
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
		</div>
		
	</form>
	</div>
</div>
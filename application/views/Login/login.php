<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
    <title>FORWARD POS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url('assets/js/html5shiv.js')?>"></script>
    <![endif]-->
 
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/ico/apple-touch-icon-144-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/ico/apple-touch-icon-114-precomposed.png')?>">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/ico/apple-touch-icon-72-precomposed.png')?>">
                    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/ico/apple-touch-icon-57-precomposed.png')?>">
                                   <link rel="shortcut icon" href="<?php echo base_url('assets/ico/favicon.jpg')?>">
        
    <!-- Styles -->
    <link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/styles.css')?>" />    
    <script src="<?php echo base_url('assets/js/jquery/jquery-1.10.2.min.js')?>"></script>
	
</head>
<body>

    <!-- Wrap all page content here -->
    <div id="wrap">
    
        <div class="login-bg">
            <div class="login-logo">
                <?php echo img(array("src"=>base_url("assets/img/pos-logo.png"))); ?>
            </div>
            <div class="login-box">
                <div>
                	
                    <?php if($this->session->flashdata('message') || $this->session->flashdata('error')): ?>
						<?php
                            //lets have the flashdata overright "$message" if it exists
                            if($this->session->flashdata('message')){
                                $message	= $this->session->flashdata('message');
                            }
                            
                            if($this->session->flashdata('error')){
                                $error	= $this->session->flashdata('error');
                            }
                            
                            if(function_exists('validation_errors') && validation_errors() != ''){
                                $error	= validation_errors();
                            }
                        ?>        
                        <?php if (!empty($message)): ?>
                            <div class="alert alert-success">
                                <a class="close" data-dismiss="alert">×</a>
                                <?php echo $message; ?>
                            </div>
                        <?php endif; ?>
                    
                        <?php if (!empty($error)): ?>
                            <div class="alert alert-danger">
                                <a class="close" data-dismiss="alert">×</a>
                                <?php echo $error; ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                
                </div>
                <?php echo form_open('login') ?>
                    <div class="form-name">
                        <div class="label-ttle">Username</div>
                        <input type="text" value="" class="login-input" name="username" id="username" autocomplete="off" placeholder="Username" required>
                    </div>
                    <div class="form-name">
                        <div class="label-ttle">Password</div>
                        <input type="password" value="" class="login-input" name="password" id="password" autocomplete="off" placeholder="Password" required>
                    </div>
                    <div class="login-button">
                        <div class="orange-button-login">
                            <input type="submit" id="login" name="login" value="<?php echo lang('enter');?>" class="btn-login" />
                            <input type="hidden" value="<?php echo $redirect; ?>" name="redirect"/>
                        </div>
                    </div>
                    <div class="form-name" style="margin-bottom: 0">
                        <label class="checkbox">
                            <input type="checkbox" name="remember" value="remember-me"> Remember me
                        </label>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        
    </div>
    
    <!-- <div id="footer">
    	<div class="container-fluid">
            <p class="text-muted">© 2014 by <a href="http//:forward.ph">Forward.ph</a>. All rights reserved</p>
        </div>
	</div> -->

	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <link href="<?php echo base_url('assets/js/jquery/smoothness/jquery-ui-1.10.4.custom.min.css')?>" rel="stylesheet" type="text/css">
    
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery/jquery-ui-1.10.4.custom.min.js')?>"></script>

</body>
</html>
<?php if(!empty($userlevels)):?>
	<?php foreach($userlevels as $userlevel):?>
	<tr>
		
		<td><?php echo $userlevel->userlevel?></td>
		<td>
			<div class="input-group inputgroup-gray">
				
				<a href="<?php echo base_url('userlevels/permissions/'.$userlevel->id); ?>" class="btn edit-item" >Module access</a>
				<button class="btn edit-item" data-id="<?php echo $userlevel->id?>" data-encode='<?php echo json_encode($userlevel); ?>'>Edit</button>
				<button class="btn delete-item" data-id="<?php echo $userlevel->id?>" >Remove</button>
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="6"><?php echo $pagination_link; ?></td>
	</tr>
<?php else: ?>

	<tr>
		<td colspan="5">
			<div class="alert alert-warning alert-dismissible fade in">
				<h5> There are no menu right now! </h5>
			</div>
		</td>
	</tr>
<?php endif;?>
		
		
<script type="text/javascript">
	$(document).ready(function(){
		
		
		
		$('.delete-item').click(function(){
			id = $(this).attr('data-id');
			
			console.log(id);
			
			swal({   
				title: "Are you sure?",   
				text: "You will deleting this userlevel.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Delete it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'userlevels/delete',
							data: {id : id},
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
							
								if(response.result == 1){
									swal("Deleted!", response.msg, "success");  
									$('.back-list').trigger('click');
									get_list();
								} else {
									swal("Cancelled", response.msg, "error");   
								}
									
									
								
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		});
		
	});
</script>
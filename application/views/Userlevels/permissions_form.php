
<?php
	$attributes = array('name' => 'userpriv', 'id' => 'userpriv');
	echo form_open(site_url("userlevels/permissions"), $attributes);
?>
<input type="hidden" name="userlevel_id" id="userlevel_id" value="<?php echo $userlevel->id ?>">
<table class="table table-stripped">
	
	
    <tbody>
		 <tr>
            <th>Dashboard Stages</th>
			<?php foreach($this->useraccess->getPermissionTypes() as $_k => $_v ):?>
            <th style="text-align:center;text-decoration:capitalize;">
            	<?php echo $_v['permission_type']?>
            </th>
			<?php endforeach; ?>
        </tr>
		<?php foreach($modules as $key => $val):?>
			<tr> 
				<td><?php echo $val?></td>
				<?php foreach($this->useraccess->getPermissionTypes() as $_k => $_v ):?>
					<td style="text-align:center;">
						<input type="checkbox" <?php echo ($this->useraccess->getcurrent_permission($userlevel->id, $val, $_k+1 ) == 1? 'checked ' : '')?> <?php echo ($userlevel->id == -1 ? 'checked disabled ': '') ?>name="<?php echo 'module['.str_replace(' ','_',$val).']['.$_v['id'].']' ?>" value="<?php echo $_v['permission_type']?>">
					</td>
				<?php endforeach; ?>
			</tr> 
		<?php endforeach; ?>
		
		
		
		
    </tbody>
	
</table>

<div class="show-grid">
    <div class="col-md-12 pull-right">
		<?php if($CanEdit):?>
        <button type="submit" id="update" class="btn btn-warning">
            <span class="glyphicon glyphicon-floppy-disk"></span>
            Save
        </button>
		<?php endif; ?>
    </div>
</div>  
<div class="clearfix"> </div>
<?php echo form_close(); ?>   


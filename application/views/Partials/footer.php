		<!--	</div>
-->			<!--container-fluid page-content-->

        </div>
        </div>
    </div>


<div id="modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
     
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
    
    <?php if($this->user_session->userdata('user')): ?>
        <div id="footer">
        	<div class="container-fluid container">
                <p class="text-muted">© 2015 by <a href="http://forward.ph">Forward.ph</a>. All rights reserved</p>
            </div>
    	</div>
    <?php endif; ?>

	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/js/jquery/jquery-ui-1.10.4.custom.min.js')?>" type="text/javascript"></script>
    
    <link href="<?php echo base_url('assets/css/jquery.rating.css')?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('assets/css/bootstrap-spinedit.css')?>" rel="stylesheet" type="text/css">
    <?php /*?><link href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css')?>" rel="stylesheet" type="text/css"/><?php */?>
    <link href="<?php echo base_url('assets/css/bootstrap-fileupload.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/jquery.datetimepicker.css')?>" rel="stylesheet" type="text/css" />
    
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-spinedit.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-fileupload.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.datetimepicker.js')?>"></script>
    
    <script src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/jquery.cycle.all.js')?>" type="text/javascript"></script>
    <?php /*?><script src="<?php echo base_url('assets/js/select2.js')?>" type="text/javascript"></script><?php */?>
    <script src="<?php echo base_url('assets/js/number_format.js')?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/jquery.rating.pack.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/jquery.unveil.min.js')?>" type="text/javascript"></script>
    
    <script src="<?php echo base_url('assets/js/moment-2.4.0.js')?>" type="text/javascript"></script>
	
    <script src="<?php echo base_url('assets/js/script.js')?>" type="text/javascript"></script>
    <?php /*?><script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.ru.js')?>" type="text/javascript"></script><?php */?>
</body>
</html>
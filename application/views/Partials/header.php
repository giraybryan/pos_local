<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
    <title>FORWARD POS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url('assets/js/html5shiv.js')?>"></script> 
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/ico/apple-touch-icon-144-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/ico/apple-touch-icon-114-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/ico/apple-touch-icon-72-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/ico/apple-touch-icon-57-precomposed.png')?>">
    <link rel="shortcut icon" href="<?php echo base_url('assets/ico/favicon.jpg')?>">
        
    <!-- Styles -->
    <link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/normalize.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/styles.css')?>" rel="stylesheet" type="text/css" />    
    <link href="<?php echo base_url('assets/plugins/sweetalert/sweet-alert.css')?>" rel="stylesheet" type="text/css" />    
    <?php /*?><link href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css')?>" rel="stylesheet" type="text/css" media="screen" /><?php */?>
    <link href="<?php echo base_url('assets/js/jquery/smoothness/jquery-ui-1.10.4.custom.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/plugins/nailthumb/jquery.nailthumb.1.1.min.css')?>" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    
    <script src="<?php echo base_url('assets/js/jquery/jquery-1.10.2.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/chartjs/Chart.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/sweetalert/sweet-alert.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/nailthumb/jquery.nailthumb.1.1.min.js')?>"></script>




    <script src="<?php echo base_url('assets/js/select2.js')?>"></script>
	<link href="<?php echo base_url('assets/css/select2.css')?>" rel="stylesheet">
	
	<script type="text/javascript">
		var base_url = '<?php echo base_url(); ?>';
		var ajax_loader = '<div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div>';
	</script>
    
<?php $this->load->view("scripts.php"); ?>
	
</head>

<body class="body-bg">

    <!-- Wrap all page content here -->
    <div id="wrap">

        <!-- Fixed navbar -->
        <div class="navbar-fixed-top" id="lhoopa-nav" role="navigation">
            <div class="container-fluid container">
                <div style="float: left;">
                    <a class="navbar-brand" href="<?php echo base_url("/");?>">
                    	<?php echo img(array("src"=>base_url("assets/img/pos-logo.png"),"class"=>"img-responsive")); ?>
                    </a>
                </div>
                <div>
                    <ul class="nav navbar-nav2 pull-right topright-menu">
						<li class="<?php echo ($this->uri->segment(1) == 'company' || $this->uri->segment(1) == 'users' || $this->uri->segment(1) == 'userlevels' || $this->uri->segment(1) == 'employees' ? 'active' : '') ?>"><a href="#"><span class="sprite sprite-inline sprite-gear"></span>Settings</a>
                             <div class="dropdown">
                                    <ul>
                                        <li class="<?php echo ($this->uri->segment(1) == 'company' ? 'active': '') ?>"><a href="<?php echo base_url('company') ?>">Company</a></li>
                                        <li class="<?php echo ($this->uri->segment(1) == 'users' ? 'active': '') ?>"><a href="<?php echo base_url('users') ?>">Users</a></li>
                                        <li class="<?php echo ($this->uri->segment(1) == 'userlevels' ? 'active': '') ?>"><a href="<?php echo base_url('userlevels') ?>">Userlevels</a></li>
                                        <li class="<?php echo ($this->uri->segment(1) == 'employees' ? 'active': '') ?>"><a href="<?php echo base_url('employees') ?>">Employees</a></li>
                                    </ul>
                            </div>    
                        </li>
                        <li class="<?php echo ($this->uri->segment(1) == 'reports' ? 'active' : '') ?>"><a href="<?php echo base_url('reports')?>"><span class="sprite sprite-inline sprite-report"></span>Report</a></li>
                        
						<li class=""><a href="<?php echo base_url('login/logout');?>"><span class="sprite sprite-inline sprite-user"></span>Logout</a></li>
						<!--<li class=""><a href="#"><span class="sprite sprite-inline sprite-bell"></span>Bell</a></li> -->
						
						
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
        
        <!-- Begin page content -->
        <div class="container-fluid">
        
        	<div class="row">
            	<div>
                	<div class="navbar navbar-default">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse default-background">
                            <ul class="nav navbar-nav submenu">
                               
							   <li>

                                <a class="<?php echo ($this->uri->segment(1) == 'inventories' ? 'active' : '') ?>" href="<?php echo base_url('inventories'); ?>">
                                     <span class="sprite2 sprite-block sprite-inventory"></span>
                                    Inventories
                                    </a>
                                </li>
							   <li>
                                    <a class="<?php echo ($this->uri->segment(1) == 'categories' ? 'active' : '') ?>"  href="<?php echo base_url('categories'); ?>">
                                       <span class="sprite2 sprite-block sprite-category_icon"></span>
                                        Categories
                                    </a>
                                </li>
							   <li>
                                    <a class="<?php echo ($this->uri->segment(1) == 'menus' ? 'active' : '') ?>" href="<?php echo base_url('menus')?>">
                                        <span class="sprite2 sprite-block sprite-menu_icon"></span>   
                                        Menu Packages
                                    </a>
                                </li>
							   <li>
                                    <a class="<?php echo ($this->uri->segment(1) == 'promotions' ? 'active' : '') ?>"  href="<?php echo base_url('promotions')?>">
                                       <span class="sprite2 sprite-block sprite-promotion_icon"></span>   
                                        Promotions
                                    </a>
                                    <div class="dropdown">
                                            <ul>
                                                <li><a href="<?php echo base_url('discounts') ?>">Discounts</a></li>
                                                <li><a href="<?php echo base_url('coupons') ?>">Discount Coupon</a></li>
                                            </ul>
                                    </div>    
                                </li>
							   <li>
                                    <a class="<?php echo ($this->uri->segment(1) == 'sales_orders' ? 'active' : '') ?>"  href="<?php echo base_url('sales_orders')?>">
                                          <span class="sprite2 sprite-block sprite-sales_order_icon"></span>
                                        Sales Orders
                                    </a>
                                </li>
							   <li>
                                    <a class="<?php echo ($this->uri->segment(1) == 'purchase_orders' ? 'active' : '') ?>"  href="<?php echo base_url('request_orders')?>">
                                          <span class="sprite2 sprite-block sprite-purchase_icon"></span>
                                        Request Stocks
                                    </a>
                                </li>
							   <li>
                                    <a class="<?php echo ($this->uri->segment(1) == 'customers' ? 'active' : '') ?>" href="<?php echo base_url('customers') ?>">
                                          <span class="sprite2 sprite-block sprite-customer_management_icon"></span>
                                        Customer Management
                                    </a>
                                </li>
                                
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
           <!-- <div class="container-fluid page-content"> -->
            
				<?php if($this->session->flashdata('message') || $this->session->flashdata('error')): ?>
					<?php
                        //lets have the flashdata overright "$message" if it exists
                        if($this->session->flashdata('message')){
                            $message	= $this->session->flashdata('message');
                        }
                        
                        if($this->session->flashdata('error')){
                            $error	= $this->session->flashdata('error');
                        }
                        
                        if(function_exists('validation_errors') && validation_errors() != ''){
                            $error	= validation_errors();
                        }
                    ?>        
                    <?php if (!empty($message)): ?>
                        <div class="alert alert-success a-float">
                            <a class="close" data-dismiss="alert">×</a>
                            <?php echo $message; ?>
                        </div>
                    <?php endif; ?>
                
                    <?php if (!empty($error)): ?>
                        <div class="alert alert-danger a-float">
                            <a class="close" data-dismiss="alert">×</a>
                            <?php echo $error; ?>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
				
			<div class="container">

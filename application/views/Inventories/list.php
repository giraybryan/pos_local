
<thead class="center-text" >
	<tr>
		<!--<th>Image </th> -->
		<th><a onclick="event.preventDefault(); $('#item_list').fadeOut();  load_pageloader('item_list'); $('#item_list').load('<?php echo base_url('inventories/paginate/'.$category_id.'/'.$keyword.'/'.$status_id.'/inventories.item/'.$sort_order.'/'.$limit.'/'.$offset) ?>').fadeIn();">Item Name</a> </th>
		<th><a onclick="event.preventDefault(); $('#item_list').fadeOut();  load_pageloader('item_list'); $('#item_list').load('<?php echo base_url('inventories/paginate/'.$category_id.'/'.$keyword.'/'.$status_id.'/inventories.price/'.$sort_order.'/'.$limit.'/'.$offset) ?>').fadeIn();">Price</a></th>
		<th><a onclick="event.preventDefault(); $('#item_list').fadeOut();  load_pageloader('item_list'); $('#item_list').load('<?php echo base_url('inventories/paginate/'.$category_id.'/'.$keyword.'/'.$status_id.'/inventories.stock/'.$sort_order.'/'.$limit.'/'.$offset) ?>').fadeIn();">Stock Qty</a></th>
		<th><a onclick="event.preventDefault(); $('#item_list').fadeOut();  load_pageloader('item_list'); $('#item_list').load('<?php echo base_url('inventories/paginate/'.$category_id.'/'.$keyword.'/'.$status_id.'/category_name/'.$sort_order.'/'.$limit.'/'.$offset) ?>').fadeIn();">Inventory Category</th>
		<th><a onclick="event.preventDefault(); $('#item_list').fadeOut();  load_pageloader('item_list'); $('#item_list').load('<?php echo base_url('inventories/paginate/'.$category_id.'/'.$keyword.'/'.$status_id.'/threshold/'.$sort_order.'/'.$limit.'/'.$offset) ?>').fadeIn();">Threshold</th>
		<th></th>
	</tr>
</thead>
<tbody class="center-text" >
<?php if($items):?>
	<?php foreach($items as $item):?>
	<tr class="<?php echo ($item->threshold >= $item->stock ? 'danger': '')?>">
		<!--
		<td>
			<?php  ?>
			<div class="image-container">
				<img class="" src="<?php echo base_url('uploads/item_image/'.$item->image); ?>"/>
			</div>
		</td>
	-->
		<td><?php echo $item->item?></td>
		<td>P <?php echo $item->price?></td>
		<td><?php echo $item->stock?></td>
		<td><?php echo $item->category_name ?></td>
		<td><?php echo $item->threshold ?></td>
		<td>
			<div class="input-group inputgroup-gray">
				<button class="btn view-item" data-id="<?php echo $item->id ?>" data-encode='<?php echo json_encode($item); ?>' data-toggle="tooltip" data-placement="bottom" title="View Details" ><span class="glyphicon glyphicon-eye-open"></span></button>
				<!--<button class="btn edit-item" data-id="<?php echo $item->id?>" data-encode='<?php echo json_encode($item); ?>' data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button>
				<button class="btn delete-item" data-id="<?php echo $item->id?>" data-toggle="tooltip" data-placement="bottom" title="Delete" ><span  class="glyphicon glyphicon-trash"></span></button> -->

				
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="6"><?php echo $pagination_link; ?></td>
	</tr>
<?php else: ?>

	<tr>
		<td colspan="6">
			<div class="alert alert-warning alert-dismissible fade in">
				<h5> There are no item right now! </h5>
			</div>
		</td>
	</tr>
<?php endif;?>
</tbody>
		
<script type="text/javascript" src="<?php echo base_url('assets/js/append.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		
		$('[data-toggle="tooltip"]').tooltip();
		
		$('.delete-item').click(function(){
			id = $(this).attr('data-id');
			
			console.log(id);
			
			swal({   
				title: "Are you sure?",   
				text: "You will deleting this item.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Delete it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'inventories/delete_item',
							data: {id : id},
							// dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								
								
								swal("Deleted!", "Item has been deleted.", "success");  
								$('.back-list').trigger('click');
								get_list();
									
									
								
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		});
		
	});
</script>
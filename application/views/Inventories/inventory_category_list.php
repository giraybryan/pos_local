<?php if($categories):?>
	<?php foreach($categories as $category):?>
	<tr>
		
		<td><?php echo $category->name?></td>
		<td>
			<div class="input-group inputgroup-gray">
				
				<!-- <button class="btn edit-category" data-id="<?php echo $category->id?>" data-encode='<?php echo json_encode($category); ?>' data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button>
				<button class="btn delete-category" data-id="<?php echo $category->id?>" data-toggle="tooltip" data-placement="bottom" title="Delete" ><span  class="glyphicon glyphicon-trash"></span></button> -->

			</div>
		</td>
	</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="5"><?php echo $category_pagination_link; ?></td>
	</tr>
<?php else: ?>

	<tr>
		<td colspan="5">
			<div class="alert alert-warning alert-dismissible fade in">
				<h5> There are no item right now! </h5>
			</div>
		</td>
	</tr>
<?php endif;?>
		
<script type="text/javascript" src="<?php echo base_url('assets/js/append.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		
		$('[data-toggle="tooltip"]').tooltip();
		
		$('.delete-category').click(function(){
			id = $(this).attr('data-id');
			
			console.log(id);
			
			swal({   
				title: "Are you sure?",   
				text: "You will deleting this item.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Delete it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'inventories/delete_category',
							data: {id : id},
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
								
								if(response.result == 1){
									swal("Deleted!", "Category has been deleted.", "success");  
									$('#view-inventory-category').trigger('click');
									get_category_list();
									
								} else {
									swal("Cancelled", "Error in deleting category", "error");   
								}
								
									
								
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		});
		
	});
</script>
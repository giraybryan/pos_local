<div class="col-sm-3 inventory_quick_search">
	<div class="widget-container">
		<div class="widget-title">
			<h4>Quick Search</h4>
		</div>
		<div class="widget-body">
			<label>Keyword Search</label>
			<input type="text" class="form-control" id="filter_keyword" placeholder="Search Keyword"/>
		</div>
	</div>
	
	<div class="widget-container">
		<div class="widget-title">
			<h4>Advance Search</h4>
		</div>
		<div class="widget-body">
			
			<label>Category</label>
			<select class="select" id="filter_category" placeholder="Select Category">
				<option value=""></option>
				<?php foreach($categories as $category): ?>
				<option value="<?php echo $category->id?>"><?php echo $category->name ?></option>
				<?php endforeach;?>
			</select>
			
			<label>Status</label>
			<select class="select" name="filter_status" id="filter_status" placeholder="Select Status">
				<option value=""></option>
				<?php foreach($status as $stat):?>
					<option value="<?php echo  $stat->id?>"><?php echo $stat->status?></option>
				<?php endforeach;?>
			</select>
			
		</div>
	</div>
	
	
</div>
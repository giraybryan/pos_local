

<div class="row">
	<?php $this->load->view('Inventories/quick_search');?>
	<?php $this->load->view('Inventories/category_quick_search');?>
	<div id="inventory-list-wrapper" class="col-sm-9 right-panel">
	
		<div class="container-fluid widget-content list-container">
			<div class="">
			</div>
			
			<div class="page-title">
				<h4><?php echo $page?></h4>
			</div>
			<!--<button id="add-item" class="btn btn-default add-item">Add Item</button> -->
			<button id="view-inventory-category" class="btn btn-default add-item">Inventory Categories</button>
			<table class="table table-stripes" id="item_list">
				
				<?php $this->load->view('Inventories/list');?>
				
			</table>
		</div>

		<div class="container-fluid widget-content view-container hide">
			<button class="btn btn-default back-to-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button>
			<table class="table table-stripes">
				
				<tr>
					<td>Image</td>
					<td><img src="" id="image_view"/></td>
				</tr>
				<tr>
					<td>Item name</td>
					<td id="item_view"></td>
				</tr>
				<tr>
					<td>Stock Qty</td>
					<td id="stock_view"></td>
				</tr>
				<tr>
					<td>Price</td>
					<td id="price_view"></td>
				</tr>
				<tr>
					<td>Inventor Category</td>
					<td id="category_name_view"></td>
				</tr>
				<tr>
					<td>Threshold</td>
					<td id="threshold_view"></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="button"  class="btn btn-default edit-item" value='EDIT'  data-encode="" data-id="" id="id_view"/></td>
				</tr>
			</table>
		</div>
	</div>


	<?php $this->load->view('Inventories/form');?>


	<div id="inventory-category-wrapper" class="col-sm-8 right-panel hide">

		<div class="container-fluid widget-content list-container">
			<div class="page-title">
				<h4>Inventory Categories</h4>
			</div>
			<div class="">
			</div>
			<button id="" class="btn btn-default back-to-item">Back to Item list</button>
			<!--<button id="add-category" class="btn btn-default add-item">Add Inventory Category</button> -->
			<table class="table table-stripes">
				<thead class="center-text">
					<tr>
						<th>Inventory Category </th>
						<th></th>
					</tr>
				</thead>
				<tbody class="center-text" id="category_list">
				<?php $this->load->view('Inventories/inventory_category_list');?>
				</tbody>
			</table>
		</div>
	</div>
	<?php $this->load->view('Inventories/category_form');?>

</div>



<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/modules/inventories.js');?>"></script>
<div id="inventory-form-wrapper" class="col-sm-8 right-panel hide">
	
	<div class="container-fluid widget-content">
	<button class="btn btn-default back-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button>
	<form id="inventory-form"  class="form-horizontal">
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Item Name:</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="item" name="item" placeholder="Item">
				<input type="hidden" class="form-control" id="id" name="id" placeholder="Item">
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Stock Qty:</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="stock" name="stock" placeholder="Stock">
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Price:</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="price" name="price" placeholder="Price">
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Inventory Category</label>
			<div class="col-sm-10">
				<select class="select" placeholder="Select Category" id="inventory_category_id" name="inventory_category_id">
					<option value=""></option>
					<?php foreach($categories as $category): ?>
					<option value="<?php echo $category->id?>"><?php echo $category->name ?></option>
					<?php endforeach;?>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Status</label>
			<div class="col-sm-10">
				<select class="select" placeholder="Select Status" id="status" name="status">
					<option value=""></option>
					<?php foreach($status as $stat):?>
						<option value="<?php echo  $stat->id?>"><?php echo $stat->status?></option>
					<?php endforeach;?>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Image</label>
			<div class="col-sm-10">
				<input type="file" name="item_image" id="item_image" />
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Threshold</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="threshold" id="threshold" placeholder="Threshold" />
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
		</div>
		
	</form>
	</div>
</div>
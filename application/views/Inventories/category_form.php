<div id="inventory-category-form-wrapper" class="col-sm-8 right-panel hide">
	
	<div class="container-fluid widget-content">
	<button class="btn btn-default back-category-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button>
	<form id="inventory-category-form"  class="form-horizontal">
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Category Name:</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" id="category_name" name="category_name" placeholder="Category name">
				<input type="hidden" class="form-control" id="category_id" name="category_id" placeholder="">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Category image:</label>
			<div class="col-sm-8">
				<input type="file" name="category_image" id="category_image"/>
				<div class="current_file">
					Current file: <img src="" id="current_image" style='width: 50px'/>
				</div>
			</div>
		</div>
	
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
		</div>
		
	</form>
	</div>
</div>
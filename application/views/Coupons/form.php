<div id="coupon-form-wrapper" class="col-sm-8 right-panel hide">
	
	<div class="container-fluid widget-content">
	<button class="btn btn-default back-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button>
	<form id="coupon-form"  class="form-horizontal">
		
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Coupon Name:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="title" name="title" placeholder="Coupon Title">
				<input type="hidden" class="form-control" id="id" name="id" placeholder="">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Start Date:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control datepick" id="start_date" name="start_date" placeholder="Start Date">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">End Date:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control datepick" id="end_date" name="end_date" placeholder="End Date">
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Discount Type:</label>
			<div class="col-sm-9">
				<select name="reduction_type" id="reduction_type" placeholder="Select Discount Type" class="select">
					<option value=""></option>
					
					<?php foreach($discount_types as $discount_type):?>
						<?php if($discount_type->id != 3 ):?>
							<option value="<?php echo $discount_type->id ?>"><?php echo $discount_type->discount_type_name?></option>
						<?php endif; ?>
					<?php endforeach;?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Discount Amount:</label>
			<div class="col-sm-9">
				<input type="text" name="reduction_amount" placeholder="Discount Amount" class="form-control" id="reduction_amount" />
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Required Items:</label>
			<div class="col-sm-9">
				<input type="text" class="" placeholder="Required Item(s)" id="required_item_ids" name="required_item_ids" style='width: 100%' />
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Reqired Quantity:</label>
			<div class="col-sm-9">
				<input type="text" name="required_quantity" placeholder="Quantity item condition"   class="form-control" id="required_quantity" multiple="multiple"/>	
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3"  class="col-sm-3 control-label">Reqired Amount:</label>
			<div class="col-sm-9">
				<input type="text" placeholder="Required Amount Price" name="required_amount" class="form-control" id="required_amount"/>	
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Max Use Coupon:</label>
			<div class="col-sm-9">
				<input type="text" name="max_use" class="form-control" id="max_use" placeholder="Max no. used of promo" />	
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Branch:</label>
			<div class="col-sm-9">
				<select name="branch_id" id="branch_id" placeholder="Select Branch" class="select">
					<option value=""></option>
					<?php foreach($branches as $branch):?>
					<option value='<?php echo $branch->id?>'><?php echo $branch->branch_name?></option>
					<?php endforeach;?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
		</div>
		
	</form>
	</div>
</div>
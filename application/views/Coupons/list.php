<?php if(!empty($coupons)):?>
	<?php foreach($coupons as $coupon):?>

	<?php 
		
			$coupon->required_item_ids = get_field_menu_value($coupon->required_item_ids);
	?>
	<tr>
		
		<td><?php echo $coupon->coupon_code?></td>
		<td><?php echo $coupon->title?></td>
		<td> <?php echo ($coupon->reduction_type == 1 ? 'Percent': 'Amount' )?></td>
		<td><?php
			switch($coupon->reduction_type){
					case 1:
						echo $coupon->reduction_amount.'%';
						break;
					case 2:
						 echo format_currency($coupon->reduction_amount);
						break;
					case 3: 
					echo $coupon->reduction_amount;
						break;

				}
	

		 ?></td>
		<td><?php echo $coupon->branch_name?></td>
		<td><?php echo $coupon->start_date?></td>
		<td><?php echo $coupon->end_date?></td>
		<td>
			
				<button class="btn view-item" data-id="<?php echo $coupon->id ?>" data-encode='<?php echo json_encode($coupon); ?>' data-toggle="tooltip" data-placement="bottom" title="View Details" ><span class="glyphicon glyphicon-eye-open"></span></button>
				<!--<button class="btn edit-item" data-id="<?php echo $coupon->id?>" data-encode='<?php echo json_encode($coupon); ?>' data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button>
				<button class="btn delete-item" data-id="<?php echo $coupon->id?>" data-toggle="tooltip" data-placement="bottom" title="Delete" ><span  class="glyphicon glyphicon-trash"></span></button> -->
			
		</td>
	</tr>
	<?php endforeach; ?>
	<tr>
		<td colspan="6"><?php echo $pagination_link; ?></td>
	</tr>
<?php else: ?>

	<tr>
		<td colspan="6">
			<div class="alert alert-warning alert-dismissible fade in">
				<h5> There are no menu right now! </h5>
			</div>
		</td>
	</tr>
<?php endif;?>
		
		
<script type="text/javascript">
	$(document).ready(function(){
		
		$('[data-toggle="tooltip"]').tooltip();
		
		$('.delete-item').click(function(){
			id = $(this).attr('data-id');
			
			console.log(id);
			
			swal({   
				title: "Are you sure?",   
				text: "You will deleting this coupon.",   
				type: "warning",  
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",  
				confirmButtonText: "Yes, Delete it!",   
				cancelButtonText: "No, cancel it!",   
				closeOnConfirm: false,   
				
				
			}, 
			
			function(isConfirm){   
					if (isConfirm) {
						$.ajax({
							type: 'POST',
							url:  base_url+'coupons/delete',
							data: {id : id},
							dataType: "json",
							success: function ( response ) {
								/* alert(response); */
							
								if(response.result == 1){
									swal("Deleted!", response.msg, "success");  
									$('.back-list').trigger('click');
									get_list();
								} else {
									swal("Cancelled", response.msg, "error");   
								}
									
									
								
							}
						});
						

						
					} else {     
						swal("Cancelled", "Canceled", "error");   
					} 
			});
		});
		
	});
</script>
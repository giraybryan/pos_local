<link href="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.min.css')?>"  rel="stylesheet">
<div class="row">
	<?php $this->load->view($module.'/quick_search');?>
	<div id="coupon-list-wrapper" class="col-sm-9 right-panel">
	
		<div class="container-fluid widget-content list-container">
			<div class="">
			</div>

			<div class="page-title">
				<h4><?php echo $page?></h4>
			</div>
			
			<!--<button id="add-item" class="btn btn-default add-item">Add Coupon</button> -->
			
			<table class="table table-stripes">
				<thead class="center-text">
					<tr>
						<th>Coupon Code </th>
						<th>Title </th>
						<th>Discount Type </th>
						<th>Discount Amount </th>
						<th>Branch</th>
						<th>Date start </th>
						<th>Date end </th>
						
						<th></th>
					</tr>
				</thead>
				<tbody class="center-text" id="list_container">
				<?php $this->load->view($module.'/list');?>
				</tbody>
			</table>
		</div>

		<div class="container-fluid widget-content view-container hide">
			
			<table class="table table-bordered">
				<tr>
					<td><button class="btn btn-default back-to-list"><span class="glyphicon glyphicon-chevron-left"></span>Back</button></td>
					<td><input type="button"  class="btn btn-default edit-item" value='EDIT'  data-encode="" data-id="" id="id_view"/></td>
				</tr>
				<tr>
					<td style=''>Coupon Code</td>
					<td id="coupon_code_view"></td>
				</tr>
				<tr>
					<td style=''>Title</td>
					<td id="title_view"></td>
				</tr>
				<tr>
					<td>Promotion Type</td>
					<td id="reduction_type_view"></td>
				</tr>
				<tr>
					<td>Promotion Amount</td>
					<td id="reduction_amount_view"></td>
				</tr>
				<tr>
					<td>Required Menu(s)</td>
					<td id="required_item_ids_view">

					</td>
				</tr>
				<tr>
					<td>Required Quantity</td>
					<td id="required_quantity_view">
						
					</td>
				</tr>
				<tr>
					<td>Required Price Amount</td>
					<td id="required_amount_view">
						
					</td>
				</tr>
				<tr>
					<td>Start Date</td>
					<td id="start_date_view">
						
					</td>
				</tr>
				<tr>
					<td>End Date</td>
					<td id="end_date_view">
						
					</td>
				</tr>
				<tr>
					<td>Used</td>
					<td id="used_view">
						
					</td>
				</tr>
				<tr>
					<td>Max use</td>
					<td id="max_use_view">
						
					</td>
				</tr>
				<tr>
					<td>Remaining</td>
					<td id="used_view">
						
					</td>
				</tr>

				<tr>
					<td>Branch </td>
					<td id="branch_name_view"></td>
				</tr>
				
				
				
			</table>

		</div>
	</div>


	<?php $this->load->view($module.'/form');?>
	
</div>



<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/modules/coupons.js');?>"></script>
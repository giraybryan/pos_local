<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	var $module;
	var $redirect 	= "user";
	
	function __construct() {
		parent::__construct();
		
		$this->load->model('User_model');
		$this->lang->load('login');
		
		$this->module = get_class($this);
	}
 
	public function index()
	{		
		$data['logged_in'] = $this->auth->is_logged_in(false, false);
		if ($data['logged_in']){
			redirect($this->redirect);
		}
		$this->load->helper('form');
		$data['redirect']	= $this->session->flashdata('redirect');
		$submit 			= $this->input->post('login');
		if ($submit){
			$username		= $this->input->post('username');
			$password		= $this->input->post('password');
			$remember   	= $this->input->post('remember');
			$this->redirect	= $this->input->post('redirect');
			$login			= $this->auth->login_user($username, $password, $remember);			
			if ($login){
				redirect($this->redirect);
			}else{
				//this adds the redirect back to flash data if they provide an incorrect credentials
				$this->session->set_flashdata('redirect', $this->redirect);
				$this->session->set_flashdata('error', lang('error_authentication_failed'));
				redirect(strtolower($this->module));
			}
		}
		$this->load->view($this->module.'/login', $data);
	}
	
	function logout(){
		 
		$user = $this->user_session->userdata('user');
		$this->User_model->update_logs($user['user_id']);

		$this->auth->logout();
		
		$this->session->set_flashdata('message', lang('message_logged_out'));
		redirect(strtolower($this->module));
	}
	
}
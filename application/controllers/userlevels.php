<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userlevels extends Admin_Controller {
	
	public $module = 'userlevels';
	public $page_title = 'Userlevels';
	public $table = 'useraccess_userlevels';
	public $controller = 'userlevels';
	public $single = 'userlevel';
	
	public function __construct()
	{
		error_reporting(E_ALL & ~E_NOTICE);
		
		parent::__construct();
		$this->load->model(array('userlevels_model'));
		$this->module 	= get_class($this);
		$this->redirect = strtolower($this->module);
		$this->current_module = strtolower($this->module);
	}
	
	public function index($keywords = 0,$osrt_by= 'id', $sort_order = 'DESC', $limit = 10, $offset = 0)
	{
	
		
		$data = $arrayName = array('page' => $this->page_title, 'module' => $this->module );
		
		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			
		);
		
		
		$data['userlevels'] = $this->userlevels_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->userlevels_model->fetch_all($params, true);
		
	
		
		
		/*$post	= $this->input->post(null, false);
		if($post)
		{
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
			//reset the term to an object for use
			$term	= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
 		$data['term']		= $term;
		$data['userlevels'] = $this->Userlevels_model->fetch_list(array(
																	'search'		=> $term,
																	'sort_by' 		=> $sort_by,
																	'sort_order' 	=> $sort_order,
																	'limit' 		=> $rows,
																	'offset' 		=> $page,
																));
		$total 				= $this->Userlevels_model->fetch_list(array(
																	'search' 		=> $term,
																	'sort_by' 		=> $sort_by,
																	'sort_order' 	=> $sort_order,
																),true);	

		// echo "<pre>"; print_r($data['userlevels']); die;
		
		$config['base_url']			= site_url(strtolower($this->module).'/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $total;
		$config['per_page']			= $rows;
		$config['uri_segment']		= 6;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<ul class="pagination">';
		$config['full_tag_close']	= '</ul>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
		
		$data['pagination_link']= $this->pagination->create_links();	
		
		$data["security"] = $this->security;
		if(!$data["security"]->CanView)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect("timetracker");
		}*/
		
	
		$data['modules'] = $this->useraccess->getModules();
		$data['module'] = $this->module;
	
		
		$this->view($this->module . '/index', $data);
	}

	public function add(){

		$rules = array(
           	'userlevel' => array(
                     'field' => 'userlevel',
                     'label' => 'Userlevel Title',
                     'rules' => 'trim|required|xss_clean'
                     ),
          /* 	'description' => array(
                     'field' => 'description',
                     'label' => 'description',
                     'rules' => 'trim|required|xss_clean'
                     ),*/
           );
		
		$this->form_validation->set_rules($rules);


		 if ( $this->form_validation->run() ) {
		 		$form_data = $this->input->post();

		 		
		 	
	 			$module_data = array(
					'table' 	=> $this->table,
					'data'		=> $this->input->post(),
					'module'	=> $this->current_module,
				);

				
	 			$id = $this->userlevels_model->dynamic_save($module_data);

	 			if(!empty($id)){

				
					$return['result'] = 1;
					$return['msg'] = $this->single.' Saved!';

				} else {

					$result['result'] = 0;
					$result['msg'] = 'Error in saving '. $this->single.' !';

				}

		 } else {
		 	$return['msg'] = validation_errors();
		 	$return['result'] = 0;
		 }
		
		echo json_encode($return);
	}


	public function delete(){

		if( $this->input->post() ) {
			$delete_param = array(
				'table' 	=> $this->table,
				'module'	=> $this->current_module,
				'where' 	=> $this->input->post(),
			);
			
			$result = $this->userlevels_model->dynamic_delete($delete_param);

			if(!empty($result)){
				$return['msg'] =  $this->single.' Successfully deleted';
				$return['result'] = 1;
			} else {
				$return['msg'] = 'Error in deleting '. $this->single;
				$return['result'] = 0;
			}
			
		} else {
			$return['msg'] = 'Error in deleting '. $this->single;
			$return['result'] = 0;
		}

		echo json_encode($return);
	}

	public function paginate($keywords = 0, $sort_order = 'DESC', $limit = 10, $offset = 0){

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);
		
		
		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}
		
		$data = array(); 

		$data['userlevels'] = $this->userlevels_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->userlevels_model->fetch_all($params, true);
		
	
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$keywords . '/'.$sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 6;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');
		
		

		echo $this->load->view($this->module . '/list', $data, true, true);
	}
	
	/*public function add()
	{
		
		$data["page_title"] = "Add Userlevel";
		$data["module"] 	= strtolower($this->module);
		
		$data["security"] = $this->security;
		if(!$data["security"]->CanAdd)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect($this->redirect);
		}
		
		if( $post = $this->input->post() )
		{			
			$check_title = $this->Userlevels_model->check_title($post);
			if(empty($check_title)){
				$result = $this->Userlevels_model->add_level($post);
				if(!empty($result)){
					$this->session->set_flashdata('success_result', 'Successfully added');
					redirect($this->redirect);
				}
			} else {
				$this->session->set_flashdata('error_result', 'Title already taken');
				redirect($this->redirect);
			}
		}
	
		$this->view($this->module . '/form', $data); 
	}*/
	
	
	 
	/*public function delete($id = false)
	{
		$data["security"] = $this->security;
		if(!$data["security"]->CanDelete)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect($this->redirect);
		}
		
		if(!$id) {
			$this->session->set_flashdata('error', 'Unknown userlevel');
			redirect($this->redirect);
		}
		
		if($this->input->is_ajax_request()) {
			$return = $this->Userlevels_model->delete($id);
			echo json_encode(array('result'=> $return));
		} else {
			$return = $this->Userlevels_model->delete($id);
			$this->session->set_flashdata('error', 'Successfully Deleted!');
			redirect($this->redirect);
		}		
	}*/
	
	function permissions($userlevel_id = false, $userlevel_name = false)
	{	
	
		$data['CanView'] = $this->useraccess->ValidateAccess($this->module, 'View');
		$data['CanEdit'] = $this->useraccess->ValidateAccess($this->module, 'Edit');

		if(!$data['CanView']) {
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect($this->redirect);
		} 
		
		$data['modules'] = $this->useraccess->getModules();
		$data['permission_types'] = $this->useraccess->getPermissionTypes();
	
		if(!$data["modules"])
		{
			$this->session->set_flashdata('error', 'No Modules generated');
			redirect($this->redirect);
		}
	 
		if($post = $this->input->post())
		{
			
			
		
			// Get fields from form
			
			foreach($post['module'] as $module => $module_permissions){
				// echo $module;/* /*  print_r($module_permissions); */
				
				$this->db->where(array('userlevel_id' => $post['userlevel_id'],'module_type' =>  $module))->delete('userlevel_permissions');
				
				foreach($module_permissions as $key_permission => $permission){
					$permission_data = array(
						'userlevel_id' => $post["userlevel_id"],
						'module_type' => $module,
						'permission' => $key_permission,
					);
					
					$this->db->insert('userlevel_permissions', $permission_data);
				} 
			}
			
			$this->session->set_flashdata('message', 'Userlevel Permissions Successfully Setup!');
			redirect($this->redirect."/permissions/".$post["userlevel_id"]);
		}
		
		if(!$userlevel_id)
		{
			$this->session->set_flashdata('error', 'No User Level ID Provided!');
			redirect($this->redirect);
		}
		
		
		$userlevel_info = $this->userlevels_model->get_row_by_id($userlevel_id);

		$name = ($userlevel_info->userlevel) ? $userlevel_info->userlevel : "";
		$data["page"] 	= "User Levels Permissions: ".$name;
		$data['modules'] = $this->useraccess->getModules();
		$data['permissions_types'] = $this->useraccess->getPermissionTypes();

		$data['userlevel'] = $userlevel_info;
		//$userlevels = $this->useraccess->getUserlevels();

		$this->view($this->module . '/permissions_form', $data); 
	}
}
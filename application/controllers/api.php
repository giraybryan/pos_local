<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends Front_Controller {

	var $module;
	var $redirect 	= "user";
	
	function __construct() {
		parent::__construct();
		
		$this->load->model(array('User_model','inventories_model','Inventory_categories','Menus_model','Categories_model','Employees_model','Discounts_model','Promotions_model','Coupons_model','Request_orders_model','Coupons_model','Company_model'));
		$this->module = get_class($this);
	}


	function login(){

		
		$rules = array(
           	'Username' => array(
                     'field' => 'Username',
                     'label' => 'Username',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'Password' => array(
                     'field' => 'Password',
                     'label' => 'Password',
                     'rules' => 'trim|required|xss_clean'
                     ),
           
          
           );
		// $_POST['Username'] = 'orden@pos.com';
		// $_POST['Password'] = 'admin';
		// $_POST['DeviceID'] = '2323';
		// $_POST['update_logs'] = '1,2,4';
		// $_POST['IsSetup'] = '1'; //REMOVE_V1 value (1,0) 1 meaning it was already sync with device
		// $_POST['isSynced'] = '1'; //REMOVE_V1 value (1,0) 1 meaning it was already sync with device
		// $_POST['isSynced'] = '1'; //REMOVE_V1 value (1,0) 1 meaning it was already sync with device

		// $_POST['IsToRefresh'] = 1;
		// $_POST['Password'] = 'test2';

		// if ( $_POST ) {

		// print_R($_POST);
		if($_POST['IsToRefresh'] == 1){

			$this->reset_table('branch_devices', array('DeviceID' => $_POST['DeviceID']));
		}


		$this->form_validation->set_rules($rules);
		if ( $this->form_validation->run() ) {
		

		 	$params = array(
		 			'table' => 'employees',
		 			'where' => array('email' => $this->input->post('Username'), 'password' => $this->input->post('Password')),
		 		);

		 	$mobile_DeviceID = $_POST['DeviceID'];
		 	// $mobile_DeviceID = $this->input->post('DeviceID');

			
		 	$user = $this->Employees_model->dynamic_fetch($params);
		 	//$return['query'] = $this->db->last_query();

		 	
		 	
			//if user exist and device id of user and device match
		 	//if( !empty($user) && $mobile_DeviceBranch == $user->branch_id ){

		
		 	if( !empty($user) ){

			 	$device_info = $this->db->where('DeviceID', $mobile_DeviceID)->get('branch_devices')->row();
			 	
			 	//if no record yet for current device
			 	if(empty($device_info)){

			 		$first_login = 1;
			 		$devicedata = array(
			 				'table' => 'branch_devices',
			 				'data' => array('branch_id' => $user->branch_id, 'DeviceID' => $mobile_DeviceID, 'date_updated' => date('Y-m-d')),
			 			);

			 		$device_data_id = $this->Employees_model->dynamic_save($devicedata);

			 		if(empty($device_data_id)){

			 			$return['result'] = 0;
						$return['msg'] = 'Error saving the branch!';
						exit;

			 		} 

			 		$return['result'] = 1;
					$return['msg'] = 'device brach saved!';

					//set user branch id as device branch id
					$mobile_DeviceBranch = $user->branch_id;
					$device_synced = 0;

			 	}  else {

			 		//set branch id of device from the device branch table
			 		$mobile_DeviceBranch = $device_info->branch_id;
			 		$device_synced = $device_info->device_sync;

			 	}

				//var_dump($this->Coupons_model); die;
				$return['result'] = 1;
				$return['msg'] = 'Successfully Logged in!';

				//$return['update_logs'] = $this->db->get('update_logs')->result();
				//$return['branch'] = $this->db->get('branch_inventory_log')->result();

				if(empty($user->branch_id))
					$user->branch_id = 1; //REMOVE_V1 set static branch_id per user (this is only for version1)

				if($device_synced == 0  && !empty($first_login) ) {

						$get_item_branch_params = array(
		 						'where' => array(
		 								'branch_id' => $user->branch_id,
		 								'device_id' => $mobile_DeviceID,
		 								), 
		 						'table' => 'branch_inventory_log',
		 						'order_by' => array('branch_inventory_log.id' => 'DESC'),
		 					);

		 			$branch_log = $this->inventories_model->get_item_branch_params($get_item_branch_params);
		 			
			 		if( !empty($branch_log) && empty($_POST['IsToRefresh']) ){

			 			$inventory_fields = 'inventories.*,inventories.item as inventory_name, inventory_status.status as status_label, inventory_categories.name as category_name, CONCAT("'.base_url().'","uploads/item_image/",inventories.image) as image_url, branch_inventory_items.brach_item_qty,  IF(branch_inventory_items.brach_item_qty  is not null AND branch_inventory_items.branch_log_id = '.$branch_log->id.', branch_inventory_items.brach_item_qty , 0) AS brach_item_qty';			 		
			 			$params = array(
									'fields' => $inventory_fields,
									'order_by' 	=> array('inventories.id' => 'ASC'),
									'where' => array('branch_inventory_items.branch_log_id' => $branch_log->id )
									);
			 		} else {
			 			$inventory_fields = 'inventories.*,inventories.item as inventory_name, inventory_status.status as status_label, inventory_categories.name as category_name, CONCAT("'.base_url().'","uploads/item_image/",inventories.image) as image_url, branch_inventory_items.brach_item_qty,  IF(branch_inventory_items.brach_item_qty  is not null AND branch_inventory_items.branch_log_id = 0, branch_inventory_items.brach_item_qty , 0) AS brach_item_qty';
			 			
			 			$params = array(
									'fields' => $inventory_fields,
									'order_by' 	=> array('inventories.id' => 'ASC'),
									);
			 			
			 		}	

			 		$return['inventory_items'] = $this->inventories_model->fetch_branch_item($params);
			 		// echo "<pre>";
			 		// echo $this->db->last_query();
		 			//  print_r($return); die;

				} elseif($device_synced == 1 &&  $this->input->post('IsToRefresh') == 1){
					$inventory_fields = 'inventories.*,inventories.item as inventory_name, inventory_status.status as status_label, inventory_categories.name as category_name, CONCAT("'.base_url().'","uploads/item_image/",inventories.image) as image_url, branch_inventory_items.brach_item_qty,  IF(branch_inventory_items.brach_item_qty  is not null AND branch_inventory_items.branch_log_id = 0, branch_inventory_items.brach_item_qty , 0) AS brach_item_qty';
			 			
			 			$params = array(
								'fields' => $inventory_fields,
								'order_by' 	=> array('inventories.id' => 'ASC'),
								);
			 			

				} else {

					// $inventory_fields = 'inventories.*,inventories.item as inventory_name, inventory_status.status as status_label, inventory_categories.name as category_name, CONCAT("'.base_url().'","uploads/item_image/",inventories.image) as image_url, branch_inventory_items.brach_item_qty,  IF(branch_inventory_items.brach_item_qty  is not null AND branch_inventory_items.branch_log_id = 0, branch_inventory_items.brach_item_qty , 0) AS brach_item_qty';
			 			
		 		// 	$params = array(
					// 			'fields' => $inventory_fields,
					// 			'order_by' 	=> array('inventories.id' => 'ASC'),
					// 			);
					// $return['inventory_items'] = $this->inventories_model->fetch_branch_item($params);
					$return['inventory_items'] = '';
				}

				//GET REQUEST ORDER DELIVERED THAT NOT YET SYNC WITH MOBILE
				$request_orders = $this->db->where(array('DeviceID' => $mobile_DeviceID, 'device_synced' => 0, 'Delivered' => 1 ))->get('request_orders')->result();
				if(!empty($request_orders)){

					$return['request_orders'] = $request_orders;
					$this->db->where( array('DeviceID' => $mobile_DeviceID) ) ->update('request_orders', array('device_synced' => 1));
					
					//GET ITEMS PER REQUEST ORDER
					foreach($request_orders as $ro_key =>  $ro){
						$return['request_orders'][$ro_key]->items = $this->db->where('ro_id', $ro->id)->get('request_order_list')->result();

					}

				} else {

					$return['request_orders'] = '';
				}

		 		

		 		// echo $this->db->last_query();
		 		// echo "<pre>"; print_r($return['inventory_items']); die;

				
				//$return['user'] = $user;
				$return['company'] = $this->Company_model->fetch_info();
		 		$return['inventory_category'] = $this->Inventory_categories->fetch();
		 		
		 		$return['menus'] = $this->Menus_model->fetch_all($params = array('fields' => 'menus.*, menus.title as item_name, menu_status.status_name, CONCAT("'.base_url().'","uploads/menu/",menus.image) as image_url'));
		 		
		 		$return['menu_categories'] = $this->Categories_model->fetch_all($params = array('fields' => 'menu_categories.*, CONCAT("'.base_url().'","uploads/menu_categories/",menu_categories.image) as image_url'));
		 		
		 		$return['menu_category_options'] = $this->Categories_model->fetch_optons();
		 		
		 		$return['users'] = $this->Employees_model->fetch_all();
		 		
		 		
		 		$return['promotions'] = $this->Promotions_model->fetch_all( $params = array(
		 																		'fields' => 'promotions.*, settings_branch.branch_name, promotion_types.promotion_type_name, promotions.reduction_type as promotion_type'
		 																	));
		 		$return['discounts'] = $this->Discounts_model->fetch_all($params = array(
		 																		'fields' => 'discounts.id, discounts.title, discount_type.discount_type_name, discounts.reduction_type as discount_type, discounts.reduction_amount as discount_value',
		 																		));
		 		$return['coupons'] = $this->Coupons_model->fetch_all();
		 		$menu_ingredients = $this->db->from('menu_ingredients')->order_by('ingredient_id', 'ASC')->get()->result();
		 		$menu_addons = $this->db->get('menu_addons')->result();
		 		$menu_excludes= $this->db->get('menu_excludes')->result();

		 		
		 		$_menu_ingredients_qty = array();
		 		$_menu_ingredients = array();

		 		//PARSE THE MENU INGREDIENTS 
		 		foreach($menu_ingredients as $ingredient){

		 			//if key of menu_ingredient by menu_id is not yet existing
		 			if(empty($_menu_ingredients[$ingredient->menu_id])){

		 				//store ingredient_id in an array per menu_id
		 				$_menu_ingredients[$ingredient->menu_id][] = $ingredient->ingredient_id;

		 				//store ingredient_id quantity in an array per menu_id
		 				$_menu_ingredients_qty[$ingredient->menu_id][$ingredient->ingredient_id] = ( empty($ingredient->qty) ? 1 : $ingredient->qty  );

		 			} else {
		 				
		 				
		 				// //if existing ingredient_id
		 				// if (in_array($ingredient->ingredient_id, $_menu_ingredients[$ingredient->menu_id])) {
		 					
		 				// 	$_menu_ingredients_qty[$ingredient->menu_id][$ingredient->ingredient_id] = $_menu_ingredients_qty[$ingredient->menu_id][$ingredient->ingredient_id] + 1;

		 				// } else {

		 				// 	//adding menu_igredient_id to existing list of ingredient per menu_id
		 				// 	$_menu_ingredients[$ingredient->menu_id][] = $ingredient->ingredient_id;
		 				// 	$_menu_ingredients_qty[$ingredient->menu_id][$ingredient->ingredient_id] = 1;
		 				// }

		 			}

		 		}



		 		//PARSE ADDONS PER MENU ID
		 		$_menu_addons = array();
		 		foreach ($menu_addons as $key => $addon) {

		 			if(!empty($_menu_addons[$addon->menu_id])){

		 				if(!in_array($addon->addon_id, $_menu_addons[$addon->menu_id]))
		 					$_menu_addons[$addon->menu_id][] = $addon->addon_id;

		 			} else {
		 				$_menu_addons[$addon->menu_id][] = $addon->addon_id;
		 			}
		 			
		 		 } 	

		 		 //PARSE EXCLUDE PER MENU ID
		 		 $_menu_excludes = array();
		 		 foreach ($menu_excludes as $key => $exclude) {
		 		 	if(!empty($_menu_excludes[$exclude->menu_id])) {

			 		 	if(!in_array($exclude->item_id, $_menu_excludes[$exclude->menu_id]))
			 		 		$_menu_excludes[$exclude->menu_id][] = $exclude->item_id;

		 		 	} else {

		 		 		$_menu_excludes[$exclude->menu_id][] = $exclude->item_id;
		 		 	}
		 		 	
		 		 }
		 		 // echo "<pre>"; print_R($_menu_excludes); die;
 

		 		//converting array to string 
		 		foreach($_menu_ingredients as $key => $id_list){
		 			$_menu_ingredients[$key] = '('.implode(',',$_menu_ingredients[$key]).')';
		 		}
		 		foreach($_menu_ingredients_qty as $key => $id_list){
		 			$_menu_ingredients_qty[$key] = '('.implode(',',$_menu_ingredients_qty[$key]).')';
		 		}
		 		foreach($_menu_excludes as $key => $id_list){
		 			$_menu_excludes[$key] = '('.implode(',',$_menu_excludes[$key]).')';
		 		}
		 		foreach($_menu_addons as $key => $id_list){
		 			$_menu_addons[$key] = '('.implode(',',$_menu_addons[$key]).')';
		 		}




		 		foreach($return['menus'] as $key => $menu_info){

		 				$return['menus'][$key]->ingredients = '';
		 				$return['menus'][$key]->ingredients_quantity = '';
		 				$return['menus'][$key]->addons = '';
		 				$return['menus'][$key]->excludes = '';

		 				if(!empty($_menu_ingredients[$menu_info->id])){
		 					$return['menus'][$key]->ingredients = $_menu_ingredients[$menu_info->id]; 
		 					$return['menus'][$key]->ingredients_quantity = $_menu_ingredients_qty[$menu_info->id]; 
		 				} 

		 				if(!empty($_menu_excludes[$menu_info->id])){
		 					$return['menus'][$key]->excludes = $_menu_excludes[$menu_info->id];
		 				}
		 				if(!empty($_menu_addons[$menu_info->id])){

		 					$return['menus'][$key]->addons = $_menu_addons[$menu_info->id];
		 				}



	 			}

		 		// echo "<pre>"; print_r($menu_excludes); 
		 		// echo "<pre>"; print_r($return['menus']); die;
		 		// echo "<pre>"; print_r($_menu_ingredients); 
		 		// echo "<pre>"; print_r($_menu_ingredients_qty);  die;
		 		// echo "<pre>";
		 		// print_r($menu_ingredients);
		 		// print_r($menu_addons);
		 		// print_r($menu_excludes);

	 			//UPDATE DEVICE BRANCH STATUS SO SYNCED
	 			$branch_data = array(
	 					'data'  => array('device_sync' => 1),
	 					'where' => array('DeviceID' => $mobile_DeviceID),
	 				);

	 			$this->Request_orders_model->update_mobile_status($branch_data);


			} else {

				$return['result'] = 0;
				$return['msg'] = 'Error in login!';



			}

	 			
		 } else {
		 	$return['msg'] = validation_errors();
		 	$return['result'] = 0;
		 	
		 }

		// echo "<pre>"; print_r($return);
		echo json_encode($return);
	}


	//FOR SALES
	public function fetch_mobile(){

	//$_POST['Data'] = '{"Discounts":{"Coupon":{},"Percent":{},"Promo":{}},"Sales":{"TotalVat":10.714285714285714,"TotalDiscount":0,"TotalPromo":0,"TotalAmount":100,"TotalAmountDue":100,"CashierID":2,"TotalSeniorDiscount":0},"UserLogs":{"Log":{"Login":1429834842728,"DeviceSerialID":"73f3275838854d0c","IsChangeFundDone":1,"DateCreated":"2015-04-24 00:20:42","UserID":2,"Logout":1429835112894,"LogID":1,"ChangeFund":100}},"Transaction":{"ShiftStarted":1429834842728,"Shift":[{"DiscountPercent":0,"DiscountID":0,"DiscountTotal":0,"OrderList":"{\"Item\":{\"ItemID\":4,\"DefaultIngredientListQty\":\"(1)\",\"DefaultIngredientList\":\"(17)\",\"SizeOptionID\":0,\"IsFreeItem\":0,\"ItemMenuID\":0,\"RemoveInformation\":\"()\",\"df\":\"java.text.DecimalFormat@0\",\"ItemName\":\"Chicken Burger\",\"AdditionalInformation\":\"\",\"SizeOptionTypeID\":0,\"ItemCategoryID\":2,\"AddIngredientListID\":\"\",\"ItemSubTotal\":100,\"Description\":\"Burger with Chicken\",\"DeviceSerialID\":\"\",\"DateCreated\":\"\",\"DefaultIncludeList\":\"\",\"IconPath\":\"http:\\\/\\\/forward-dev.com\\\/pos\\\/uploads\\\/menu\\\/2cfa4b1a278323b1fdc2cefe1a6f64a3.jpg\",\"ExtraOptions\":\"\",\"AdditionalTotalAmount\":0,\"ItemStockQTY\":\"\",\"ItemQTY\":1,\"RemoveIngredientListID\":\"\",\"ItemTypeID\":0,\"ItemPrice\":100,\"ExtraAmount\":0,\"DefaultExcludeList\":\"\",\"ItemNoIngredient\":0}}","CouponDiscountType":0,"CashierName":"Ralph Gabrielle Orden","PromoAmount":0,"PromoID":0,"PromoTotal":0,"TransactionType":"DineIn","DiscountAmount":0,"ReceiptDateIssued":"April 24, 2015","TransactionID":1,"PromoPercent":0,"CouponDiscountAmount":0,"CouponID":0,"PromoType":0,"Cash":600,"CreaditCardData":"","DeviceSerialID":"73f3275838854d0c","FreeItemModel":"","TotalAmountDue":100,"Change":500,"ReceiptModel":"  1  Chicken Burger\n    @100.00               100.00\n","DateCreated":"2015-04-24 00:21:02","CouponDiscountPercent":0,"IsVoid":0,"DiscountPosition":0,"ReceiptData":"","ORNumber":"001","Status":1,"MiscIncome":0,"IsSeniorDiscount":0,"Vat":10.714285714285714,"CashierID":2,"CustomerID":0,"TotalAmount":100,"ShiftNumber":1,"CouponCode":"","TransactionNO":"0424201500212","DiscountType":0}],"ShiftNumber":1,"ShiftEnded":1429835114457}}';
	//$_POST['Data'] = ' {"Discounts":{"Coupon":{},"Percent":{},"Promo":{"Promo0":{"ObjectID":"3","ObjectUse":1}}},"Sales":{"TotalVat":10.607142857142856,"TotalDiscount":0,"TotalPromo":0,"TotalAmount":99,"TotalAmountDue":99,"CashierID":2,"TotalSeniorDiscount":0},"UserLogs":{"Log0":{"Login":1429578882111,"DeviceSerialID":"b2d3bd4f0171db1d","IsChangeFundDone":0,"DateCreated":"2015-04-21 09:14:42","UserID":2,"Logout":1429579025386,"LogID":1,"ChangeFund":0}},"Transaction":{"ShiftStarted":1429578882111,"Shift":[{"DiscountPercent":0,"DiscountID":0,"DiscountTotal":0,"OrderList":"{\"Item\":[{\"ItemID\":1,\"DefaultIngredientListQty\":\"(1)\",\"DefaultIngredientList\":\"(16)\",\"SizeOptionID\":0,\"IsFreeItem\":0,\"ItemMenuID\":0,\"df\":\"java.text.DecimalFormat@0\",\"ItemName\":\"Classic Burger\",\"SizeOptionTypeID\":0,\"ItemCategoryID\":2,\"AddIngredientListID\":\"\",\"ItemSubTotal\":99,\"Description\":\"Burger in a classic way\",\"DeviceSerialID\":\"\",\"DateCreated\":\"\",\"DefaultIncludeList\":\"(22,17)\",\"IconPath\":\"http:\\\/\\\/forward-dev.com\\\/pos\\\/uploads\\\/menu\\\/2c5a4fea198aacb87cdd87158404debf.jpg\",\"AdditionalsInformation\":\"{}\",\"ExtraOptions\":\"\",\"AdditionalTotalAmount\":0,\"ItemStockQTY\":\"\",\"ItemQTY\":1,\"RemoveIngredientListID\":\"\",\"ItemTypeID\":0,\"ItemPrice\":99,\"ExtraAmount\":0,\"DefaultExcludeList\":\"(16)\",\"ItemNoIngredient\":0},{\"ItemID\":6,\"DefaultIngredientListQty\":\"(1)\",\"DefaultIngredientList\":\"(17)\",\"SizeOptionID\":0,\"IsFreeItem\":1,\"ItemMenuID\":0,\"df\":\"java.text.DecimalFormat@0\",\"ItemName\":\"Cheese Bread\",\"SizeOptionTypeID\":0,\"ItemCategoryID\":5,\"AddIngredientListID\":\"\",\"ItemSubTotal\":25,\"Description\":\"cheese bread\",\"DeviceSerialID\":\"b2d3bd4f0171db1d\",\"DateCreated\":\"2015-04-21 09:14:34\",\"DefaultIncludeList\":\"(19)\",\"IconPath\":\"http:\\\/\\\/forward-dev.com\\\/pos\\\/uploads\\\/menu\\\/a4be9caaaa3afff1f631283006e470f4.jpg\",\"AdditionalsInformation\":\"\",\"ExtraOptions\":\"\",\"AdditionalTotalAmount\":0,\"ItemStockQTY\":\"\",\"ItemQTY\":1,\"RemoveIngredientListID\":\"\",\"ItemTypeID\":0,\"ItemPrice\":25,\"ExtraAmount\":0,\"DefaultExcludeList\":\"\",\"ItemNoIngredient\":0}]}","CouponDiscountType":0,"CashierName":"ralph orden","PromoAmount":0,"PromoID":3,"PromoTotal":0,"TransactionType":"DineIn","DiscountAmount":0,"ReceiptDateIssued":"April 21, 2015","TransactionID":1,"PromoPercent":0,"CouponDiscountAmount":0,"CouponID":0,"PromoType":1,"Cash":100,"CreaditCardData":"","DeviceSerialID":"b2d3bd4f0171db1d","FreeItemModel":"FREE \n  1  Cheese Bread\n","TotalAmountDue":99,"Change":1,"ReceiptModel":"  1  Classic Burger\n     @99.00                99.00\n","DateCreated":"2015-04-21 09:15:17","CouponDiscountPercent":0,"IsVoid":0,"DiscountPosition":0,"ReceiptData":"","ORNumber":"001","MiscIncome":0,"IsSeniorDiscount":0,"Vat":10.607142857142856,"CashierID":2,"CustomerID":0,"TotalAmount":99,"ShiftNumber":1,"CouponCode":"","TransactionNO":"04212015091516","DiscountType":0}],"ShiftNumber":1,"ShiftEnded":1429579025386}}';
		//$_POST['Data'] = '{"Sales":{"TotalVat":0,"TotalDiscount":0,"TotalPromo":0,"TotalAmount":0,"TotalAmountDue":0,"CashierID":2,"TotalSeniorDiscount":0},"UserLogs":{"Log0":{"Login":1428996342318,"DeviceSerialID":"b2d3bd4f0171db1d","IsChangeFundDone":0,"DateCreated":"2015-04-14 15:25:42","UserID":2,"Logout":1428996386246,"LogID":1,"ChangeFund":0}},"Transaction":{"ShiftStarted":1428996342318,"Shift":[],"ShiftNumber":1,"ShiftEnded":1428996386246}}';
		// $mobile_decode = json_decode($mobile_encode);
	//$_POST['Data'] = '{"Discounts":{"Coupon":{},"Percent":{},"Promo":{"Promo0":{"ObjectID":"3","ObjectUse":1}}},"Sales":{"TotalVat":74.89285714285712,"TotalDiscount":0,"TotalPromo":0,"TotalAmount":699,"TotalAmountDue":699,"CashierID":2,"TotalSeniorDiscount":0},"UserLogs":{"Log0":{"Login":1429581917087,"DeviceSerialID":"b2d3bd4f0171db1d","IsChangeFundDone":0,"DateCreated":"2015-04-21 10:05:17","UserID":2,"Logout":1429582007989,"LogID":1,"ChangeFund":0}},"Transaction":{"ShiftStarted":1429581917087,"Shift":[{"DiscountPercent":0,"DiscountID":0,"DiscountTotal":0,"OrderList":"{\"Item\":[{\"ItemID\":1,\"DefaultIngredientListQty\":\"(1)\",\"DefaultIngredientList\":\"(16)\",\"SizeOptionID\":1,\"IsFreeItem\":0,\"ItemMenuID\":0,\"RemoveInformation\":\"\",\"df\":\"java.text.DecimalFormat@0\",\"ItemName\":\"Classic Burger\",\"SizeOptionTypeID\":0,\"ItemCategoryID\":2,\"AddIngredientListID\":\"17\",\"ItemSubTotal\":99,\"Description\":\"Burger in a classic way\",\"DeviceSerialID\":\"\",\"DateCreated\":\"\",\"DefaultIncludeList\":\"(22,17)\",\"IconPath\":\"http:\/\/forward-dev.com\/pos\/uploads\/menu\/2c5a4fea198aacb87cdd87158404debf.jpg\",\"AdditionalsInformation\":\"{\"Include\":{\"Additionals 0\":{\"id\":17,\"qty\":4}}}\",\"ExtraOptions\":\"Small\",\"AdditionalTotalAmount\":600,\"ItemStockQTY\":\"\",\"ItemQTY\":1,\"RemoveIngredientListID\":\"\",\"ItemTypeID\":0,\"ItemPrice\":99,\"ExtraAmount\":0,\"DefaultExcludeList\":\"(16)\",\"ItemNoIngredient\":0},{\"ItemID\":6,\"DefaultIngredientListQty\":\"(1)\",\"DefaultIngredientList\":\"(17)\",\"SizeOptionID\":0,\"IsFreeItem\":1,\"ItemMenuID\":0,\"RemoveInformation\":\"\",\"df\":\"java.text.DecimalFormat@0\",\"ItemName\":\"Cheese Bread\",\"SizeOptionTypeID\":0,\"ItemCategoryID\":5,\"AddIngredientListID\":\"\",\"ItemSubTotal\":25,\"Description\":\"cheese bread\",\"DeviceSerialID\":\"b2d3bd4f0171db1d\",\"DateCreated\":\"2015-04-21 10:05:15\",\"DefaultIncludeList\":\"(19)\",\"IconPath\":\"http:\/\/forward-dev.com\/pos\/uploads\/menu\/a4be9caaaa3afff1f631283006e470f4.jpg\",\"AdditionalsInformation\":\"\",\"ExtraOptions\":\"\",\"AdditionalTotalAmount\":0,\"ItemStockQTY\":\"\",\"ItemQTY\":1,\"RemoveIngredientListID\":\"\",\"ItemTypeID\":0,\"ItemPrice\":25,\"ExtraAmount\":0,\"DefaultExcludeList\":\"\",\"ItemNoIngredient\":0}]}","CouponDiscountType":0,"CashierName":"ralph orden","PromoAmount":0,"PromoID":3,"PromoTotal":0,"TransactionType":"DineIn","DiscountAmount":0,"ReceiptDateIssued":"April 21, 2015","TransactionID":1,"PromoPercent":0,"CouponDiscountAmount":0,"CouponID":0,"PromoType":1,"Cash":700,"CreaditCardData":"","DeviceSerialID":"b2d3bd4f0171db1d","FreeItemModel":"FREE \n  1  Cheese Bread\n","TotalAmountDue":699,"Change":1,"ReceiptModel":"  1  Classic Burger\n     @99.00                99.00\n      +4 chicken          150.00\n\n","DateCreated":"2015-04-21 10:06:03","CouponDiscountPercent":0,"IsVoid":0,"DiscountPosition":0,"ReceiptData":"","ORNumber":"001","MiscIncome":0,"IsSeniorDiscount":0,"Vat":74.89285714285712,"CashierID":2,"CustomerID":0,"TotalAmount":699,"ShiftNumber":1,"CouponCode":"","TransactionNO":"0421201510063","DiscountType":0}],"ShiftNumber":1,"ShiftEnded":1429582007989}}';
	
		
		if(!empty($_POST['Data'])){
			// echo $_POST['Data'];  die;
			$mobile_decode = json_decode($_POST['Data']);
			
			// echo "<pre>"; print_R($mobile_decode); 
			// print_r( json_decode($mobile_decode->Transaction->Shift[0]->OrderList));
			// die;
			
			$log = $mobile_decode->UserLogs->Log;
			if(!empty($log)){
				$existing_data = $this->db->where(array('DateCreated' => $log->DateCreated, 'DeviceSerialID' => $log->DeviceSerialID))->get('employee_logs')->row();
				
				//log data
				$log_data = array(
						'Login'  		 => date('Y-m-d H:i:s', $log->Login / 1000),
						'Logout'         => date('Y-m-d H:i:s', $log->Logout / 1000),
						'DeviceSerialID' => $log->DeviceSerialID,
						'employee_id'    => $log->UserID,
						'ChangeFund'     => $log->ChangeFund,
						'DateCreated'    => $log->DateCreated,
					);

				if(!empty($existing_data)){ //if the entry already existing
					$log_data['id'] = $existing_data->id;

				}

				$formdata = array(
						'table' => 'employee_logs',
						'data'  => $log_data,
					);

				
				$id = $this->Employees_model->dynamic_save($formdata);
				$log_id = $id;
				if(!empty($id)){

					$return['result'] = 1;
					$return['msg'] = 'employee log saved';
				} else {

					$return['result'] = 0;
					$return['msg'] = 'failed to save employee log for employee ID '.$log->UserID;
					break;
				}
			}
			

			if( !empty($mobile_decode->Discounts->Coupon) ){ 

				foreach($mobile_decode->Discounts->Coupon as $coupon){

					$this->db->where('id', $coupon->ObjectID)->update('discount_coupons', array('used' => $coupon->ObjectUse));

				}
			}
			if( !empty($mobile_decode->Discounts->Promo) ){ 

				foreach($mobile_decode->Discounts->Promo as $promo){

					$this->db->where('id', $promo->ObjectID)->update('promotions', array('used' => $promo->ObjectUse));

				}
			}

			
			if(!empty($mobile_decode->Sales)){

				$sales_data = array(
					'TotalVat' 			  => $mobile_decode->Sales->TotalVat,
					'TotalDiscount' 	  => $mobile_decode->Sales->TotalDiscount,
					'TotalPromo' 		  => $mobile_decode->Sales->TotalPromo,
					'TotalAmount' 		  => $mobile_decode->Sales->TotalAmount,
					'TotalAmountDue' 	  => $mobile_decode->Sales->TotalAmountDue,
					'TotalSeniorDiscount' => $mobile_decode->Sales->TotalSeniorDiscount,
					'employee_id'         => $mobile_decode->Sales->CashierID,
					'date_created'		  => date('Y-m-d H:i:s'),
					'ShiftNumber'		  => $mobile_decode->Transaction->ShiftNumber,
					'ShiftStarted'		  => $mobile_decode->Transaction->ShiftStarted,
					'ShiftEnded'		  => $mobile_decode->Transaction->ShiftEnded,
					'ChangeFund'		  => $log->ChangeFund,	
					'NetAmount'			  => $log->ChangeFund - $mobile_decode->Sales->TotalAmountDue,
				);

				$salesdata = array(
						'table' => 'sales',
						'data'  => $sales_data,
					);


				$sales_id = $this->Employees_model->dynamic_save($salesdata);


				if(!empty($sales_id) && !empty($mobile_decode->Transaction)){

					foreach($mobile_decode->Transaction->Shift as $transaction){

						$transaction_data = array(
								'DeviceSerialID'     => $transaction->DeviceSerialID,
								'DiscountPercent'    => $transaction->DiscountPercent,
								'DiscountID' 	     => $transaction->DiscountID,
								'DiscountTotal'      => $transaction->DiscountTotal,
								'CouponDiscountType' => $transaction->CouponDiscountType,
								'employee_id' 		 => $transaction->CashierID,
								'customer_id' 		 => $transaction->CustomerID,
								'promo_id' 		 	 => $transaction->PromoID,
								'PromoTotal' 		 => $transaction->PromoTotal,
								'TransactionType' 	 => $transaction->TransactionType,
								'DiscountAmount' 	 => $transaction->DiscountAmount,
								'ReceiptDateIssued'  => date('Y-m-d', strtotime($transaction->ReceiptDateIssued)),
								'PromoPercent'       => $transaction->PromoPercent,
								'CouponDiscountAmount' => $transaction->CouponDiscountAmount,
								'mobile_transaction_id' => $transaction->TransactionID,
								'PromoType' 		 => $transaction->PromoType,
								'Cash' 		 		 => $transaction->Cash,
								'Change' 		 	 => $transaction->Change,
								'TotalAmountDue' 	 => $transaction->TotalAmountDue,
								'DateCreated' 	 	 => $transaction->DateCreated,
								'CouponDiscountPercent'=> $transaction->CouponDiscountPercent,
								'ORNumber'           => $transaction->ORNumber,
								'Vat'           	 => $transaction->Vat,
								'TotalAmount'        => $transaction->TotalAmount,
								'CouponCode'         => $transaction->CouponCode,
								'TransactionNO'      => $transaction->TransactionNO,
								'DiscountType'       => $transaction->DiscountType,
								'sales_id'			 => $sales_id,

							);

						$transactiondata = array(
							'table' => 'sales_transactions',
							'data'  => $transaction_data,
						);

						$transaction_id = '';
						$transaction_id = $this->Employees_model->dynamic_save($transactiondata);



						if(!empty($transaction_id)){

							$return['result'] = 1;
							$return['msg'] = 'sales transaction saved';
						} else {

							$return['result'] = 0;
							$return['msg'] = 'failed to save transaction ID'.$transaction->TransactionID;
							break;
						}

						if(!empty($transaction_id) && !empty($transaction->OrderList) ){

							$transaction_menu = json_decode($transaction->OrderList);
							//echo "menu rows "; print_r($transaction_menu);
							foreach($transaction_menu->Item as $menu){
							//	print_r($menu); die('row');
								$menu_data = array(
										'transaction_id'        => $transaction_id,
										'ItemID' 		        => $menu->ItemID,
										'ItemQTY' 		        => $menu->ItemQTY,
										'ItemPrice' 		    => $menu->ItemPrice,
										'DefaultExcludeList'    => $menu->DefaultExcludeList,
										'AdditionalTotalAmount' => $menu->AdditionalTotalAmount,
										'ExtraAmount' 		    => $menu->ExtraAmount,
										'IsFreeItem' 		    => $menu->IsFreeItem,
										'ItemSubTotal' 		    => $menu->ItemSubTotal,
										'AddIngredientListID'	=> $menu->AddIngredientListID,
										'AdditionalInformation'	=> $menu->AdditionalInformation,
										'RemoveInformation'		=> $menu->RemoveInformation,
										'SizeOptionID'			=> $menu->SizeOptionID,
									);
								//print_r($menu_data);
								$menudata = array(
										'table' => 'sales_transaction_items',
										'data'  => $menu_data,
									);
								$order_item_id = $this->Employees_model->dynamic_save($menudata);

								if(!empty($order_item_id)){

									$return['result'] = 1;
									$return['msg'] = 'sales transaction item saved';
								} else {

									$return['result'] = 0;
									$return['msg'] = 'failed to save order item ID '.$menu->ItemID;
									break;
								}
							}

						}
						
					}
				}


			}
			
			
		} else {

			$return['msg'] = 'empty post';
			$return['result'] = 0;

		}
		
		echo json_encode($return);
		
		// echo $mobile_decode->Log0->Login;
		// echo date('Y-m-d H:i:s', $mobile_decode->Log0->Login / 1000);
	}

	//GET ALL DATA WITHOUT LOGIN AUTH
	public function fetch_server_data(){

		$inventory_fields = 'inventories.*,inventories.item as inventory_name, inventory_status.status as status_label, inventory_categories.name as category_name, CONCAT("'.base_url().'","uploads/item_image/",inventories.image) as image_url, branch_inventory_items.brach_item_qty,  IF(branch_inventory_items.brach_item_qty  is not null AND branch_inventory_items.branch_log_id = 0, branch_inventory_items.brach_item_qty , 0) AS brach_item_qty';
		 			
 		$params = array(
				'fields' => $inventory_fields,
				'order_by' 	=> array('inventories.id' => 'ASC'),
				);
 		$return['inventory_items'] = $this->inventories_model->fetch_branch_item($params);
		$return['company'] = $this->Company_model->fetch_info();
 		$return['menus'] = $this->Menus_model->fetch_all($params = array('fields' => 'menus.*, menus.title as item_name, menu_status.status_name, CONCAT("'.base_url().'","uploads/menu/",menus.image) as image_url'));
 		$return['menu_categories'] = $this->Categories_model->fetch_all($params = array('fields' => 'menu_categories.*, CONCAT("'.base_url().'","uploads/menu_categories/",menu_categories.image) as image_url'));
 		
 		$return['menu_category_options'] = $this->Categories_model->fetch_optons();
 		echo json_encode($return);
	}

	//FOR REQUEST ORDER
	public function fetch_mobile_po(){


		// $_POST['Data'] = '{"Information":{"BranchID":"1","RequestData":"2015-04-08 13:48:45","RequestBy":"2","DeviceID":"b2d3bd4f0171db1d"},"IngredientRequest":{"Ingredient 0":{"IngredientType":1,"LifeSpan":"","IngredientID":17,"DeviceSerialID":"","CanBeRemove":0,"Delivered_Temp":0,"DateCreated":"","IngredientStock":50,"CanBeAdded":0,"IngredientName":"chicken","Threshold":3,"IngredientCode":"","RequestQty":100,"IsSelected":1,"DeliveredQty":0,"IngredientPerPrice":150,"Delivered":0},"Ingredient 1":{"IngredientType":1,"LifeSpan":"","IngredientID":24,"DeviceSerialID":"","CanBeRemove":0,"Delivered_Temp":0,"DateCreated":"","IngredientStock":50,"CanBeAdded":0,"IngredientName":"hotdogs","Threshold":3,"IngredientCode":"","RequestQty":100,"IsSelected":1,"DeliveredQty":0,"IngredientPerPrice":3,"Delivered":0}}}';
		$mobile_encode = $_POST['Data'];
		$mobile_decode = json_decode($mobile_encode);
		//echo "<pre>"; print_r($mobile_decode); die;
		// echo json_encode($mobile_decode);

	
		//print_r( json_decode( $mobile_decode->OrderData ) );
		// foreach(){

		// }
		$info = $mobile_decode->Information;
		$key_code = format_code(5);
		$request_data = array(
				'RequestBy' => $info->RequestBy,
				'BranchID' => $info->BranchID,
				'RequestData' => $info->RequestData,
				'DeviceID' => $info->DeviceID,
				'date_created' => date('Y-m-d'),
				'RequestCode'	 => $key_code,
			);

		$requestdata = array(
				'table' => 'request_orders',
				'data' => $request_data,
			);

		$request_id = $this->Employees_model->dynamic_save($requestdata);

		
		foreach($mobile_decode->IngredientRequest as $ingredient){
			
		
			$rodata = array(
				'IngredientType' => $ingredient->IngredientType,
				'LifeSpan' 		 => $ingredient->LifeSpan,
				'IngredientID'   => $ingredient->IngredientID,
				'IngredientStock'=> $ingredient->IngredientStock,
				'IngredientCode' => $ingredient->IngredientCode,
				'RequestQty' 	 => $ingredient->RequestQty,
				'DeliveredQty' 	 => $ingredient->DeliveredQty,
				'Delivered' 	 => $ingredient->Delivered,
				'ro_id' 	 	 => $request_id,
				
			);


			//echo "<pre>"; print_r($podata); die;

			$ro_data = array(
					'table' => 'request_order_list',
					'data'  => $rodata,
				);

			$id = $this->Employees_model->dynamic_save($ro_data);

			if(!empty($id)){

				$return['msg'] = 'R.O entry saved!';
				$return['result'] = 1;
				$return['RequestCode'] = $key_code;
			} else {
				
				$return['msg'] = 'error in saving R.O';
				$return['result'] = 0;
				echo json_encode($return);
				exit;
			}
		}

		

		

		echo json_encode($return);
	}


	public function reset_table($table = 'branch_devices', $where = array()){

		if(!empty($where)){
			$branchdevice = $this->db->where($where)->get($table)->row();
			$this->db->where($where)->delete($table);
			
			if(!empty($branchdevice))
				$this->db->where('device_id', $branchdevice->DeviceID)->delete('branch_inventory_log');
			
		} else {
			//$this->db->truncate($table);
		}


	}


	public function truncate_login_tables($table = 'branch_devices', $where = array()){

		
		$this->db->truncate('branch_devices');
		$this->db->truncate('branch_inventory_items');
		$this->db->truncate('branch_inventory_log');
		$this->db->truncate('request_orders');
		$this->db->truncate('request_order_list');
	


	}


	public function update_ro(){


		
		//$_POST['Data'] =  '{"Information":{"BranchID":1,"UserID":2,"DeviceID":"b2d3bd4f0171db1d"}}';
		$mobile_encode = $_POST['Data'];
		$mobile_decode = json_decode($mobile_encode);

		//echo "<pre>"; print_r($mobile_decode); die;

		if(!empty($mobile_decode->Information->DeviceID)){

			$mobile_DeviceID = $mobile_decode->Information->DeviceID;

			$where = array(
					'DeviceID'  	=> $mobile_DeviceID,
					'Delivered' 	=> 1,
					'device_synced' => 0,
					'BranchID' 		=> $mobile_decode->Information->BranchID,
				);
			
			//GET REQUEST ORDER DELIVERED THAT NOT YET SYNC WITH MOBILE
			$request_orders = $this->db->where($where)->get('request_orders')->result();
			if(!empty($request_orders)){

				$return['request_orders'] = $request_orders;
				
				
				//GET ITEMS PER REQUEST ORDER
				foreach($request_orders as $ro_key =>  $ro){
					$return['request_orders'][$ro_key]->items = $this->db->where('ro_id', $ro->id)->get('request_order_list')->result();

				}

				$return['msg'] = 'Request Order update';
				$return['result'] = 1;

				$this->db->where( array('DeviceID' => $mobile_DeviceID,'BranchID' => $mobile_decode->Information->BranchID ) ) ->update('request_orders', array('device_synced' => 1));

			} else {

				$return['request_orders'] = '';

				$return['msg'] = 'No Request Order Update';
				$return['result'] = 0;
			}


		} else {

			$return['msg'] = 'Invalid DeviceID';
			$return['result'] = 0;
		}


		echo json_encode($return);
	}

	public function update_inventories(){

		$inventory_fields = 'inventories.*,inventories.item as inventory_name, inventory_status.status as status_label, inventory_categories.name as category_name, CONCAT("'.base_url().'","uploads/item_image/",inventories.image) as image_url, branch_inventory_items.brach_item_qty,  IF(branch_inventory_items.brach_item_qty  is not null AND branch_inventory_items.branch_log_id = 0, branch_inventory_items.brach_item_qty , 0) AS brach_item_qty';
		$params = array(
		'fields' => $inventory_fields,
		'order_by' 	=> array('inventories.id' => 'ASC'),
		);
			 			
			 		

		$return['inventory_items'] = $this->inventories_model->fetch_branch_item($params);


		echo json_encode($return);

		// $this->db->query('where 1')->update('inventories', array('stock' => 300));
		// echo $this->db->last_query();
		// echo $this->db->affected_rows();
	}

	public function get_branches(){

		$return['branches'] = $this->db->get('settings_branch')->result();

		echo json_encode($return);
	}

	public function register_mobile(){

		// $_POST['Data']
		$_POST = array(
			'BranchID' => 3, 
			'DeviceSerialID' => 32342, 
			);


		$rules = array(
           	'BranchID' => array(
                     'field' => 'BranchID',
                     'label' => 'BranchID',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'DeviceSerialID' => array(
                     'field' => 'DeviceSerialID',
                     'label' => 'DeviceSerialID',
                     'rules' => 'trim|required|xss_clean'
                     ),
           
          
           );
		// $_POST['Username'] = 'orden@pos.com';
		// $_POST['Password'] = 'admin';
		// $_POST['DeviceID'] = '2323';
		// $_POST['update_logs'] = '1,2,4';
		// $_POST['IsSetup'] = '1'; //REMOVE_V1 value (1,0) 1 meaning it was already sync with device
		// $_POST['isSynced'] = '1'; //REMOVE_V1 value (1,0) 1 meaning it was already sync with device
		// $_POST['isSynced'] = '1'; //REMOVE_V1 value (1,0) 1 meaning it was already sync with device

		// $_POST['IsToRefresh'] = 1;
		// $_POST['Password'] = 'test2';

		// if ( $_POST ) {

		// print_R($_POST);


		$this->form_validation->set_rules($rules);
		if ( $this->form_validation->run() ) {

			$where = array(
					'DeviceID'  => $this->input->post('DeviceSerialID'),
					'branch_id' =>  $this->input->post('BranchID'),
				);

			$result = $this->db->where($where)->from('branch_devices')->get()->row();

			if(empty($result)){

				$params['data'] = array(
					'DeviceID'     => $this->input->post('DeviceSerialID'),
					'branch_id'    =>  $this->input->post('BranchID'),
					'date_updated' => date('Y-m-d'), 
					'device_sync'  => 0, 
				);
				$params['table'] = 'branch_devices';

				$result = $this->Company_model->dynamic_save($params);

				if(!empty($result)){

					$return['result'] = 0;
					$return['msg'] = 'Successfully Registered!';

				} else {
					$return['result'] = 0;
					$return['msg'] = 'Failed to register please try again';

				}
				
			} else {

				$return['result'] = 0;
				$return['msg'] = 'DeviceSerialID already exist!';
			}

		} else {

			$return['result'] = 0;
			$return['msg'] = validation_errors();
		}


		echo json_encode($return);

	}


	public function inventory(){

		$return['result'] = 1;

		echo json_encode($return);
	}



}
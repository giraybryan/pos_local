<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchase_orders extends Admin_Controller {

	public $module = 'purchase_orders';
	public $page_title = 'Purchase Orders';
	public $controller = 'purchase_orders';
	public $table = 'purchase_orders';
	public $single = 'Purchase order';

	
	public function __construct(){

		parent::__construct();
		$this->load->model(array('purchase_orders_model','inventories_model'));
		$this->module 	= get_class($this);
		$this->redirect = strtolower($this->module);
		$this->current_module = strtolower($this->module);

	}

	public function index($keywords = 0, $branch_id = 0,$category_id = 0,$sort_by = 'purchase_orders.id', $sort_order= 'DESC', $limit = 10, $offset = 0){
		
		$data = $arrayName = array('page' => $this->page_title, 'module' => $this->module );

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);


		$data['pos'] = $this->purchase_orders_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->purchase_orders_model->fetch_all($params, true);
		
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$keywords . '/'.$branch_id.'/'.$category_id.'/'.$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');

		$data['categories'] = $this->inventories_model->get_categories();
		$data['branches'] = $this->db->get('settings_branch')->result();

		
		$this->view($this->module . '/index', $data);
	}

	public function paginate($keywords = 0, $branch_id = 0, $category_id = 0, $sort_by = 'purchase_orders.id', $sort_order = 'DESC', $limit = 10, $offset = 0){

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);
		
		if(!empty($branch_id))
			$params['where'][$this->table.'.BranchID'] = $branch_id;	
		
		if(!empty($category_id))
			$params['where']['inventories.inventory_category_id'] = $category_id;

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}

				$params['or_like']['inventories.item'] = $keywords;
				$params['or_like']['settings_branch.branch_name'] = $keywords;
				$params['or_like']['inventory_categories.name'] = $keywords;
		}
		
		$data = array(); 

		$data['pos'] = $this->purchase_orders_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->purchase_orders_model->fetch_all($params, true);
		
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$keywords . '/'.$branch_id.'/'.$category_id.'/'.$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 9	;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');
		
		

		echo $this->load->view($this->module . '/list', $data, true, true);
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Admin_Controller {
	public $module = 'users';
	public $page_title = 'Users';
	public $table = 'users';
	public $controller = 'users';
	public $single = 'user';

	public function __construct(){

		parent::__construct();
		$this->load->model(array('users_model'));
		$this->module 	= get_class($this);
		$this->redirect = strtolower($this->module);
		$this->current_module = strtolower($this->module);

	}

	public function index($keywords = 0, $branch_id = 0, $sort_by = 'users.id', $sort_order = 'DESC', $limit = 10, $offset = 0){
		
		$data = $arrayName = array('page' => $this->page_title, 'module' => $this->module );

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);
	
 

	
		$data['users'] = $this->users_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->users_model->fetch_all($params, true);
		
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$keywords . '/'.$branch_id.'/'.$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');


		$data['branches'] = $this->db->get('settings_branch')->result();
		$data['positions'] = $this->db->get('settings_positions')->result();
		$data['userlevels'] = $this->db->get('userlevels')->result();
		//echo 'sd';
		
		$this->view($this->module . '/index', $data);
	}


	public function add(){

		$rules = array(
           	'first_name' => array(
                     'field' => 'first_name',
                     'label' => 'First Name',
                     'rules' => 'trim|required|xss_clean'
                     ),
          /* 	'description' => array(
                     'field' => 'description',
                     'label' => 'description',
                     'rules' => 'trim|required|xss_clean'
                     ),*/
           	'last_name' => array(
                     'field' => 'last_name',
                     'label' => 'Last Name',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'position_id' => array(
                     'field' => 'position_id',
                     'label' => 'Position',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'userlevel_id' => array(
                     'field' => 'userlevel_id',
                     'label' => 'Userlevel',
                     'rules' => 'trim|required|xss_clean'
                     ),
         	'email' => array(
                     'field' => 'email',
                     'label' => 'Email',
                     'rules' => 'trim|required|xss_clean'
                     ),
         	'password' => array(
                     'field' => 'password',
                     'label' => 'Password',
                     'rules' => 'trim|required|xss_clean'
                     ),
          	'phone' => array(
                     'field' => 'phone',
                     'label' => 'Phone',
                     'rules' => 'trim|required|xss_clean'
                     ),
          	'branch_id' => array(
                     'field' => 'branch_id',
                     'label' => 'Branch',
                     'rules' => 'trim|required|xss_clean'
                     ),
           );
		
		$this->form_validation->set_rules($rules);


		 if ( $this->form_validation->run() ) {
		 		$form_data = $this->input->post();

		 		
		 		
		 		if(empty($form_data['id'])){
		 			$form_data['date_created'] = date('Y-m-d');
		 		}
		 			

	 			$module_data = array(
					'table' 	=> $this->table,
					'data'		=> $form_data,
					'module'	=> $this->current_module,
				);

				
	 			$id = $this->users_model->dynamic_save($module_data);

	 			if(!empty($id)){

					$image_params = array(
						'module' 	=> $this->table,
						'id' 	=> $id,
					);
					// print_r($_FILES);
					$result = $this->users_model->save_images($id, $image_params);

					$return['result'] = 1;
					$return['msg'] = $this->single.' Saved!';

				} else {

					$result['result'] = 0;
					$result['msg'] = 'Error in saving '. $this->single.' !';

				}

		 } else {
		 	$return['msg'] = validation_errors();
		 	$return['result'] = 0;
		 }
		
		echo json_encode($return);
	}

	public function delete(){

		if( $this->input->post() ) {
			$delete_param = array(
				'table' 	=> $this->table,
				'module'	=> $this->current_module,
				'where' 	=> $this->input->post(),
			);
			
			$result = $this->users_model->dynamic_delete($delete_param);

			if(!empty($result)){
				$return['msg'] =  $this->single.' Successfully deleted';
				$return['result'] = 1;
			} else {
				$return['msg'] = 'Error in deleting '. $this->single;
				$return['result'] = 0;
			}
			
		} else {
			$return['msg'] = 'Error in deleting '. $this->single;
			$return['result'] = 0;
		}

		echo json_encode($return);
	}

	public function paginate($keywords = 0, $branch_id = 0, $sort_by = 'coupons.id', $sort_order = 'DESC', $limit = 10, $offset = 0){

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);
		
		if(!empty($branch_id))
			$params['where'][$this->table.'.branch_id'] = $branch_id;	
		

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}
		
		$data = array(); 

		$data['users'] = $this->users_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->users_model->fetch_all($params, true);
		
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$keywords . '/'.$branch_id.'/'.$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 8;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');
		
		

		echo $this->load->view($this->module . '/list', $data, true, true);
	}



}
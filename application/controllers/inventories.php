<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventories extends Admin_Controller {
	
	public $module;
	public $redirect;
	public $security;
	public $page_title;
	public $permission_module 	= "Userlevel Permissions";
	public $table = 'inventories';
	
	public function __construct()
	{
		error_reporting(E_ALL & ~E_NOTICE);
		
		parent::__construct();
		$this->load->model(array('inventories_model','inventory_categories'));
		$this->module 	= get_class($this);
		$this->redirect = strtolower($this->module);
		$this->current_module = strtolower($this->module);
		$this->load->library('form_validation');
		$this->page_title = $this->module;
		//$this->security 	= $this->abstracts->set_security_access($this->module);
	}
	
	
	public function index($category_id = 0, $keyword = '0', $status_id = 0, $sort_by = 'inventories.id', $sort_order = 'DESC',$limit = 20, $offset = 0){
		
		


		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' => array($sort_by => $sort_order),
		);
		
		if(!empty($category_id))
			$params['where']['inventories.inventory_category_id'] = $category_id;
		
		if(!empty($keyword))
			$params['like']['inventories.item'] = $keyword;
		
		if(!empty($status_id))
			$params['where']['inventories.status'] = $status_id;
		
		
	
		// print_r($params);

		$data = $this->paginate_category();

		$data['limit'] = $limit;
		$data['sort_order']  = ($sort_order == 'DESC'  ? 'ASC' : 'DESC');
		$data['category_id'] = $category_id;
		$data['keyword'] 	 = $keyword;
		$data['status_id'] 	 = $status_id;
		$data['offset'] 	 = $offset;



		$data['page'] = $this->page_title;
		$data['module'] = $this->module;

		$data['status'] = $this->inventories_model->fetch_status();
		$data['items'] = $this->inventories_model->fetch_all($params);
		// echo '<pre>'; print_R($data['items']);
		// echo $this->db->last_query(); die;
		unset($params['limit']);
		$total = $this->inventories_model->fetch_all($params, true);
		
		
		$this->load->library('pagination');
		$config['base_url']			= site_url( '/inventories/paginate/' . $category_id . '/' .$keyword . '/' .$status_id. '/' . $sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('item_list');
		// echo $this->db->last_query(); die;
		
		
		//echo "<pre>"; print_r($data); die;
		
		if($this->input->is_ajax_request()){
		
			echo $this->load->view($this->module . '/list', $data, true, true);
		
		} else {
			$this->view($this->module . '/index', $data);
		}
		
	
	}
	
	
	public function add_item(){
		// print_R($_POST);
		
		// echo strtolower($this->module); die;
		$module_data = array(
			'table' 	=> $this->current_module,
			'data'		=> $this->input->post(),
			'module'	=> $this->current_module,
		); 

		if(empty($module_data['data']['id']))
			$module_data['data']['item_code'] = format_code(5);
		
		$id = $this->inventories_model->dynamic_save($module_data);
		
		if(!empty($id)){
		
			$image_params = array(
				'module' 	=> 'inventories',
				'id' 	=> $id,
			);
			// print_r($_FILES);
			$return = $this->inventories_model->save_images($id, $image_params);
			
			if(!empty($return)){
				echo $return;
			} else {
				echo "Item Saved!";
			}
		
		}
		
		
	}
	
	
	function delete_item(){
	
		if( $this->input->post() ) {
			$delete_param = array(
				'table' 	=> $this->current_module,
				'module'	=> $this->current_module,
				'where' 	=> $this->input->post(),
			);
			
			$return = $this->inventories_model->dynamic_delete($delete_param);
			
		}
	}
	
	function paginate($category_id = 0, $keyword = '0', $status_id = 0, $sort_by = 'id', $sort_order = 'ASC', $limit = 20, $offset = 0){
		
		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' => array($sort_by => $sort_order),
		);
		
		if(!empty($category_id))
			$params['where']['inventories.inventory_category_id'] = $category_id;
		
		if(!empty($keyword))
			$params['like']['inventories.item'] = $keyword;
		
		if(!empty($status_id))
			$params['where']['inventories.status'] = $status_id;
		
		
		if(!empty($keyword)){
			$columns = $this->db->list_fields($this->table);
			
			foreach ($columns as $key => $value) {
				if(empty($params['like'])) {

					$params['like'][$this->table.'.'.$value] = $keyword;
				} else {
					$params['or_like'][$this->table.'.'.$value] = $keyword;
				}		
			}
		}

		//print_R($params);
		
	
		$data['limit'] = $limit;
		$data['sort_order']  = ($sort_order == 'DESC'  ? 'ASC' : 'DESC');
		$data['category_id'] = $category_id;
		$data['keyword'] 	 = $keyword;
		$data['status_id'] 	 = $status_id;
		$data['offset'] 	 = $offset;
		
		
		$data['categories'] = $this->inventory_categories->fetch();
		$data['items'] = $this->inventories_model->fetch_all($params);
		echo $this->db->last_query();
		unset($params['limit']);
		$total = $this->inventories_model->fetch_all($params, true);
		
		
		$this->load->library('pagination');
		$config['base_url']			= site_url( '/inventories/paginate/' . $category_id . '/' .$keyword . '/' .$status_id. '/' . $sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 9;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('item_list');
		
		
		echo $this->load->view($this->module . '/list', $data, true, true);
		
	
	}

	


	function add_category(){
		$this->form_validation->set_rules('category_name', 'Category Name', 'trim|required');
		if ($this->form_validation->run() != FALSE) {
			$form_data =  array(
						'id'   => $this->input->post('category_id'), 
						'name' => $this->input->post('category_name') 
					);
			$module_data = array(
				'table' 	=> 'inventory_categories',
				'data'		=> $form_data,
				'module'	=> $this->current_module,
			);
			$id = $this->inventories_model->dynamic_save($module_data);

			if(!empty($id)){

				$image_params = array(
					'module' 	=> 'inventory_categories',
					'id' 	=> $id,
				);
				// print_r($_FILES);
				$return = $this->inventories_model->save_images($id, $image_params);
				$result['result'] = 1;
				$result['msg'] = 'Category Saved!';

			} else {

				$result['result'] = 0;
				$result['msg'] = 'Error in saving category!';

			}
			

		} else {
			$result['msg'] = validation_errors();
			$result['result'] = 0;
		}
		

		echo json_encode($result); 
		//print_r($module_data);
		//



	}

	function delete_category(){

		if( $this->input->post() ) {
			$delete_param = array(
				'table' 	=> 'inventory_categories',
				'module'	=> $this->current_module,
				'where' 	=> $this->input->post(),
			);
			
			$result = $this->inventories_model->dynamic_delete($delete_param);

			if(!empty($result)){
				$return['msg'] = 'Category Successfully deleted';
				$return['result'] = 1;
			} else {
				$return['msg'] = 'Error in deleting category';
				$return['result'] = 0;
			}
			
		} else {
			$return['msg'] = 'Error in deleting category';
			$return['result'] = 0;
		}

		echo json_encode($return);
	}

	function paginate_category($keyword = 0, $sort_by = 'id', $sort_order = 'ASC', $limit = 20, $offset = 0){

		$table = 'inventory_categories';

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);

		if(!empty($keyword)){

				$columns = $this->db->list_fields('inventory_categories');
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$table.'.'.$value] = $keyword;
					} else {
						$params['or_like'][$table.'.'.$value] = $keyword;
					}
						
				}
				
		}

		$data['categories'] = $this->inventories_model->get_categories($params);
		unset($params['limit']);
		$total = $this->inventories_model->get_categories($params, true);

		$this->load->library('pagination');
		$config['base_url']			= site_url( '/inventories/paginate_category/' . $keyword. '/' . $sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		$data['category_pagination_link'] 	= $this->pagination->create_ajax_links('category_list');


		if ($this->input->is_ajax_request()) {
			echo $this->load->view($this->module .'/inventory_category_list', $data, true);
		} else {
			return $data;
		}



	}

	function get_category_option($sort_by = 'name', $sort_order = 'ASC'){

		$params = array(
			'order_by' 	=> array($sort_by => $sort_order),
		);

		$data['categories'] = $this->inventories_model->get_categories($params);

		echo $this->load->view($this->module.'/category_field_options', $data, true);


	}
	
}
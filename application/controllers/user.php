<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends Admin_Controller {
		
	public $module;	
	public $redirect;
	public $security;
	
	function __construct() 
	{

		parent::__construct();		
		$this->module 	= get_class($this);
		$this->redirect = strtolower($this->module);
	
		date_default_timezone_set("Asia/Singapore");
	}
	
	function index($sort_by='date_created',$sort_order='DESC', $code=0, $page=0)
	{

		$data["page_title"] = "Users";
		$rows				= $this->config->item('rows_per_page');
		
		$data["sort_by"	]	= $sort_by;
		$data["sort_order"]	= $sort_order;
		$data['code']		= $code;
		$term				= false;
		
		$post	= $this->input->post(null, false);
		if($post)
		{
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
			//reset the term to an object for use
			$term	= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
 		$data['term']		= $term;
		$data['users'] 		= $this->User_model->fetch_list(array(
																	'search'		=> $term,
																	'sort_by' 		=> $sort_by,
																	'sort_order' 	=> $sort_order,
																	'limit' 		=> $rows,
																	'offset' 		=> $page,
																));
		$total 				= $this->User_model->fetch_list(array(
																	'search' 		=> $term,
																	'sort_by' 		=> $sort_by,
																	'sort_order' 	=> $sort_order,
																),true);			
		
		$config['base_url']			= site_url('/user/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $total;
		$config['per_page']			= $rows;
		$config['uri_segment']		= 6;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<ul class="pagination">';
		$config['full_tag_close']	= '</ul>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
		
		$data['pagination_link']= $this->pagination->create_links();		
		
		$data["security"] = $this->security;
		if(!$data["security"]->CanView)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect("tasks");
		}
				
		$data['current_user']  	= $this->user_session->userdata['user'];	
		$data["module"] 		= strtolower($this->module);
					
		$this->view($this->module.'/list', $data);
	}
	
	function add()
	{
		$data["security"] = $this->security;
		if(!$data["security"]->CanAdd)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect( $this->redirect );
		}
		
		$data["page_title"] 	= "Add Users";
		$data["module"] 		= strtolower($this->module);
		
		if( $post = $this->input->post() )
		{
			if($post["new_password"] != "" || $post["confirm_password"] != ""){
				
				if($post["password"])
				{
					if($post["current_password"] == "" || ($post["current_password"] != $post["password"]))
					{
						$this->session->set_flashdata('error', "Current Password did not match!");
						redirect( $this->redirect."/add/" );
					}
				}
				
				if($post["new_password"] == "" || $post["confirm_password"] == "")
				{
					$this->session->set_flashdata('error', "Password/Confirm Password is empty!");
					redirect( $this->redirect."/add/" );
				}
				
				if($post["new_password"] != $post["confirm_password"])
				{
					$this->session->set_flashdata('error', "Password did not match!");
					redirect( $this->redirect."/add/" );
				}
				
				$save["password"] 	= $post["new_password"];
			}
			
			if($post["username"] != "")
			{
				$save['username']	= $post['username'];
			}
			
			$save['first_name']		= $post['first_name'];
			$save['last_name']		= $post['last_name'];
			$save['phone']		 	= $post['phone'];
			$save['email']		 	= $post['email'];
			$save['userlevel_id']	= $post['userlevel_id'];
			$save['position_id']	= $post['position_id'];
			$save['date_created'] 	= date('Y-m-d H:i:s');
			
			$user_id = $this->User_model->insert($save);
			if(!$user_id)
			{
				$this->session->set_flashdata('error', "Username Already Exist!");
				redirect( $this->redirect."/add/" );
			}
			
			if(isset($post["projects"]) && count($post["projects"]) > 0)
			{
				//save assigned projects here
				$this->User_model->update_user_projects($user_id, $post["projects"]);
			}
			
			if($_FILES["image_file"]["error"] != 4)
			{
				//upload image
				$path						= $this->config->item('user_upload_path');
				$images						= _image_process($user_id, $path);
				$user_image["image_file"] 	= $images[0];
				$this->User_model->update($user_id, $user_image);
			}
			
			$this->session->set_flashdata("message", "User Successfully Added");			
			redirect( $this->redirect );
		}
		
		$data['positions']	= $this->abstracts->get_positions();
		$data['userlevels']	= $this->abstracts->get_userlevels();
		$data['projects']	= $this->abstracts->get_projects();
		
		$user 		= $this->user_session->userdata['user'];		
		$is_admin 	= false;
		if($user["userlevel_id"] == -1)
		{
			$data['is_admin'] = true;
		}

		$this->view($this->module.'/form', $data);
	}
		

	function edit($id)
	{
		$data["security"] = $this->security;
		if(!$data["security"]->CanEdit)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect( $this->redirect );
		}
		
		$data["page_title"] = "Edit User";
		$data["module"] 	= strtolower($this->module);
		$data['edit']		= true;
		$data['user_id']	= $id;
		
		if( $post = $this->input->post() )
		{	
			$user_id = $post["user_id"];
					
			if($post["current_password"] != "" || $post["new_password"] != "" || $post["confirm_password"] != ""){
				
				if($post["current_password"] == "" || ($post["current_password"] != $post["password"])){
					$this->session->set_flashdata('error', "Current Password did not match!");
					redirect( $this->redirect."/edit/".$post["user_id"] );
				}
				
				if($post["new_password"] == "" || $post["confirm_password"] == ""){
					$this->session->set_flashdata('error', "Password/Confirm Password is empty!");
					redirect( $this->redirect."/edit/".$post["user_id"] );
				}
				
				if($post["new_password"] != $post["confirm_password"]){
					$this->session->set_flashdata('error', "Password did not match!");
					redirect( $this->redirect."/edit/".$post["user_id"] );
				}
				
				$save["password"] 	= $post["new_password"];
			}
			
			$save['first_name']		= $post['first_name'];
			$save['last_name']		= $post['last_name'];
			$save['phone']		 	= $post['phone'];
			$save['email']		 	= $post['email'];
			$save['userlevel_id']	= $post['userlevel_id'];
			$save['position_id']	= $post['position_id'];
			
			$this->User_model->update($post['user_id'], $save);
			
			if(isset($post["projects"]) && count($post["projects"]) > 0)
			{
				//save assigned projects here
				$this->User_model->update_user_projects($user_id, $post["projects"]);
			}

			if(!isset($data['current_image']) && $_FILES["image_file"]["error"] != 4)
			{				
				$path						= $this->config->item('user_upload_path');
				$images						= _image_process($user_id, $path);
				$user_image["image_file"] 	= $images[0];
				$this->User_model->update($user_id, $user_image);
			}
			
			$this->session->set_flashdata("message", "User Successfully Updated");
			redirect( $this->redirect );
		}
		
		$post["user_id"]		= $id;
		$data['rec']	   		= $this->User_model->fetch($id);
		$data['img_path']		= base_url($this->config->item('user_upload_path').$data['rec']->user_id."/");
		$data['projects_list']	= $this->User_model->fetch_projects($id);
		
		$data['positions']  	= $this->abstracts->get_positions();
		$data['userlevels']		= $this->abstracts->get_userlevels();	
		$data['projects']		= $this->abstracts->get_projects();	
		
		$data["module"]			= strtolower($this->module);
		
		$this->view($this->module.'/form', $data);
	}
	
	//copy of edit
	function settings($id)
	{
		$user = $this->user_session->userdata['user'];
		if($user["user_id"] != $id)
		{
			$this->session->set_flashdata('error', 'Illegal access of user information settings!');
			redirect( "tasks" );
		}
		
		$data["security"] = $this->security;
		if(!$data["security"]->CanEdit)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect( "tasks" );
		}
		
		$data["page_title"] = "Settings";
		$data["module"] 	= strtolower($this->module);
		$data['edit']		= true;
		$data['user_id']	= $id;
		
		if( $post = $this->input->post() )
		{	
			$user_id = $post["user_id"];
					
			if($post["current_password"] != "" || $post["new_password"] != "" || $post["confirm_password"] != ""){
				
				if($post["current_password"] == "" || ($post["current_password"] != $post["password"])){
					$this->session->set_flashdata('error', "Current Password did not match!");
					redirect( $this->redirect."/settings/".$post["user_id"] );
				}
				
				if($post["new_password"] == "" || $post["confirm_password"] == ""){
					$this->session->set_flashdata('error', "Password/Confirm Password is empty!");
					redirect( $this->redirect."/settings/".$post["user_id"] );
				}
				
				if($post["new_password"] != $post["confirm_password"]){
					$this->session->set_flashdata('error', "Password did not match!");
					redirect( $this->redirect."/settings/".$post["user_id"] );
				}
				
				$save["password"] 	= $post["new_password"];
			}
			
			$save['first_name']		= $post['first_name'];
			$save['last_name']		= $post['last_name'];
			$save['phone']		 	= $post['phone'];
			$save['email']		 	= $post['email'];
			if($post['userlevel_id'])
			{
				$save['userlevel_id']	= $post['userlevel_id'];
			}
			if($post['position_id'])
			{
				$save['position_id']	= $post['position_id'];
			}
			
			$this->User_model->update($post['user_id'], $save);
			
			if(isset($post["projects"]) && count($post["projects"]) > 0)
			{
				//save assigned projects here
				$this->User_model->update_user_projects($user_id, $post["projects"]);
			}

			if(!isset($data['current_image']) && $_FILES["image_file"]["error"] != 4)
			{				
				$path						= $this->config->item('user_upload_path');
				$images						= _image_process($user_id, $path);
				$user_image["image_file"] 	= $images[0];
				$this->User_model->update($user_id, $user_image);
			}
			
			$this->session->set_flashdata("message", "Settings Successfully Updated");
			redirect( "tasks" );
		}
		
		$post["user_id"]		= $id;
		$data['rec']	   		= $this->User_model->fetch($id);
		$data['img_path']		= base_url($this->config->item('user_upload_path').$data['rec']->user_id."/");
		$data['projects_list']	= $this->User_model->fetch_projects($id);
		
		$data['positions']  	= $this->abstracts->get_positions();
		$data['userlevels']		= $this->abstracts->get_userlevels();	
		$data['projects']		= $this->abstracts->get_projects();	
		
		$data["module"]			= strtolower($this->module);
		
		$this->view($this->module.'/form', $data);
	}
	
	public function delete($id = false)
	{
		$data["security"] = $this->security;
		if(!$data["security"]->CanDelete)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect($this->redirect);
		}
		
		if(!$id) {
			$this->session->set_flashdata('error', 'Unknown ID');
			redirect($this->redirect);
		}
		
		if($this->input->is_ajax_request()) {
			$return = $this->User_model->delete($id);
			
			//remove images
			$dir = $this->config->item('user_upload_path') . $id .'/';
			$this->rrmdir($dir);
			
			//remove record on 'user_projects' table
			$this->User_model->delete_user_projects($id);
			
			echo json_encode(array('result'=> $return));
		} else {
			$return = $this->User_model->delete($id);
			
			//remove images
			$dir = $this->config->item('user_upload_path') . $id .'/';
			$this->rrmdir($dir);
			
			//remove record on 'user_projects' table
			$this->User_model->delete_user_projects($id);
			
			$this->session->set_flashdata('error', 'Successfully Deleted!');
			redirect($this->redirect);
		}		
	}
	
	//function to remove all contents and directory
	function rrmdir($dir)
	{ 
		foreach(glob($dir . '/*') as $file) { 
			if(is_dir($file)) rrmdir($file); else unlink($file); 
		} rmdir($dir); 
	}
}
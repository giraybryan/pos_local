<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends Admin_Controller {
	
	public $module = 'reports';
	public $page_title = 'Reports';
	public $controller = 'reports';
	public $single = 'Report';
	
	public function __construct(){

		parent::__construct();
		$this->load->model(array('reports_model','employees_model','sales_orders_model'));
		$this->module 	= get_class($this);
		$this->redirect = strtolower($this->module);
		$this->current_module = strtolower($this->module);

	}


	public function index(){
		
		$data = $arrayName = array('page' => $this->page_title, 'module' => $this->module );

		if($_POST){
			

			if( !empty($_POST['employee_id']) ) {

				$where['sales.employee_id'] = $this->input->post('employee_id');
			}

			if( !empty($_POST['shift']) ) {

				$where['sales.ShiftNumber'] = $this->input->post('shift');
			}

			if( !empty($_POST['start_date']) ) {

				$where['sales.date_created >='] = date('Y-m-d', strtotime($this->input->post('start_date')) ).' 00:00:00';

			}
			if( !empty($_POST['end_date']) ) {

				$where['sales.date_created <='] = date('Y-m-d', strtotime( $this->input->post('end_date') )).' 24:59:59';
			}


			$sales_params = array(
				'order_by' => array('sales.id' => 'DESC'),
			);

			if( !empty($where) )
				$sales_params['where'] = $where;

			//echo "<pre>"; print_r($sales_params); 
			$data['sales'] = $this->sales_orders_model->fetch_all($sales_params);
			
		}



		$params = array(
			'order_by' 	=> array('employees.first_name' => 'ASC'),
		);
		

		$data['employees'] = $this->employees_model->fetch_all($params);

		
		$this->view($this->module . '/index', $data);
	}


	public function export_pdf(){

		
		echo $this->db->last_query();

			
		if( !empty($_GET['employee_id']) ) {

			$where['sales.employee_id'] = $this->input->get('employee_id');

			$data['employee'] = $this->db->where('id', $_GET['employee_id'])->get('employees')->row();

		}

		if( !empty($_GET['shift']) ) {

			$where['sales.ShiftNumber'] = $this->input->get('shift');
		}

		if( !empty($_GET['start_date']) ) {

			$where['sales.date_created >='] = date('Y-m-d', strtotime($this->input->get('start_date')) ).' 00:00:00';

		}

		if( !empty($_GET['end_date']) ) {

			$where['sales.date_created <='] = date('Y-m-d', strtotime( $this->input->get('end_date') )).' 24:59:59';
		}


		$sales_params = array(
				'order_by' => array('sales.id' => 'DESC'),
			);

		if( !empty($where) )
			$sales_params['where'] = $where;

		$data['sales'] = $this->sales_orders_model->fetch_all($sales_params);
		
		$filename = 'sales-report '.date('Y-m-d');
		$html = $this->load->view('Reports/pdf',$data, true);
		$this->load->helper('dompdf');
		// echo $html; die;
        pdf_create($html, $filename);

	}
}
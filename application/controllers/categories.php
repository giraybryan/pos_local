<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends Admin_Controller {

	public $module = 'categories';
	public $page_title = 'Categories';
	public $table = 'menu_categories';
	public $controller = 'categories';

	public function __construct(){

		parent::__construct();
		$this->load->model(array('categories_model'));
		$this->module 	= get_class($this);
		$this->redirect = strtolower($this->module);
		$this->current_module = strtolower($this->module);
	}

	public function index($keyword = 0,$sort_by = 'menu_categories.id', $sort_order = 'DESC',$limit = 20, $offset = 0){

		$data = $arrayName = array('page' => $this->page_title, 'module' => $this->module );

		$params = array(
			'limit'  => $limit,
			'offset' => $offset,
			'order_by' => array($sort_by => $sort_order),
			);


		$data['categories'] = $this->categories_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->categories_model->fetch_all($params, true);

		//echo $this->db->last_query(); die;

		$this->load->library('pagination');
		$config['base_url']			= site_url( $this->controller.'/paginate/' . $keyword . '/' .$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');

		$this->view($this->module . '/index', $data);
	}

	public function add(){

		$rules = array(
           	'menu_category_name' => array(
                     'field' => 'menu_category_name',
                     'label' => 'Menu Category',
                     'rules' => 'trim|required|xss_clean'
                     ),
          
           );
		
		$this->form_validation->set_rules($rules);


		 if ( $this->form_validation->run() ) {

	 			$module_data = array(
					'table' 	=> $this->table,
					'data'		=> $this->input->post(),
					'module'	=> $this->current_module,
				);

	 			$id = $this->categories_model->dynamic_save($module_data);

	 			if(!empty($id)){

					$image_params = array(
						'module' 	=> $this->table,
						'id' 	=> $id,
					);
					// print_r($_FILES);
					$result = $this->categories_model->save_images($id, $image_params);
					$return['result'] = 1;
					$return['msg'] = 'Category Saved!';

				} else {

					$result['result'] = 0;
					$result['msg'] = 'Error in saving category!';

				}

		 } else {
		 	$return['msg'] = validation_errors();
		 	$return['result'] = 0;
		 }
		
		echo json_encode($return);
	}

	public function delete(){

		if( $this->input->post() ) {
			$delete_param = array(
				'table' 	=> $this->table,
				'module'	=> $this->current_module,
				'where' 	=> $this->input->post(),
			);
			
			$result = $this->categories_model->dynamic_delete($delete_param);

			if(!empty($result)){
				$return['msg'] = 'Category Successfully deleted';
				$return['result'] = 1;
			} else {
				$return['msg'] = 'Error in deleting category';
				$return['result'] = 0;
			}
			
		} else {
			$return['msg'] = 'Error in deleting category';
			$return['result'] = 0;
		}

		echo json_encode($return);
	}

	public function paginate($keywords = 0, $sort_by = 'menu_categories.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);
		
		
		
		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}
		
		$data = array(); 
		
		// print_r($params); die;
		$data['categories'] = $this->categories_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->categories_model->fetch_all($params, true);
		
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' . $keywords . '/' .$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');
		
		
		echo $this->load->view($this->module . '/list', $data, true, true);

	}

	function category_options($category_id = false){
		$params = array();

		if(!empty($category_id))
			$params['where']['menu_category_id'] = $category_id;

		$data['options'] = $this->categories_model->fetch_optons($params);
		
		echo $this->load->view($this->module.'/option_list', $data, true);
		
	}

	function add_option(){
		$rules = array(
           	'option_name' => array(
                     'field' => 'option_name',
                     'label' => 'Option Name',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'price' => array(
                     'field' => 'price',
                     'label' => 'Price',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'menu_category_id' => array(
                     'field' => 'menu_category_id',
                     'label' => 'Menu Category',
                     'rules' => 'trim|required|xss_clean'
                     ),
          
           );
		
		$this->form_validation->set_rules($rules);


		 if ( $this->form_validation->run() ) {

		 	$form_data = array(
		 		'option_name' 	   => $this->input->post('option_name'),
		 		'id' 		  	   => $this->input->post('option_id'),
		 		'menu_category_id' => $this->input->post('menu_category_id'),
		 		'price' 		   => $this->input->post('price'),
		 		);


		 	$module_data = array(
				'table' 	=> 'menu_category_options',
				'data'		=> $form_data,
				'module'	=> $this->current_module,
			);

			$id = $this->categories_model->dynamic_save($module_data);

			if(!empty($id)){

				// print_r($_FILES);
				$return['result'] = 1;
				$return['msg'] = 'Option Saved!';

			} else {

				$return['result'] = 0;
				$return['msg'] = 'Error in saving option!';

			}

		 } else {
		 		$return['msg'] = validation_errors();
		 	$return['result'] = 0;
		 }
		
		echo json_encode($return);
	}

	function delete_option(){


		if( $this->input->post() ) {
			$delete_param = array(
				'table' 	=> 'menu_category_options',
				'module'	=> $this->current_module,
				'where' 	=> $this->input->post(),
			);
			
			$result = $this->categories_model->dynamic_delete($delete_param);

			if(!empty($result)){
				$return['msg'] = 'Option Successfully deleted';
				$return['result'] = 1;
			} else {
				$return['msg'] = 'Error in deleting Option';
				$return['result'] = 0;
			}
			
		} else {
			$return['msg'] = 'Error in deleting Option';
			$return['result'] = 0;
		}

		echo json_encode($return);
	}
}
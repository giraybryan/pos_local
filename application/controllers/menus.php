<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menus extends Admin_Controller {

	public $module = 'menus';
	public $page_title = 'Menus';
	public $table = 'menus';
	public $controller = 'menus';

	public function __construct(){

		parent::__construct();
		$this->load->model(array('menus_model', 'categories_model','inventories_model','inventory_categories'));
		$this->module 	= get_class($this);
		$this->redirect = strtolower($this->module);
		$this->current_module = strtolower($this->module);

	}

	public function index($category_id = 0, $keywords = 0, $status_id = 0,$sort_by = 'menus.id', $sort_order= 'DESC', $limit = 20, $offset = 0){

		$data = $arrayName = array('page' => $this->page_title, 'module' => $this->module );

		$category_params = array('order_by' => array('id' => 'ASC'));
		$data['categories'] = $this->categories_model->fetch_all($category_params);

		$data['limit'] = $limit;
		$data['sort_order']  = ($sort_order == 'DESC'  ? 'ASC' : 'DESC');
		$data['category_id'] = $category_id;
		$data['keywords'] 	 = $keywords;
		$data['status_id'] 	 = $status_id;
		$data['offset'] 	 = $offset;
		
		$data['status'] = $this->db->get('menu_status')->result();

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);
		
		if(!empty($category_id))
			$params['where'][$this->table.'.menu_category_id'] = $category_id;	
		
		if(!empty($status_id))
			$params['where'][$this->table.'.status_id'] = $status_id;	


		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

	
		$data['menus'] = $this->menus_model->fetch_all($params);
		
		unset($params['limit']);
		$total = $this->menus_model->fetch_all($params, true);
	
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$category_id.'/'. $keywords . '/'.$status_id.'/'.$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');

		$this->view($this->module . '/index', $data);


		
	}

	public function add(){

		$rules = array(
           	'title' => array(
                     'field' => 'title',
                     'label' => 'Menu Title',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'description' => array(
                     'field' => 'description',
                     'label' => 'description',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'price' => array(
                     'field' => 'price',
                     'label' => 'Price',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'status_id' => array(
                     'field' => 'status_id',
                     'label' => 'Menu Status',
                     'rules' => 'trim|required|xss_clean'
                     ),
          
           );
		
		$this->form_validation->set_rules($rules);


		 if ( $this->form_validation->run() ) {

	 			$module_data = array(
					'table' 	=> $this->table,
					'data'		=> $this->input->post(),
					'module'	=> $this->current_module,
				);

	 			$id = $this->menus_model->dynamic_save($module_data);

	 			if(!empty($id)){

					$image_params = array(
						'module' 	=> $this->table,
						'id' 	=> $id,
					);
					// print_r($_FILES);
					$result = $this->menus_model->save_images($id, $image_params);
					$return['result'] = 1;
					$return['msg'] = 'Category Saved!';
					$return['modify_link'] = base_url('menus/menu_ingredients/'.$id);

				} else {

					$result['result'] = 0;
					$result['msg'] = 'Error in saving category!';

				}

		 } else {
		 	$return['msg'] = validation_errors();
		 	$return['result'] = 0;
		 }
		
		echo json_encode($return);
	}

	public function delete(){

		if( $this->input->post() ) {
			$delete_param = array(
				'table' 	=> $this->table,
				'module'	=> $this->current_module,
				'where' 	=> $this->input->post(),
			);
			
			$result = $this->menus_model->dynamic_delete($delete_param);

			if(!empty($result)){
				$return['msg'] = 'Menu Successfully deleted';
				$return['result'] = 1;
			} else {
				$return['msg'] = 'Error in deleting Menu';
				$return['result'] = 0;
			}
			
		} else {
			$return['msg'] = 'Error in deleting Menu';
			$return['result'] = 0;
		}

		echo json_encode($return);
	}

	public function paginate($category_id = 0, $keywords = 0, $status_id = 0, $sort_by = 'menus.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);
		
		if(!empty($category_id))
			$params['where'][$this->table.'.menu_category_id'] = $category_id;	
		
		if(!empty($status_id))
			$params['where'][$this->table.'.status_id'] = $status_id;	


		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}
		
		$data['limit'] = $limit;
		$data['sort_order']  = ($sort_order == 'DESC'  ? 'ASC' : 'DESC');
		$data['category_id'] = $category_id;
		$data['keywords'] 	 = $keywords;
		$data['status_id'] 	 = $status_id;
		$data['offset'] 	 = $offset;

		$data['menus'] = $this->menus_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->menus_model->fetch_all($params, true);
		
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$category_id.'/'. $keywords . '/'.$status_id.'/'.$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 9;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');
		
		

		echo $this->load->view($this->module . '/list', $data, true, true);
	}

	public function menu_ingredients($menu_id = false){


		if($menu_id){

			$data['menu'] = $this->menus_model->get_menu_by_id($menu_id);

			//get category options tagged in the menu
			$options_params = array('where' => array('menu_category_id' => $data['menu']->menu_category_id));
			
			$data['category_options'] = $this->categories_model->fetch_optons($options_params);

			
			
			$data['items'] = $this->inventories_model->fetch_all();

			$data['ingredients'] = $this->menus_model->get_ingredients($menu_id);
			$data['addons'] = $this->menus_model->get_addons($menu_id);
			$data['excludes'] = $this->menus_model->get_excludes($menu_id);
			
			$data['exclude_ids'] = array();
			foreach($data['excludes'] as $exclude_item){
				$data['exclude_ids'][] = $exclude_item->id;


			}

			// echo "<pre>"; print_R($data['ingredients']); die;

			$category_params = array('order_by' => array('name' => 'ASC'));
			$data['categories'] = $this->inventory_categories->fetch($category_params);

			
			$this->view($this->module . '/modify_menu', $data);
	
		}
		
	}

	public function modify_menu(){

		$post = $_POST;

		// print_r($post); die;	
		$this->db->where('menu_id', $this->input->post('menu_id'))->delete('menu_ingredients');
		if(!empty($post['ingredient'])) {

			foreach($post['ingredient'] as $ingredient){
				$data = array(
					'menu_id' => $this->input->post('menu_id'),
					'ingredient_id' => $ingredient['ingredient_id'],
					'qty' => $ingredient['qty'],

					);

				$module_data = array(
						'table' 	=> 'menu_ingredients',
						'data'		=> $data,
						'module'	=> 'Ingredients Menu',
					);
				$id = $this->menus_model->dynamic_save($module_data);
				// /echo $this->db->last_query();

			}
			
		}
		/*if(!empty($post['ingredients'])) {
			
			$ingredient_ids = explode(',',$post['ingredients']);
			
			$this->db->where('menu_id', $this->input->post('menu_id'))->delete('menu_ingredients');
			if(!empty($ingredient_ids)){
			
				
				foreach($ingredient_ids as $key => $ingredient_id){

					$data = array(
						'menu_id' => $this->input->post('menu_id'),
						'ingredient_id' => $ingredient_id,
						);

					$module_data = array(
						'table' 	=> 'menu_ingredients',
						'data'		=> $data,
						'module'	=> 'Ingredients Menu',
					);

		 			$id = $this->menus_model->dynamic_save($module_data);

				}

			} 
			
		}*/

		$this->db->where('menu_id', $this->input->post('menu_id'))->delete('menu_addons');
		if(!empty($post['addons'])){

			$addons_ids = explode(',',$post['addons']);
			
			if(!empty($addons_ids)){

				

				foreach($addons_ids as $key => $addon_id){

					$data = array(
						'menu_id' => $this->input->post('menu_id'),
						'addon_id' => $addon_id,
						);

					
					$module_data = array(
						'table' 	=> 'menu_addons',
						'data'		=> $data,
						'module'	=> 'Addons Menu',
					);

		 			$id = $this->menus_model->dynamic_save($module_data);
				}

			} 
			
		}

		$this->db->where('menu_id', $this->input->post('menu_id'))->delete('menu_excludes');

		if(!empty($post['excludes'])){

			$excludes_ids = explode(',',$post['excludes']);
			
			if(!empty($excludes_ids)){

				

				foreach($excludes_ids as $key => $exclude_id){

					$data = array(
						'menu_id' => $this->input->post('menu_id'),
						'item_id' => $exclude_id,
						);

					
					$module_data = array(
						'table' 	=> 'menu_excludes',
						'data'		=> $data,
						'module'	=> 'Addons Menu',
					);

		 			$id = $this->menus_model->dynamic_save($module_data);
				}

			} 
		}

		$return['msg'] = 'Menu Updated!';
		$return['result'] = 1;

		echo json_encode($return);
	}


	function get_inventory_items($category_id = 0){

		$params = array();

		if(!empty($category_id)){

			$params['where']['inventories.inventory_category_id'] = $category_id;
		}

		$data['items'] = $this->inventories_model->fetch_all($params);

		echo $this->load->view($this->module.'/inventory_item', $data, true);
	}

	function get_menus(){


		$params['type'] = 'array';
		if(!empty($_GET['q']))
			$params['like']['menus.title'] = $_GET['q'];

		$data['items'] = $this->menus_model->fetch_all($params);

		//echo $this->db->last_query();
	
		echo json_encode($data);
	}
}
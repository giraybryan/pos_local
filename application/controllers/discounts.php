<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Discounts extends Admin_Controller {
	public $module = 'discounts';
	public $page_title = 'Discounts';
	public $table = 'discounts';
	public $controller = 'discounts';
	public $single = 'Discount';

	public function __construct(){

		parent::__construct();
		$this->load->model(array('discounts_model'));
		$this->module 	= get_class($this);
		$this->redirect = strtolower($this->module);
		$this->current_module = strtolower($this->module);

	}

	public function index($keywords = 0, $reduction_type = 0, $sort_by = 'discounts.id', $sort_order = 'DESC', $limit = 10, $offset = 0){
		$data = $arrayName = array('page' => $this->page_title, 'module' => $this->module );

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);
	

	
	
		$data['discounts'] = $this->discounts_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->discounts_model->fetch_all($params, true);
	
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$keywords . '/'.$reduction_type.'/'.$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');

		$data['discount_types'] = $this->db->get('discount_type')->result();

		$this->view($this->module . '/index', $data);
	}


	public function add(){

		$rules = array(
           	'title' => array(
                     'field' => 'title',
                     'label' => 'Discount Title',
                     'rules' => 'trim|required|xss_clean'
                     ),
          /* 	'description' => array(
                     'field' => 'description',
                     'label' => 'description',
                     'rules' => 'trim|required|xss_clean'
                     ),*/
           	'reduction_type' => array(
                     'field' => 'reduction_type',
                     'label' => 'Discoun Type',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'reduction_amount' => array(
                     'field' => 'reduction_amount',
                     'label' => 'Discount Amount',
                     'rules' => 'trim|required|xss_clean'
                     ),
          
           );
		
		$this->form_validation->set_rules($rules);


		 if ( $this->form_validation->run() ) {

	 			$module_data = array(
					'table' 	=> $this->table,
					'data'		=> $this->input->post(),
					'module'	=> $this->current_module,
				);

	 			$id = $this->discounts_model->dynamic_save($module_data);

	 			if(!empty($id)){

				
					$return['result'] = 1;
					$return['msg'] = $this->single.' Saved!';

				} else {

					$result['result'] = 0;
					$result['msg'] = 'Error in saving '. $this->single.' !';

				}

		 } else {
		 	$return['msg'] = validation_errors();
		 	$return['result'] = 0;
		 }
		
		echo json_encode($return);
	}

	public function delete(){

		if( $this->input->post() ) {
			$delete_param = array(
				'table' 	=> $this->table,
				'module'	=> $this->current_module,
				'where' 	=> $this->input->post(),
			);
			
			$result = $this->discounts_model->dynamic_delete($delete_param);

			if(!empty($result)){
				$return['msg'] =  $this->single.' Successfully deleted';
				$return['result'] = 1;
			} else {
				$return['msg'] = 'Error in deleting '. $this->single;
				$return['result'] = 0;
			}
			
		} else {
			$return['msg'] = 'Error in deleting '. $this->single;
			$return['result'] = 0;
		}

		echo json_encode($return);
	}

	public function paginate($keywords = 0, $reduction_type = 0, $sort_by = 'discounts.id', $sort_order = 'DESC', $limit = 10, $offset = 0){

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);
		
		if(!empty($reduction_type))
			$params['where'][$this->table.'.reduction_type'] = $reduction_type;	
		

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}
		
		$data = array(); 

		$data['discounts'] = $this->discounts_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->discounts_model->fetch_all($params, true);
		
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$keywords . '/'.$reduction_type.'/'.$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 8;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');
		
		

		echo $this->load->view($this->module . '/list', $data, true, true);
	}



}
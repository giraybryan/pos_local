<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends Admin_Controller {

	public $module = 'company';
	public $page_title = 'Company info';
	public $table = 'settings_company_info';
	public $controller = 'company';

	public function __construct(){

		parent::__construct();
		//$this->load->model(array('company_model'));
		$this->module 	= get_class($this);
		$this->redirect = strtolower($this->module);
		$this->current_module = strtolower($this->module);

	}

	public function index(){

		$data = $arrayName = array('page' => $this->page_title, 'module' => $this->module );

		$data['info'] = $this->Company_model->fetch_info();
		// print_r($data); echo $this->db->last_query(); die;
		$this->view($this->module . '/index', $data);
	}

	public function add(){

		$rules = array(
           	'name' => array(
                     'field' => 'name',
                     'label' => 'Company Name',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'slogan' => array(
                     'field' => 'slogan',
                     'label' => 'Slogan',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'address' => array(
                     'field' => 'address',
                     'label' => 'Address',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	
          
           );
		
		$this->form_validation->set_rules($rules);


		 if ( $this->form_validation->run() ) {

	 			$module_data = array(
					'table' 	=> $this->table,
					'data'		=> $this->input->post(),
					'module'	=> $this->current_module,
				);

	 			$id = $this->Company_model->dynamic_save($module_data);

	 			if(!empty($id)){

					$image_params = array(
						'module' 	=> $this->table,
						'id' 	=> $id,
					);
					// print_r($_FILES);
					$result = $this->Company_model->save_images($id, $image_params);
					$return['result'] = 1;
					$return['msg'] = 'Company Information Saved!';

				} else {

					$result['result'] = 0;
					$result['msg'] = 'Error in saving information!';

				}

		 } else {
		 	$return['msg'] = validation_errors();
		 	$return['result'] = 0;
		 }
		
		echo json_encode($return);
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales_orders extends Admin_Controller {
	
	public $module = 'sales_orders';
	public $page_title = 'Sales Orders';
	public $controller = 'sales_orders';
	public $single = 'Sales';
	public $table = 'sales';
	
	public function __construct(){

		parent::__construct();
		$this->load->model(array('sales_orders_model'));
		$this->module 	= get_class($this);
		$this->redirect = strtolower($this->module);
		$this->current_module = strtolower($this->module);

	}


	public function index($keywords = 0, $branch_id = 0,$sort_by = 'sales.id', $sort_order= 'DESC', $limit = 10, $offset = 0){
		
		$data = $arrayName = array('page' => $this->page_title, 'module' => $this->module );

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);


		$data['sales'] = $this->sales_orders_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->sales_orders_model->fetch_all($params, true);
	
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$keywords . '/'.$branch_id.'/'.$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');

		
		$this->view($this->module . '/index', $data);
	}

	public function get_transactions($sales_id){
		$params = array('where' => array('sales_transactions.sales_id' => $sales_id));
		$data['transactions'] = $this->sales_orders_model->fetch_transactions($params);

		echo $this->load->view($this->module.'/transaction_list', $data, true);

	}
	
	public function get_transaction_details($id)
	{
		
		$data['details'] = $this->sales_orders_model->fetch_transaction_details($id);
		$data['transaction'] = $this->sales_orders_model->fetch_transaction($id);

		//print_R($data['transaction']);
		$this->load->view($this->module.'/transaction_details', $data);
	}

	public function paginate($keywords = 0, $branch_id = 0, $sort_by = 'sales.id', $sort_order = 'DESC', $limit = 10, $offset = 0){

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);
		
		if(!empty($branch_id))
			$params['where'][$this->table.'.branch_id'] = $branch_id;	
		

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}
		
		$data = array(); 

		$data['sales'] = $this->sales_orders_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->sales_orders_model->fetch_all($params, true);
		
	
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$keywords . '/'.$branch_id.'/'.$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 8;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');
		
		

		echo $this->load->view($this->module . '/list', $data, true, true);
	}
	
	
}
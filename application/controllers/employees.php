<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employees extends Admin_Controller {

	public $module = 'employee';
	public $page_title = 'Employees';
	public $table = 'employees';
	public $controller = 'employees';
	public $single = 'Employee';
	public function __construct(){

	parent::__construct();

		$this->load->model(array('employees_model'));
		$this->module 	= get_class($this);
		$this->redirect = strtolower($this->module);
		$this->current_module = strtolower($this->module);

	}

	public function index($keywords = 0, $branch_id = 0, $sort_by = 'employees.id', $sort_order = 'DESC', $limit = 10, $offset = 0){


		$data = $arrayName = array('page' => $this->page_title, 'module' => $this->module );

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);
	

	
	
		$data['employees'] = $this->employees_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->employees_model->fetch_all($params, true);
	
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$keywords . '/'.$branch_id.'/'.$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');


		$data['branches'] = $this->db->get('settings_branch')->result();
		$data['shifts'] = $this->db->get('employee_shift')->result();
		$this->view($this->module . '/index', $data);
	}


	public function add(){

		$rules = array(
           	'first_name' => array(
                     'field' => 'first_name',
                     'label' => 'First Name',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'last_name' => array(
                     'field' => 'last_name',
                     'label' => 'Last Name',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'branch_id' => array(
                     'field' => 'branch_id',
                     'label' => 'Branch',
                     'rules' => 'trim|required|xss_clean'
                     ),
           	'email' => array(
                     'field' => 'email',
                     'label' => 'Email',
                     'rules' => 'trim|required|xss_clean'
                     ),
         	'phone' => array(
                     'field' => 'phone',
                     'label' => 'Start Date',
                     'rules' => 'trim|required|xss_clean'
                     ),
         	'shift' => array(
                     'field' => 'shift',
                     'label' => 'Shift',
                     'rules' => 'trim|required|xss_clean'
                     ),
         	
           );
		
		$this->form_validation->set_rules($rules);


		 if ( $this->form_validation->run() ) {
		 		$form_data = $this->input->post();

		 		

		 		if(empty($form_data['id']))
		 			$form_data['date_created'] = date('Y-m-d');

	 			$module_data = array(
					'table' 	=> $this->table,
					'data'		=> $form_data,
					'module'	=> $this->current_module,
				);

				
	 			$id = $this->employees_model->dynamic_save($module_data);

	 			if(!empty($id)){

				
					$return['result'] = 1;
					$return['msg'] = $this->single.' Saved!';

				} else {

					$result['result'] = 0;
					$result['msg'] = 'Error in saving '. $this->single.' !';

				}

		 } else {
		 	$return['msg'] = validation_errors();
		 	$return['result'] = 0;
		 }
		
		echo json_encode($return);
	}


	public function delete(){

		if( $this->input->post() ) {
			$delete_param = array(
				'table' 	=> $this->table,
				'module'	=> $this->current_module,
				'where' 	=> $this->input->post(),
			);
			
			$result = $this->employees_model->dynamic_delete($delete_param);

			if(!empty($result)){
				$return['msg'] =  $this->single.' Successfully deleted';
				$return['result'] = 1;
			} else {
				$return['msg'] = 'Error in deleting '. $this->single;
				$return['result'] = 0;
			}
			
		} else {
			$return['msg'] = 'Error in deleting '. $this->single;
			$return['result'] = 0;
		}

		echo json_encode($return);
	}

	public function paginate($keywords = 0, $branch_id = 0, $sort_by = 'employees.id', $sort_order = 'DESC', $limit = 10, $offset = 0){

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);
		
		if(!empty($branch_id))
			$params['where'][$this->table.'.branch_id'] = $branch_id;	
		

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}
		
		$data = array(); 

		$data['employees'] = $this->employees_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->employees_model->fetch_all($params, true);
		
	
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$keywords . '/'.$branch_id.'/'.$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 8;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');
		
		

		echo $this->load->view($this->module . '/list', $data, true, true);
	}


	function paginate_logs($employee_id = 0, $limit = 10, $offset = 0){


		$params = array(
				'where'  => array('employee_id' => $employee_id),
				'limit'  => $limit,
				'offset' => $offset,
			);

		$data['logs'] = $this->employees_model->get_employee_logs($params);
		unset($params['limit']);
		$total = $this->employees_model->get_employee_logs($params, true);

		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate_logs/' .$employee_id . '/'.$limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 5;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('modal-body');

		echo $this->load->view($this->module.'/log_list', $data, true);
	}
}
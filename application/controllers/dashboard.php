<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Admin_Controller {
	
	public $module;
	public $redirect;
	public $security;
	public $page_title;
	
	function __construct()
	{
		error_reporting(E_ALL & ~E_NOTICE);
		parent::__construct(); 
		$this->load->model(array('inventories_model','inventory_categories','request_orders_model','sales_orders_model'));
		$this->module 	= get_class($this);
		$this->redirect = strtolower($this->module);
		$this->current_module = strtolower($this->module);
		$this->load->library('form_validation');
		$this->page_title = $this->module;
		
		
	}
	
	function index(){
		// 2015-04-06 10:12:37
		$daily_sales = array(
				'date_created >=' => date('Y-m-d').' 00:00:00',
				'date_created <=' => date('Y-m-d').' 23:59:59',
			);
		$data['sales']['today'] = $this->sales_orders_model->fetch_total_sales( $params = array('where' => $daily_sales));
		$yesterday_sales = array(
				'date_created >=' => date('Y-m-d', strtotime("-1 days")).' 00:00:00',
				'date_created <=' => date('Y-m-d', strtotime("-1 days")).' 23:59:59',
			);
		$data['sales']['yesterday'] = $this->sales_orders_model->fetch_total_sales( $params = array('where' => $yesterday_sales));

		$week_sales = array(
				'date_created >=' => date('Y-m-d', strtotime("-7 days")).' 00:00:00',
				'date_created <=' => date('Y-m-d').' 23:59:59',
			);
		$data['sales']['week'] = $this->sales_orders_model->fetch_total_sales( $params = array('where' => $week_sales));

		$two_weeks_sales = array(
				'date_created >=' => date('Y-m-d', strtotime("-14 days")).' 00:00:00',
				'date_created <=' => date('Y-m-d',  strtotime("-7 days")).' 23:59:59',
			);
		$data['sales']['week2'] = $this->sales_orders_model->fetch_total_sales( $params = array('where' => $two_weeks_sales));

		$month_sales = array(
				'date_created >=' => date('Y-m-01').' 00:00:00',
				'date_created <=' => date('Y-m-t').' 23:59:59',
			);
		$data['sales']['month'] = $this->sales_orders_model->fetch_total_sales( $params = array('where' => $month_sales));

		$lastmonth_sales = array(
				'date_created >=' => date('Y-m-01', strtotime("-1 month")).' 00:00:00',
				'date_created <=' => date('Y-m-t', strtotime("-1 month")).' 23:59:59',
			);
		$data['sales']['lastmonth'] = $this->sales_orders_model->fetch_total_sales( $params = array('where' => $lastmonth_sales));
		$last2month_sales = array(
				'date_created >=' => date('Y-m-01', strtotime("-1 month")).' 00:00:00',
				'date_created <=' => date('Y-m-t', strtotime("-1 month")).' 23:59:59',
			);
		$data['sales']['last2months'] = $this->sales_orders_model->fetch_total_sales( $params = array('where' => $last2month_sales));

		// echo $this->db->last_query();
		
		// echo "<pre>"; print_R($data); die;

		
		$data['request_orders'] = $this->request_orders_model->fetch_all($params = array('limit' => 5, 'offset' => 0));
		$data['inventories_out'] = $this->inventories_model->fetch_all($params = array(
																					'where' => 'inventories.stock <= inventories.threshold',
																					'limit' => 5,
																					'offset' => 0,
																				
																				)
																	);
		
		
		$this->view('Dashboard/index', $data);
	
	}
	
	 
	
}
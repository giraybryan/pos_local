<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request_orders extends Admin_Controller {

	public $module = 'request_orders';
	public $page_title = 'Request Stocks';
	public $controller = 'request_orders';
	public $table = 'request_orders';
	public $single = 'Request order';

	
	public function __construct(){

		parent::__construct();
		$this->load->model(array('request_orders_model','inventories_model','inventory_categories'));
		$this->module 	= get_class($this);
		$this->redirect = strtolower($this->module);
		$this->current_module = strtolower($this->module);

	}

	public function index($keywords = 0, $branch_id = 0,$category_id = 0,$sort_by = 'id', $sort_order= 'DESC', $limit = 10, $offset = 0){
		
		$data = $arrayName = array('page' => $this->page_title, 'module' => $this->module );

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);


		$data['pos'] = $this->request_orders_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->request_orders_model->fetch_all($params, true);
		
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$keywords . '/'.$branch_id.'/'.$category_id.'/'.$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');

		$data['categories'] = $this->inventories_model->get_categories();
		$data['branches'] = $this->db->get('settings_branch')->result();

		
		$this->view($this->module . '/index', $data);
	}

	public function paginate($keywords = 0, $branch_id = 0, $category_id = 0, $sort_by = 'id', $sort_order = 'DESC', $limit = 10, $offset = 0){

		$params = array(
			'limit' 	=> $limit,
			'offset'	=> $offset,
			'order_by' 	=> array($sort_by => $sort_order),
		);
		
		if(!empty($branch_id))
			$params['where'][$this->table.'.BranchID'] = $branch_id;	
		
		if(!empty($category_id))
			$params['where']['inventories.inventory_category_id'] = $category_id;

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}

				$params['or_like']['inventories.item'] = $keywords;
				$params['or_like']['settings_branch.branch_name'] = $keywords;
				$params['or_like']['inventory_categories.name'] = $keywords;
		}
		
		$data = array(); 

		$data['pos'] = $this->purchase_orders_model->fetch_all($params);
		unset($params['limit']);
		$total = $this->purchase_orders_model->fetch_all($params, true);
		
		
		$this->load->library('pagination');
		$config['base_url']			= site_url($this->controller.'/paginate/' .$keywords . '/'.$branch_id.'/'.$category_id.'/'.$sort_by . '/' . $sort_order . '/' . $limit  );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 9	;
		$this->pagination->initialize( $config );
		$data['pagination_link'] 	= $this->pagination->create_ajax_links('list_container');
		
		

		echo $this->load->view($this->module . '/list', $data, true, true);
	}


	public function update_request($id = false){

		if(!empty($_POST)){
			// echo "<pre>"; print_r($_POST);die;

			// CHECK ITEM QUANTITY VS REQUEST QUANTY
			


			$items = $this->input->post('item');

			$item_save_ctr = 0;
			$item_row_status = 0;

			//SET A BRANCH ITEM LOG 
			$branch_log_data = array(
					'table' => 'branch_inventory_log',
					'data' 	=> array(
							'branch_id' => $this->input->post('BranchID'),
							'date_created' => date('Y-m-d'),
							'device_id' => $this->input->post('DeviceID'),
						),
				);

			$branch_log_id = $this->request_orders_model->dynamic_save($branch_log_data);
			
			foreach( $items as $ro_id => $ro_info ){


				$ro_info['id'] = $ro_id;
				if( $ro_info['DeliveredQty'] != $ro_info['RequestQty'] ){

					$ro_info['PartialQty'] = $ro_info['DeliveredQty'];
				}

				$current_stock = $ro_info['current_stock'];
				unset($ro_info['current_stock']);

				$ro_item_data = array(
						'table' => 'request_order_list',
						'data' => $ro_info,
					);

				$ro_id = $this->request_orders_model->dynamic_save($ro_item_data);

				//get status of update query
				$item_row_status = $this->db->affected_rows(); 

				if(!empty($item_row_status)){ //if row updated

					$item_save_ctr++;

					//deduct delivered qty to current stock\
					$updated_stock = $current_stock - $ro_info['DeliveredQty'];
					$update_item = array(
							'table' => 'inventories',
							'data' => array('id' => $ro_info['IngredientID'], 'stock' => $updated_stock),
						);
					$this->request_orders_model->dynamic_save($update_item);

					//save item_id and quantity to branch inventory item
					$branch_item_inventory = array(
							'table' => 'branch_inventory_items',
							'data'  => array(
									'branch_log_id' 	=> $branch_log_id,
									'inventory_item_id' => $ro_info['IngredientID'],
									'brach_item_qty' => $ro_info['DeliveredQty'],
								), 
						);
					$branch_log_item_id = $this->request_orders_model->dynamic_save($branch_item_inventory);
				}

			}

			if(!empty($item_save_ctr)){ //if item update save status is not null


				$ro_data = array(
						'id'        => $this->input->post('id'),
						'DeviceID'  => $this->input->post('DeviceID'),
						'Delivered' => 1,
					);

				$rodata = array(
						'table' => 'request_orders',
						'data'  => $ro_data,
					);

				//update the request order 
				$this->request_orders_model->dynamic_save($rodata);

				
				//set mobile device to device_sync = 0 (0 = not yet synced and 1 = synced)so that mobile will fetch the changes in his inventory
				$mobile_data = array(
						'data' => array ('device_sync' => 0),
						'where' => array('DeviceID' => $this->input->post('DeviceID')),
					);

				$mobile_update = $this->request_orders_model->update_mobile_status($mobile_data);

				//set
				$this->session->set_flashdata('message', 'request order item upated!');
				redirect($this->controller.'/update_request/'.$id);

			} else {

				$this->session->set_flashdata('error', 'request order failed to save!');
				redirect($this->controller.'/update_request/'.$id);
			}

			
		}
		if($id){

			
			// $data['inventory_items'] =   $this->inventories_model->fetch_all();
			$data['request_info'] =   $this->request_orders_model->fetch_request_info($id);
			$data['request_items'] = $this->request_orders_model->fetch_request_items($id);



			$this->view($this->module . '/update_request', $data);
			// echo "<pre>";
			// print_r($data);

		}

	}


	public function request_inventory(){

		$category_params = array('order_by' => array('name' => 'ASC'));
		$data['categories'] = $this->inventory_categories->fetch($category_params);
		//echo "<pre>"; print_r($data); die;

		$data['items'] = $this->inventories_model->fetch_all();

		$this->view($this->module. '/request_form/request_form', $data);
	}

	public function add(){

		
		$post = $_POST;

		if(!empty($post['item'])){

			if(!empty($post['id'])){
				$request_id = $this->input->post('id');
			} else {

				$company_info = $this->db->get('settings_company_info')->row();
				$user  = $this->session->CI->user_info;
				
				$key_code = format_code(5);
				$ro_data = array(
						'table' => 'request_orders',
						'data' => array(
								'RequestBy'    => $user['id'],
								'RequestCode'  => $key_code,
								'BranchID'     => $company_info->branch_id,
								'date_created' => date('Y-m-d'),
								'branch_synced'=> 0,
							),
					);

				$request_id = $this->request_orders_model->dynamic_save($ro_data);

				if(empty($request_id)){
					$return['result'] = 0;
					$return['msg'] = 'failed to save request item(s)';
				} else {
					$return['result'] = 1;
					$return['msg'] = 'successfully saved!';
				}

				
			}
		}

		foreach($post['item'] as $item){
				
				$ro_item_data = array(
						'table' => 'request_order_list',
						'data'  => array(
								'RequestQty' 	=> $item['RequestQty'],
								'IngredientID'  => $item['IngredientID'],
								'DeliveredQty'  => (!empty($item['DeliveredQty']) ? $item['DeliveredQty'] : 0 ),
								'PartialQty'	=> 0,
								'ro_id'			=> $request_id,
								'IngredientCode'=> $item['IngredientCode'],
								'IngredientStock'=> $item['current_stock'],
								'IngredientStock'=> $item['current_stock'],
							),


					);

				
				
				$request_item_id = $this->request_orders_model->dynamic_save($ro_item_data);
				
				if(empty($request_item_id)){

					$this->db->where('id', $request_id)->delete('request_orders');
					$return['result'] = 0;
					$return['msg'] = 'failed to save item';
					echo json_encode($return);
					exit;
				} else {

					$return['result'] = 1;
					$return['msg'] = 'item(s) request successfully saved!';
				}
		}

		echo json_encode($return);


		
	}

	public function get_inventory_support($category_id = false, $threshold = false){


		$params = array(
		);
		
		if(!empty($category_id))
			$params['where']['inventories.inventory_category_id'] = $category_id;
		
		if(!empty($keyword))
			$params['like']['inventories.item'] = $keyword;
		
		if(!empty($status_id))
			$params['where']['inventories.status'] = $status_id;
		
		
		if(!empty($threshold)){
			$params['where_statement'] = '`inventories`.`stock` <= `inventories`.`threshold`';
		}

		//print_R($params);
		
	
		

		$data['items'] = $this->inventories_model->fetch_all($params);
		
		
		echo $this->load->view($this->module.'/request_form/inventory_item', $data);


	}


	public function redeliver(){


			if($_POST['password'] == 'admin'){
				$this->db->where( array('id' => $this->input->post('id'), 'device_synced' => 1 ) )->update('request_orders',array('device_synced' => 0));

				$return['result'] = 1;
				$return['msg'] = 'Request order Re-delivered';

			} else {

				$return['result'] = 0;
				$return['msg'] = 'Invalid Password';
			}


			echo json_encode($return);


	}
}
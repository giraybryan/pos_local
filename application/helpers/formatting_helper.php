<?php 
function format_address($fields, $br=false)
{
	if(empty($fields))
	{
		return ;
	}
	
	// Default format
	$default = "{firstname} {lastname}\n{company}\n{address_1}\n{address_2}\n{city}, {zone} {postcode}\n{country}";
	
	// Fetch country record to determine which format to use
	$CI = &get_instance();
	$CI->load->model('location_model');
	$c_data = $CI->location_model->get_country($fields['country_id']);
	
	if(empty($c_data->address_format))
	{
		$formatted	= $default;
	} else {
		$formatted	= $c_data->address_format;
	}

	$formatted		= str_replace('{firstname}', $fields['firstname'], $formatted);
	$formatted		= str_replace('{lastname}',  $fields['lastname'], $formatted);
	$formatted		= str_replace('{company}',  $fields['company'], $formatted);
	
	$formatted		= str_replace('{address_1}', $fields['address1'], $formatted);
	$formatted		= str_replace('{address_2}', $fields['address2'], $formatted);
	$formatted		= str_replace('{city}', $fields['city'], $formatted);
	$formatted		= str_replace('{zone}', $fields['zone'], $formatted);
	$formatted		= str_replace('{postcode}', $fields['zip'], $formatted);
	$formatted		= str_replace('{country}', $fields['country'], $formatted);
	
	// remove any extra new lines resulting from blank company or address line
	$formatted		= preg_replace('`[\r\n]+`',"\n",$formatted);
	if($br)
	{
		$formatted	= nl2br($formatted);
	}
	return $formatted;
	
}

function format_currency($value, $symbol=true)
{

	if(!is_numeric($value))
	{
		return;
	}
	
	$CI = &get_instance();
	
	if($value < 0 )
	{
		$neg = '- ';
	} else {
		$neg = ' ';
	}
	
	if($symbol)
	{
		#$formatted	= number_format(abs($value), 2, $CI->config->item('currency_decimal'), $CI->config->item('currency_thousands_separator'));
		$formatted	= number_format(abs($value));
		
		if($CI->config->item('currency_symbol_side') == 'left')
		{
			$formatted	= $neg.'<span>'.$CI->config->item('currency_symbol').'</span>'.$formatted;
		}
		else
		{
			$formatted	= $neg.'<span>'.$CI->config->item('currency_symbol').'</span>'.$formatted;
		}
	}
	else
	{
		//traditional number formatting
		#$formatted	= number_format(abs($value), 2, '.', ',');
		$formatted	= number_format(abs($value));
	}
	
	return $formatted;
}

function get_discount_value($discount_value, $discount_type){
	$CI = &get_instance();
	if($discount_type == 1 || $discount_type == 3){
		return $discount_value.'%';

	} else {

		return format_currency($discount_value);
	}

}

function get_promo_value($promo_value, $promo_type, $item_ids){
	$CI = &get_instance();

	$CI->load->model('menus_model');
	switch($promo_type){
		case 1: 

			$params['where_in']['menus.id'] = $item_ids;
			
			$menus = $CI->menus_model->fetch_all($params);
			
			$items = '';
			if(!empty($menus)){

				foreach($menus as $menu){

					if(!empty($items)){
						$items .= ', 1'.$menu->title;

					} else {
						$items = '1 '.$menu->title;

					}

				}
			}
			return $promo_items = 'Free '.$items;

			break;
		case 2:

			return format_currency($promo_value);
			break;
		case 3: 
			return $promo_value.'%';
			break;
	} 

}


function format_code($length = 5){

	
    // $key = '';
    // $keys = array_merge(range(0, 9), range('a', 'z'));

    // for ($i = 0; $i < $length; $i++) {
    //     $key .= $keys[array_rand($keys)];
    // }

    $keycode = substr(md5(rand(0, 1000000)), 0, $length);

    return $keycode;

}

function random_numeric($length=5) {       
        $random = "";
        srand((double) microtime() * 1000000);
        $data = "0123456789";
       // $data = "A0B1C2DE3FG4HIJ5KLM6NOP7QR8ST9UVW0XYZ";
        for ($i = 0; $i < $length; $i++) {
            $random .= substr($data, (rand() % (strlen($data))), 1);
        }

        return $random;
}

function get_field_menu_value($str_ids = false){
	if(empty($str_ids))
		return false;



	$ids = explode(',', $str_ids);

	$new_data = array();

	$CI = &get_instance();
	$CI->load->model('menus_model');
	
	foreach($ids as $key => $id){

		$result = $CI->menus_model->get_menu_by_id($id);
		if(!empty($result))
			$new_data[] = array('id' => $result->id, 'text' => $result->title); 
	}
	

	return $new_data;

		
}

function get_ingredient_info($inventory_id){

	$CI = &get_instance();
	$CI->load->model('inventories_model');

	return $CI->inventories_model->get_item_by_id($inventory_id);

}

function get_menu_option($menu_option_id){

	$CI = &get_instance();
	$CI->load->model('categories_model');

	return $CI->categories_model->fetch_option($menu_option_id);

}



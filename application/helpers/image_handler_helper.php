<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Upload and image thumbnailer 
 * 
 * returns array of file names
 */

function _upload_image($temp_id)
{
	
	$CI = &get_instance();
	$upload_path = $CI->config->item('image_upload_path') . $temp_id .'/';
	
	_image_upload($upload_path);
			
}

function _upload_temp($temp_id)
{
	
	$CI = &get_instance();
	$upload_path = $CI->config->item('image_temp_path') . $temp_id .'/';
	
	_image_upload($upload_path);
			
}

function _process_images($property_id, $arrImages)
{
	
	$CI  	   = &get_instance();
	$img_path  =  $CI->config->item('image_upload_path') . $property_id .'/';
	
	_image_upload($img_path);
	_image_resize($arrImages, $img_path);
	_image_thumb($arrImages, $img_path);	
	
}

function _process_temp_images($temp_id, $property_id, $arrImages)
{
	
	$CI 	   = &get_instance();
	$temp_path =  $CI->config->item('image_temp_path') . $temp_id .'/';
	$img_path  =  $CI->config->item('image_upload_path') . $property_id .'/';
		
	_image_resize($arrImages, $temp_path);
	_image_thumb($arrImages, $temp_path);
	
	_move_files($temp_path, $img_path);
	
}

function _image_upload($upload_path)
{
	
	$CI = &get_instance();
	$CI->load->library('upload');
		
	if( !is_dir($upload_path) )
	{
		mkdir($upload_path);
	}
	
	$CI->upload->set_upload_path($upload_path);
	
	foreach ($_FILES as $key => $val)
	{
		if( !empty($_FILES[$key]['name']) )
		{
			if( $CI->upload->do_upload($key) )
			{
				$data     = $CI->upload->data();				
				$images[] = $data['file_name'];
			}	
		}
	}
		
	if( !empty($images) )
	{
		return $images;
	}
	
	return false;
	
		
}

function _image_resize($arrImages, $path)
{
			
	$CI = &get_instance();	
	$CI->config->load('image_lib');
	$CI->load->library('image_lib');	
				
    $config['width']   = $CI->config->item('width');
    $config['height']  = $CI->config->item('height');
		
    foreach ($arrImages as $img)
    {
    	
    	$config['source_image']   = $path . $img;
    	    	
		$CI->image_lib->initialize($config);
		
		if( !$CI->image_lib->resize() )
	    {
	         echo $CI->image_lib->display_errors();      
	         echo '<pre>';
			 print_r($config);
	    }
	    
	    $CI->image_lib->clear();
    }
	
}

function _image_thumb($arrImages, $path)
{
	
	$CI = &get_instance();	
	$CI->config->load('image_lib');
	$CI->load->library('image_lib');	
	
	if($blnThumb)
	{
		$config['width']     	  = $CI->config->item('thumb_width');
    	$config['height']    	  = $CI->config->item('thumb_height');	
	}
	
	foreach ($arrImages as $img)
    {
    	
    	$config['source_image']   = $path . $img;
    	$config['new_image']      = $path .'thumb_'.$img;       
    	    	
		$CI->image_lib->initialize($config);
		
		if( !$CI->image_lib->resize() )
	    {
	         echo $CI->image_lib->display_errors();      
	         echo '1<pre>';
			 print_r($config);
	    }
	    
	    $CI->image_lib->clear();
    }
	
}

function _move_files($src, $dest) 
{
			
    $dir = opendir($src);
    
    @mkdir($dest);
    
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) 
        {
           copy($src . $file, $dest . $file);
           
        }
    }
    
    closedir($dir);
    
    cleanDirr($src);
    
}

function cleanDirr($dirname)
{

    // Sanity check
    if (!file_exists($dirname)) {
        return false;
    }
 
    // Simple delete for a file
    if (is_file($dirname) || is_link($dirname)) {
        return unlink($dirname);
    }
     
    // Create and iterate stack
    $stack = array($dirname);
    while ($entry = array_pop($stack)) {
        // Watch for symlinks
        if (is_link($entry)) {
            unlink($entry);
            continue;
        }
         
        // Attempt to remove the directory
        if (@rmdir($entry)) {
            continue;
        }
         
        // Otherwise add it to the stack
        $stack[] = $entry;
        $dh = opendir($entry);
        while (false !== $child = readdir($dh)) {
            // Ignore pointers
            if ($child === '.' || $child === '..') {
                continue;
            }
             
            // Unlink files and add directories to stack
            $child = $entry . DIRECTORY_SEPARATOR . $child;
            if (is_dir($child) && !is_link($child)) {
                $stack[] = $child;
            } else {
                unlink($child);
            }
        }
        closedir($dh);
       
    }
     
    return true;
}

function _get_name_from_files()
{
	
	foreach ($_FILES as $key => $values)
	{
		if($_FILES[$key]['name'])
		{
			$images[] = $_FILES[$key]['name'];
		}
	}
			
	return json_encode($images);
}





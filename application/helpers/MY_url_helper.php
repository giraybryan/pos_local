<?php

function ssl_support()
{
	$CI =& get_instance();
    return $CI->config->item('ssl_support');
}

if ( ! function_exists('force_ssl'))
{
	function force_ssl()
	{
		if (ssl_support() &&  (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off'))
		{
			$CI =& get_instance();
			$CI->config->config['base_url'] = str_replace('http://', 'https://', $CI->config->config['base_url']);
			redirect($CI->uri->uri_string());
		}
	}
}

//thanks C4iO [PyroDEV]
if ( ! function_exists('remove_ssl'))
{
	function remove_ssl()
	{	
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')
		{
			$CI =& get_instance();
			$CI->config->config['base_url'] = str_replace('https://', 'http://', $CI->config->config['base_url']);
			
			redirect($CI->uri->uri_string());
		}
	}
}

function theme_url($uri)
{
	$CI =& get_instance();
	return $CI->config->base_url(APPPATH.'themes/'.$CI->config->item('theme').'/'.$uri);
}

//to generate an image tag, set tag to true. you can also put a string in tag to generate the alt tag
function theme_img($uri, $tag=false)
{
	if($tag)
	{
		return '<img src="'.theme_url('assets/img/'.$uri).'" alt="'.$tag.'">';
	}
	else
	{
		return theme_url('assets/img/'.$uri);
	}
	
}

function theme_js($uri, $tag=false)
{
	if($tag)
	{
		return '<script type="text/javascript" src="'.theme_url('assets/js/'.$uri).'"></script>';
	}
	else
	{
		return theme_url('assets/js/'.$uri);
	}
}

//you can fill the tag field in to spit out a link tag, setting tag to a string will fill in the media attribute
function theme_css($uri, $tag=false)
{
	if($tag)
	{
		$media=false;
		if(is_string($tag))
		{
			$media = 'media="'.$tag.'"';
		}
		return '<link href="'.theme_url('assets/css/'.$uri).'" type="text/css" rel="stylesheet" '.$media.'/>';
	}
	
	return theme_url('assets/css/'.$uri);
}

function admin_url($url)
{
	
	$CI =& get_instance();	
	
	return $CI->config->site_url($CI->config->item('admin_folder')).'/'.$url;
}


function property_map_thumb($file='', $property_id)
{	
	
	if($file)
	{
		 $CI =& get_instance();
		 
		 if( file_exists($CI->config->item('image_upload_path').$property_id .'/map_'.$file) )
		 {
		 	return $CI->config->base_url($CI->config->item('image_upload_path').$property_id .'/map_'.$file);
		 }
		 else 
		 {
		 	return $CI->config->base_url($CI->config->item('image_upload_path').$property_id .'/thumb_'.$file);
		 }
		 
	}	
	
	return base_url('assets/images/no-image-thumbs.jpg');
	
	
}
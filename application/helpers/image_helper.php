<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*====================================================================
Start - image upload.
======================================================================*/
function _image_process($id, $path)
{
	$images = _image_upload($id, $path);
	_image_resize($id, $images, $path);
 
	return $images;
}
function _image_upload($id, $path)
{
	$CI = &get_instance();
	
	$config['allowed_types'] 	= 'gif|jpg|png';
	$config['max_size']			= '2048';
	
	$CI->load->library('upload', $config);

	$upload_path = $path . $id .'/';

	if( !is_dir($upload_path) )
	{
		mkdir($upload_path);
	}

	$CI->upload->set_upload_path($upload_path);

	$index = 0;
	foreach ($_FILES as $key => $val)
	{
		$index++;
		if( !empty($_FILES[$key]['name']) )
		{
			//renaming the image file
			$fileData = pathinfo(basename($_FILES[$key]["name"]));
			$fileName = date("Ymdhis") . "-" . $index . '.' . $fileData['extension'];
			$_FILES[$key]['name'] = $fileName;
			
			if( $CI->upload->do_upload($key) )
			{
				$data     = $CI->upload->data();
				$images[] = $data['file_name'];
			}
		}
	}

	if( !empty($images) )
	{
		return $images;
	}

	return false;
}
function _image_resize($id, $arrImages, $path)
{

	$CI = &get_instance();
	$CI->config->load('image_lib');
	$CI->load->library('image_lib');

	$config['width']  		  = $CI->config->item('width');
	$config['height'] 		  = $CI->config->item('height');

    foreach ($arrImages as $img)
    {

    	$config['source_image']   = $path . $id .'/'.$img;

		$CI->image_lib->initialize($config);

		if( !$CI->image_lib->resize() )
	    {
	         echo $CI->image_lib->display_errors();
	         echo '1<pre>';
			 print_r($config);
	    }

	    $CI->image_lib->clear();
    }
}
/*====================================================================
END - image upload
======================================================================*/


// ------------------------------------------------------------------------
/* End of file image_helper.php */
/* Location: ./helpers/image_helper.php */
<?php
function load_jquery($front = false)
{
	//jquery & jquery ui files & path
	$path			= 'assets/js/jquery';
	
	$jquery			= 'jquery-1.10.2.min.js';
	$jquery_ui		= 'jquery-ui-1.10.4.custom.min.js';
	$jquery_ui_css	= 'jquery-ui-1.10.4.custom.min.css';
	
	//load jquery ui css
	
	if($front)
	{
		echo link_tag(base_url($path.'/'.$front.'/'.$jquery_ui_css));
	}
	else
	{
		echo link_tag(base_url($path.'/smoothness/'.$jquery_ui_css));
	}
	//load scripts
	echo load_script(base_url($path.'/'.$jquery));
	echo load_script(base_url($path.'/'.$jquery_ui));
	
	//colorbox
	$path			= $path.'/colorbox';
	$colorbox		= 'jquery.colorbox-min.js';
	$colorbox_css	= 'jquery.colorbox.css';
	
	echo link_tag(base_url($path.'/'.$colorbox_css));
	echo load_script(base_url($path.'/'.$colorbox));
}

function load_script($path)
{
	return '<script type="text/javascript" src="'.$path.'"></script>';
}